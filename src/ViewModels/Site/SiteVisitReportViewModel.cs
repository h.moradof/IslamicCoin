﻿namespace ViewModels.Site
{
    public class SiteVisitReportViewModel
    {

        public int TodayVisitCount { get; set; }

        public int YesterdayVisitCount { get; set; }

        public int CurrentMonthVisitCount { get; set; }

        public int CurrentYearVisitCount { get; set; }

        public int TotalVisitCount { get; set; }



        /// <summary>
        ///  Cunstructor
        /// </summary>
        public SiteVisitReportViewModel()
        {
            TodayVisitCount = YesterdayVisitCount = CurrentMonthVisitCount = CurrentYearVisitCount = TotalVisitCount = 0;
        }

    }
}
