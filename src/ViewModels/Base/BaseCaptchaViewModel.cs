﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Base
{
    public abstract class BaseCaptchaViewModel
    {
        // additional prop
        [DisplayName("کد امنیتی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public string Captcha { get; set; }


        public string cc { get; set; }
    }
}
