﻿namespace ViewModels.Base
{
    public class BaseDdlViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }
}
