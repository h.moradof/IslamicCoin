﻿namespace ViewModels.Base
{
    public class BaseTreeViewViewModel
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }
        public string Title { get; set; }
        public bool HaveChild { get; set; }
        public bool IsActive { get; set; }
    }
}
