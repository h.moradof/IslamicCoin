﻿namespace ViewModels.Base
{
    public abstract class BasePaginationViewModel
    {
        public int TotalCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
