﻿namespace ViewModels.API
{
    public class ApiResultViewModel
    {
        public string Message { get; set; }
        public bool HaveError { get; set; }
        public object Data { get; set; }


        public ApiResultViewModel()
        {
            HaveError = false;
            Data = null;
            Message = string.Empty;
        }
    }
}
