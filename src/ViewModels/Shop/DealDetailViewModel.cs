﻿using DomainModels.Entities.Shop;
using System.Collections.Generic;

namespace ViewModels.Shop
{
    public class DealDetailViewModel
    {
        public List<SaleRequestDealViewModel> SaleRequestDeals { get; set; }
        public BuyRequest BuyRequest { get; set; }



        public DealDetailViewModel()
        {
            SaleRequestDeals = new List<SaleRequestDealViewModel>();
        }
    }

    public class SaleRequestDealViewModel
    {
        public SaleRequest SaleRequest { get; set; }
        public Deal Deal { get; set; }
    }
}
