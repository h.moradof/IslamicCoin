﻿using System.ComponentModel.DataAnnotations;
using ViewModels.Base;
using System.ComponentModel;

namespace ViewModels.Account
{
    public class ForgetPasswordViewModel : BaseCaptchaViewModel
    {
        [DisplayName("آدرس ایمیل")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(150, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "فرمت آدرس ایمیل نادرست است")]
        public string Email { get; set; }

    }
}
