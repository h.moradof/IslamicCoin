﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ViewModels.Base;

namespace ViewModels.Account
{
    public class SetPasswordViewModel : BaseCaptchaViewModel
    {
        public long UserId { get; set; }

        [DisplayName("کلمه عبور")]
        [Required(ErrorMessage = "{0} الزامی است")]        
        public string Password { get; set; }
        
        [DisplayName("تکرار کلمه عبور")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "کلمه عبور و تکرار کلمه عبور یکسان نیستند")]
        public string RePassword { get; set; }
        
        public string Code { get; set; }

    }
}
