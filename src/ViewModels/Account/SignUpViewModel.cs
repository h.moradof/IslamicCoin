﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ViewModels.Base;

namespace ViewModels.Account
{
    public class SignUpViewModel : BaseCaptchaViewModel
    {
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string FullName { get; set; }


        [DisplayName("شماره تلفن ثابت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(16, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{10,16}$", ErrorMessage = "فرمت شماره تلفن ثابت نادرست است (فقط عدد - 10 تا 16 رقم)")]
        public virtual string PhoneNumber { get; set; }


        [DisplayName("آدرس ایمیل")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [EmailAddress(ErrorMessage = "فرمت آدرس ایمیل نادرست است")]
        public virtual string EmailAddress { get; set; }


        [DisplayName("نام کاربری (موبایل)")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(11, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{11}$", ErrorMessage = "فرمت نام کاربری نادرست است")]
        public virtual string Username { get; set; }


        [DisplayName("کلمه عبور")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(2048, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string Password { get; set; }


        [DisplayName("تکرار کلمه عبور")]
        [StringLength(2048, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [Compare("Password", ErrorMessage = "کلمه عبور و تکرار کلمه عبور یکسان نیستند")]
        public virtual string RePassword { get; set; }

    }
}
