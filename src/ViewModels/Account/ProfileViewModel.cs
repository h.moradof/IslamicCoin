﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Account
{
    public class ProfileViewModel
    {
        [Key]
        public long UserId { get; set; }


        [DisplayName("آدرس ایمیل")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [EmailAddress(ErrorMessage = "فرمت آدرس ایمیل نادرست است")]
        public virtual string Email { get; set; }


        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(150, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string FullName { get; set; }


        [DisplayName("نام کاربری(موبایل)")]       
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(11, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{11}$", ErrorMessage = "فرمت نام کاربری نادرست است")]
        public string Username { get; set; }

        [DisplayName("می خواهم نام کاربری(موبایل) ام را تغییر دهم")]
        public bool WantUpdateUsername { get; set; }



        [DisplayName("کلمه عبور")]
        public string Password { get; set; }

        [DisplayName("تکرار کلمه عبور")]
        public string RePassword { get; set; }

        [DisplayName("می خواهم کلمه عبورم را تغییر دهم")]
        public bool WantUpdatePassword { get; set; }

    }
}
