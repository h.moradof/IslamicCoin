﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ViewModels.Base;

namespace ViewModels.Account
{
    public class SignInViewModel : BaseCaptchaViewModel
    {

        [DisplayName("نام کاربری (موبایل)")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(11, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string Username { get; set; }


        [DisplayName("کلمه عبور")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public string Password { get; set; }


        [DisplayName("مرا به خاطر بسپار")]
        public bool RememberMe { get; set; }

    }
}
