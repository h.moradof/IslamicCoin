﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ViewModels.Base;

namespace ViewModels.Account
{
    public class SecondLevelLoginViewModel : BaseCaptchaViewModel
    {
        public Guid UserGuid { get; set; }

        [Required(ErrorMessage = "{0} الزامی است")]
        [DisplayName("کلمه عبور مرحله دوم")]
        public string SecondPassword { get; set; }
    }
}
