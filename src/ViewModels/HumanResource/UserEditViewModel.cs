﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace ViewModels.HumanResource
{
    public class UserEditViewModel
    {
        public long Id { get; set; }

        [DisplayName("حداکثر مبلغ ریالی قابل خروج از سایت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public virtual int MaximumRialWithdrawalPrice { get; set; }

        [DisplayName("حداکثر مبلغ ارزی قابل خروج از سایت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public virtual decimal MaximumDolorWithdrawalAmount { get; set; }
    }
}
