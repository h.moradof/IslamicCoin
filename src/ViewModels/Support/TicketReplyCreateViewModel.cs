﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Support
{
    public class TicketReplyCreateViewModel
    {
        [DisplayName("متن پاسخ")]
        [Required( ErrorMessage = "{0} الزامی است")]
        public string Message { get; set; }

        public long TicketId { get; set; }

        [DisplayName("باز است ؟")]
        public bool IsOpen { get; set; }


        public TicketReplyCreateViewModel()
        {
            IsOpen = false;
        }
    }
}
