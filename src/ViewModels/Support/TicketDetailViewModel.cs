﻿using DomainModels.Entities.Support;
using System.Collections.Generic;

namespace ViewModels.Support
{
    public class TicketDetailViewModel
    {
        public Ticket Ticket { get; set; }
        public List<TicketReply>  TicketReplies { get; set; }
    }
}
