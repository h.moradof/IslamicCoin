﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Support
{
    public class TicketCreateViewModel
    {
        [DisplayName("موضوع")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(256, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string Subject { get; set; }

        [DisplayName("متن پیام")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(2048, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string Message { get; set; }
    }
}
