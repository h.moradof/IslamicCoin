﻿namespace ViewModels.Financial
{
    public enum UserRialAccountStatusViewModel : byte
    {
        DontFindAnyAccount,
        AccountIsPending,
        AccountIsAccepted,
        AccountIsDeclined
    }
}
