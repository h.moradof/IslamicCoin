﻿using System;
using Infrastructure.Common;
using DomainModels.Entities.Financial;

namespace ViewModels.Financial
{
    public class RialWithdrawalRequestListViewModel
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string UserFullName { get; set; }
        public int Price { get; set; }
        public string CardNumber { get; set; }
        public string AccountNumber { get; set; }
        public string ShabaNumber { get; set; }
        public string BankName { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? PayDate { get; set; }
        public Guid RowGuid { get; set; }

        public WithdrawalRequestStatus Status { get; set; }
        public string CreatedOnFa { get { return CreatedOn.ToPersianWithTime(); } }
    }
}
