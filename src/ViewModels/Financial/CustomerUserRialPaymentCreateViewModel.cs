﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace ViewModels.Financial
{
    public class CustomerUserRialPaymentCreateViewModel
    {
        [DisplayName("حساب ریالی مبدا")]
        public long UserRialAccountId { get; set; }

        [DisplayName("کد رهگیری واریز")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(64, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string TrackingNumber { get; set; }

        [DisplayName("مبلغ (ریالی) واریز شده")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public int Price { get; set; }

        [DisplayName("تاریخ پرداخت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public string PayDate { get; set; }
    }
}
