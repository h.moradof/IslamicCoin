﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace ViewModels.Financial
{
    public class CustomerUserRialAccountCreateViewModel
    {

        [DisplayName("بانک")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public long BankId { get; set; }

        [DisplayName("شماره شبا")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(64, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string ShabaNumber { get; set; }

        [DisplayName("شماره کارت بانکی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(32, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{16}$", ErrorMessage = "فرمت شماره کارت بانکی نادرست است (فقط عدد - 16 رقم)")]
        public string CardNumber { get; set; }

        [DisplayName("شماره حساب بانکی")]
        [StringLength(64, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]$", ErrorMessage = "فرمت شماره حساب بانکی نادرست است (فقط عدد)")]
        public string AccountNumber { get; set; }

    }
}
