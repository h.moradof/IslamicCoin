﻿using NUnit.Framework;
using Infrastructure.Security;
using System;
using System.Threading;

namespace Infrastruture.Test.Security
{
    [TestFixture]
    public class DataEncryptorTests
    {
        [Test]
        public void AES_Encryptor_Should_Encrypt_And_Decrypt_Correctly()
        {
            var data = "3450345ZXITUEWTRELSKF304DGRT";

            var encryptedData = DataEncryptor.EncryptTpl(data);
            var decryptedData = DataEncryptor.DecryptTpl(encryptedData);

            Assert.AreEqual(data, decryptedData);
        }

        [Test]
        public void EncryptWithSalt_Should_Return_Data_Correctly()
        {
            var data = "34503453450345ZXITUEWTRELSKF304DGRTZXITUEWTRELSKF304DGRT";

            var encryptedData = DataEncryptor.EncryptWithSalt(data);
            var decryptedData = DataEncryptor.DecryptWithSalt(encryptedData);

            Assert.AreEqual(data, decryptedData);
        }

        [Test]
        public void TripleDes_Should_Return_Data()
        {

            var data = "see photo";

            var encryptedData = DataEncryptor.EncryptTpl(data);

            Assert.IsNotEmpty(encryptedData);
        }


        [Test]
        public void TripleDes_Should_Return_Same_Data_At_Any_Time()
        {

            var data = "45645656456345345555";

            var encryptedData1 = DataEncryptor.EncryptTpl(data);
            var encryptedData2 = DataEncryptor.EncryptTpl(data);
            Thread.Sleep(500);
            var encryptedData3 = DataEncryptor.EncryptTpl(data);

            Assert.AreEqual(encryptedData1, encryptedData2);
            Assert.AreEqual(encryptedData1, encryptedData3);
        }

    }
}
