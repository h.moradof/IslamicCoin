﻿$(function () {

    // ========================= message box =========================
    $("#MessageBox .btn-close").click(function () {
        hideMessageBox();
    });

});

function showMessageBox(message) {
    $("#MessageBox #Message").html(message);
    $("#MessageBox").show();
}


function hideMessageBox() {
    $("#MessageBox").hide();
}