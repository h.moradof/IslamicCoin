﻿/* 
    jquery wizard 
    www.artarax.com
*/
function refreshWizard(stepNumber) {
    resetSteps();
    var lastStepIndex = parseInt(stepNumber) - 1;

    for (var i = 0; i <= lastStepIndex; i++) {
        if (i == lastStepIndex) {
            $(".bs-wizard").children('.bs-wizard-step').eq(i).removeClass('disabled').addClass('active');
        }
        else {
            $(".bs-wizard").children('.bs-wizard-step').eq(i).removeClass('disabled').addClass('complete');
        }
    }
}

function resetSteps() {
    $(".bs-wizard .bs-wizard-step").each(function () {
        $(this).removeClass('disabled').removeClass('active').removeClass('complete').addClass('disabled');
    });
}