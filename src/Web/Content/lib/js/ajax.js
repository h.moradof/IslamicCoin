﻿
function Ajax(Type, Url, Data, Success, DataType, ContentType, ProcessData, Async, Cache) {
    
    var ajax = {
        type: (typeof Type === "undefined") ? "GET" : Type,
        url: (typeof Url === "undefined") ? "" : Url,
        data: (typeof Data === "undefined") ? "" : Data,
        success: (typeof Success === "undefined") ? function (data) { } : Success,
        dataType: (typeof DataType === "undefined") ? "html" : DataType,
        contentType: (typeof ContentType === "undefined") ? "application/x-www-form-urlencoded; charset=utf-8" : ContentType,//"application/json; charset=utf-8",
        processData: (typeof ProcessData === "undefined") ? true : ProcessData,
        async: (typeof Async === "undefined") ? true : Async,
        cache: (typeof Cache === "undefined") ? false : Cache,
        error: function (xhr, Result, ajaxOptions, thrownError) { alert('مشکلی در ارتباط با سرور رخ داده است\r\n' + xhr.status + ' '+ xhr.statusText); },
        failure: function () { },
        beforeSend: BeforeSend,
        complete: Complete
    };
    $.ajax(ajax);
}


function Ajax2(Type, Url, Data, Success, DataType, ContentType, ProcessData, Async, Cache) {

    var ajax = {
        type: (typeof Type === "undefined") ? "GET" : Type,
        url: (typeof Url === "undefined") ? "" : Url,
        data: (typeof Data === "undefined") ? "" : Data,
        success: (typeof Success === "undefined") ? function (data) { } : Success,
        dataType: (typeof DataType === "undefined") ? "html" : DataType,
        contentType: (typeof ContentType === "undefined") ? "application/x-www-form-urlencoded; charset=utf-8" : ContentType,//"application/json; charset=utf-8",
        processData: (typeof ProcessData === "undefined") ? true : ProcessData,
        async: (typeof Async === "undefined") ? true : Async,
        cache: (typeof Cache === "undefined") ? false : Cache,
        error: function (xhr, Result, ajaxOptions, thrownError) { alert('مشکلی در ارتباط با سرور رخ داده است\r\n' + xhr.status + ' ' + xhr.statusText); },
        failure: function () { },
        beforeSend: function () { },
        complete: function () { }
    };
    $.ajax(ajax);
}


function BeforeSend() {
    $('#loading').show();
}

function Complete() {
    $('#loading').fadeOut();
}


function prepareLoadedForm() {
    $('.operationform').removeData('validator');
    $('.operationform').removeData('unobtrusiveValidation');
    $.validator.unobtrusive.parse('.operationform');
}