﻿// copyright 2016 Moradof.ir
// ATTENTION >>  
// Initialize id_data_attribute and category_hidden_field_id and category_hidden_field_text variables befor this script file
$(function () {

    $(function () {
        var modelId = $(category_hidden_field_id).val();
        var selectedElement = $('[' + id_data_attribute + '=' + modelId + ']');
        var selectecElementText = selectedElement.html();
        $(category_hidden_field_text).val(selectecElementText);
    });

    $("#treeview a").click(function () {
        var selectedId = $(this).attr(id_data_attribute);
        var seletedText = $(this).html();
        
        $(category_hidden_field_id).val(selectedId);
        $(category_hidden_field_text).val(seletedText);
    });

});