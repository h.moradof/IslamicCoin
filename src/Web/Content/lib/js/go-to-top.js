﻿$(document).ready(function () {

    //Check to see if the window is top if not then display button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 800);
        return false;
    });


    // auto fix navbar menu
    $(window).scroll(function () {
        if ($(this).scrollTop() > 122) {
            console.log($(document).scrollTop());
            $('nav').addClass("navbar-fixed-top");
        } else {
            $('nav').removeClass("navbar-fixed-top");
        }
    });

});