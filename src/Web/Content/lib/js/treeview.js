﻿// copyright 2016 Moradof.ir
$(function () {

    $("#treeview ul li ul").hide();

    $("#treeview ul li i").click(function () {
        $(this).parent("li").children("ul").first().toggle(300);
    });

    $("#closeAll").click(function () {
        $("#treeview ul li ul").hide(300);
    });

    $("#openAll").click(function () {
        $("#treeview ul li ul").show(300);
    });

});