﻿$(function(){

    // =============================== page load ===============================
    var basket = new Array();
    var currentLevel = 1;
    var userId = 0;
    var invoiceId = 0;
    var activateCode = '';
    var mobileCenterNumber = '02156436327';
    var delayBetweenCheckUserAccountStatus = 10000;
    var isShowActivateMessage = false;

    loadProductCategories();

    // =============================== events ===============================
    $('#productCategories').on('click', '.product-amount', function ()
    {
        // change style
        var element = $(this).parent(".product-item");
        if (element.hasClass("selected")) {
            element.removeClass("selected");
        }
        else {
            element.addClass("selected");
        }
    });

    $('#productCategories').on('click', '.product-price', function () {
        // change style
        var element = $(this).parent(".product-item");
        if (element.hasClass("selected")) {
            element.removeClass("selected");
        }
        else {
            element.addClass("selected");
        }
    });
    
    $('#productCategories').on('click', '.plus', function () {
        var btn = $(this);
        var input = btn.parent('.input-group-btn-vertical').parent('.spinner').children('.txt-count');
        if (input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
            input.val(parseInt(input.val(), 10) + 1);
        } else {
            btn.next("disabled", true);
        }
    });

    $('#productCategories').on('click', '.minus', function () {
        var btn = $(this);
        var input = btn.parent('.input-group-btn-vertical').parent('.spinner').children('.txt-count');
        if (input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
            input.val(parseInt(input.val(), 10) - 1);
        } else {
            btn.prev("disabled", true);
        }
    });

    $(".close-message").click(function () {
        $(".dark-wrap").fadeOut(100);
    });

    // -------------- register order level --------------
    $('#productCategories').on('click', '.gotoLevel2', function ()
    {
        var hasSelectedProduct = false;
        basket = [];
        var productId = 0;
        var count = 0;

        // save selected ids
        $("#productCategories .product-item.selected").each(function ()
        {
            productId = parseInt($(this).attr('data-id'));
            count = $(this).children('.spinner').children('.txt-count').val();
            basket.push(new basketItem(productId, count));
            hasSelectedProduct = true;
        });
        
        if (!hasSelectedProduct) {
            showMessage(false,"مبلغ کارت مورد نظر خود را از بخش پایین انتخاب نمایید");
            return;
        }

        gotoOrderLevel(2);
    });

    $("#signupForm").submit(function (e) {
        // signup, save order and goto level3 (show factor)
        e.preventDefault();

        if ($(this).valid())
        {
            signUp();
        }
    });

    $("#loginForm").submit(function (e) {
        // login, save order and goto level3 (show factor)
        e.preventDefault();

        if ($(this).valid())
        {
            login();
        }
    });

    $("#finish").click(function () {
        // goto bank
        var selectedGateway = $("#bankGateways").children("option:selected");

        if (selectedGateway.val() == "0") {
            showMessage(false, "لطفا یک درگاه بانکی را انتخاب نمایید.");
            return;
        }

        var gotoUrl = selectedGateway.attr("data-gotoUrl");
        window.location = gotoUrl + "?id=" + invoiceId + '&gwId=' + selectedGateway.val();
    });
    
    $("#cancel").click(function () {
        cancelOrder("/Home/");
    });

    // --------------------------step buttons------------------------------
    $(".goto-step").click(function ()
    {
        if (!$(this).parent('.bs-wizard-step').hasClass('complete')) {
            return;
        }

        var levelNumber = $(this).attr('data-step');

        if (currentLevel <= levelNumber) {
            showMessage(false,'عملیات درخواستی شما امکان پذیر نیست');
            return;
        }
        gotoOrderLevel(levelNumber);
    });

    $(".btn-return-to-step").click(function ()
    {
        var toStep = $(this).attr('data-step');
        if (currentLevel > toStep)
        {
            gotoOrderLevel(toStep);
        }
    });


    // =============================== functions ===============================

    // --------------------------------- ajax -----------------------------
    function loadProductCategories()
    {
        var i = 0;
        Ajax('GET', '/Ajax/GetAllProductCategories', null, function (response, status, xhr)
        {
            if (response.HaveError) {
                showMessage(false, response.Message);
                return;
            }

            var cardsHtml = '';
            var productHtml = '';
            var isActiveSubmitOrder = false;

            $(response.Data).each(function (index, cat) {

                // products of cat
                if (cat.HasStock)
                {
                    isActiveSubmitOrder = true;

                    productHtml = '<li class="gotoLevel2 btn-submit-order">ثبت سفارش</li>';

                    $(cat.Products).each(function (i, product) {
                        productHtml +=
                            '<li class="product-item" data-id="' + product.Id + '">\
                            <div class="product-amount">$ ' + product.Amount + '</div>\
                            <div class="product-stock">موجودی<br/>' + product.Stock + '</div>\
                            <div class="product-price">' + numberSeperateWithComma(product.Price) + '<span class="rial">&nbsp;&nbsp; ریال</span></div>\
                            <div class="spinner">\
                                <input type="text" disabled="disabled" class="form-control txt-count" value="1" min="1" max="99">\
                                <div class="input-group-btn-vertical">\
                                    <button class="btn btn-default btn-count plus" type="button">+</button>\
                                    <button class="btn btn-default btn-count minus" type="button">-</button>\
                                </div>\
                             </div>\
                         </li>';
                    });
                }

                if (i == 0) {
                    cardsHtml +='<div class="row">';
                }

                cardsHtml +='\
                   <div class="col-md-4 col-sm-6">\
                        <div class="product-cat-card">\
                            <img alt="خرید و فروش ' + cat.Name + '" src="/File/Image/?fileName=' + cat.PhotoName + '" class="product-cat-img" />\
                            <div class="info"><a title="خرید و فروش ' + cat.Name + '" href="/ProductCategory/Detail/' + cat.Id + '"><i class="fa fa-info"></i> &nbsp; اطلاعات بیشتر</a></div>\
                            <h2>' + cat.Name + '</h2>\
                            <ul class="product">'
                            + productHtml +
                            '</ul>\
                        </div>\
                    </div>';

                i += 1;
                if (i == 3) {
                    cardsHtml += '</div>';
                }

                productHtml = '';

            });

            $("#productCategories").append(cardsHtml);

            if (isActiveSubmitOrder) {
                $("#StockIsEmptyMessageBox").hide();
            }
            else {
                $("#StockIsEmptyMessageBox").show();
            }

        }, 'json');
    }

    function signUp()
    {
        Ajax('POST', '/Ajax/SignUp/', $("#signupForm").serialize(), function (response, status, xhr)
        {
            if (response.HaveError) {
                showMessage(false, response.Message);
                return;
            }
            
            activateCode = response.Data;
            showActivateMessage(activateCode);
            checkUserAccountStatus();

        }, 'json');
    }

    function checkUserAccountStatus()
    {
        Ajax('GET', '/Ajax/GetUserAccountStatus/', { 'activateCode' : activateCode }, function (response, status, xhr) {
            if (response.HaveError)
            {
                if (!isShowActivateMessage)
                {
                    showMessage(false, '<div class="loader">loading</div><div class="txt-center"> هنوز اس ام اس  کد فعال سازی از طرف شما دریافت نشده است. این عملیات ممکن است با چند ثانیه تاخیر انجام پذیرد</div>');
                    isShowActivateMessage = true;
                }

                setTimeout(function () { checkUserAccountStatus(); }, delayBetweenCheckUserAccountStatus);
                return;
            }

            userId = response.Data;
            saveOrder(userId, basket);

        }, 'json');
    }

    function login()
    {
        Ajax('POST', '/Ajax/Login/', $("#loginForm").serialize(), function (response, status, xhr)
        {
            if (response.HaveError) {
                showMessage(false, response.Message);
                return;
            }

            activateCode = response.Data.ActivateCode;

            if (response.Data.UserAccountStatus != 2)
            {
                showActivateMessage(activateCode);
                setTimeout(function () { checkUserAccountStatus(); }, delayBetweenCheckUserAccountStatus);
                return;
            }

            userId = response.Data.UserId;
            saveOrder(userId, basket);

        }, 'json');
    }

    function saveOrder(userId, basket)
    {
        Ajax('POST', '/Ajax/SaveOrder/', { 'userId': userId, 'baskets': basket }, function (response, status, xhr)
        {
            if (response.HaveError) {
                showMessage(false, response.Message);
                return;
            }
            
            invoiceId = response.Data;
            hideMessages();
            gotoOrderLevel(3);
            showInvoice();

        }, 'json');
    }

    function cancelOrder(targetUrl)
    {
        if (invoiceId == 0) {
            return;
        }

        Ajax('POST', '/Ajax/CancelOrder/', { 'invoiceId': invoiceId }, function (response, status, xhr)
        {
            if (response.HaveError) {
                showMessage(false, response.Message);
                return;
            }

            window.location = targetUrl;

        }, 'json');
    }

    function showInvoice()
    {
        Ajax('GET', '/Ajax/GetInvoice/', { 'id': invoiceId }, function (response, status, xhr)
        {
            if (response.HaveError) 
            {
                showMessage(false, response.Message);
                return;
            }

            var invoiceHtml =
                '<table class="invoice-table">\
                    <tr>\
                        <th>کارت</th>\
                        <th>مبلغ</th>\
                        <th>قیمت فروش (واحد)</th>\
                        <th>تعداد</th>\
                    </tr>';

            $(response.Data.Orders).each(function (index, order) {
                invoiceHtml = invoiceHtml + '<tr>\
                                                <td>' + order.ProductCategory + '</td>\
                                                <td>' + order.Product + '</td>\
                                                <td>' + numberSeperateWithComma(order.Price) + ' ریال</td>\
                                                <td>' + order.Count + '</td>\
                                            </tr>';

            });

            invoiceHtml = invoiceHtml + '<tr class="total-price">\
                                            <td>مبلغ قابل پرداخت</td>\
                                            <td>' + numberSeperateWithComma(parseInt(response.Data.TotalPrice)) + ' ریال</td>\
                                            <td></td>\
                                            <td></td>\
                                        </tr>';

            invoiceHtml = invoiceHtml + '</table>';

            $("#invoice").html(invoiceHtml);
            fillGatewayDropDown();

        }, 'json');
    }

    function fillGatewayDropDown()
    {
        Ajax('GET', '/Ajax/GetGateways/', null, function (response, status, xhr)
        {
            if (response.HaveError) {
                showMessage(false, response.Message);
                return;
            }

            var gatewaysHtml = '<option value="0">لطفا یک گزینه را انتخاب نمایید</option>';

            $(response.Data).each(function (index, gateway)
            {
                gatewaysHtml = gatewaysHtml + '<option data-gotoUrl="' + gateway.GotoGateWayUrl + '" value="' + gateway.Id + '">' + gateway.Name + '</option>'
            });

            $("#bankGateways").html(gatewaysHtml);
        }, 'json');
    }


    // ---------------------------------------------------------------------
    function gotoOrderLevel(levelNumber)
    {
        if (currentLevel > 2 && levelNumber == 2) {
            showMessage(false, "بازگشت میسر نیست. جهت انصراف روی دکمه انصراف کلیک نمایید");
            return;
        }
        if (levelNumber == 3) {
            rerverseTimer();
        }
        showOrderLevel(levelNumber);
        refreshWizard(levelNumber);
        currentLevel = levelNumber;
    }

    function showOrderLevel(levelNumber)
    {
        hideAllOrderLevels();
        $('#orderLevel' + levelNumber).css('display', 'block');
    }

    function hideAllOrderLevels()
    {
        $("#orderLevel1").css('display','none');
        $("#orderLevel2").css('display', 'none');
        $("#orderLevel3").css('display', 'none');
    }

    function showMessage(isSuccess, message)
    {
        var cssClass = "";
        if (isSuccess)
        {
            cssClass = "alert alert-success";
        }
        else
        {
            cssClass = "alert alert-danger";
        }
        $("#messageBox").html('<div class="'+ cssClass +'">'+ message +'</div>');
        $(".dark-wrap").fadeIn(100);
    }

    function showActivateMessage(activateCode)
    {
        $("#messageBox2").html('<div class="alert alert-success">ثبت نام شما با موفقیت انجام شد، جهت فعال سازی حساب کاربری خود لازم است کد <b>' + activateCode + ' </b>را از طریق شماره موبایلی که با آن ثبت نام نموده اید به شماره ' + mobileCenterNumber + ' ارسال نمایید.</div>');
        $(".dark-wrap").fadeIn(100);
    }

    function hideMessages()
    {
        $(".dark-wrap").css('display','none');
    }

    // -------------------------------timer----------------------------------
    var timeSecond = 0;
    var timeMinute = 0;
    function rerverseTimer()
    {
        var time = $(".reverse-timer");

        if (time.html() == '') {
            timeMinute = parseInt(time.attr("data-start"));
        }

        if (timeMinute == 0 && timeSecond == 0) {
            cancelOrder("/Home/TimeOut");
        }

        timeSecond = timeSecond - 1;

        if (timeSecond == -1) {
            timeSecond = 59;
            timeMinute = timeMinute - 1;
        }

        if (timeSecond > 9) {
            time.html(timeMinute + ':' + timeSecond);
        }
        else {
            time.html(timeMinute + ':0' + timeSecond);
        }

        if (timeMinute >= 0) {
            setTimeout(function () { rerverseTimer(); }, 1000);
        }
    }

    // -------------------------------- objects -----------------------------
    function basketItem(productId, count)
    {
        this.ProductId = productId;
        this.Count = count;
    }
    
});