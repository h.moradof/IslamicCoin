﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.DependencyResolution;
using Web.App_Start;
using Infrastructure.Cache;
using DatabaseContext.Context;
using Services.Interfaces;
using StructureMap.Web.Pipeline;
using Web.Schedule.Registry;
using System.Web.Optimization;

namespace Web
{
    public class Global : HttpApplication
    {

        #region Application_Start

        void Application_Start(object sender, EventArgs e)
        {
            // area
            AreaRegistration.RegisterAllAreas();

            // mvc and web api
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // remove extra ViewEngines
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            // structuremap for mvc
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());

            // cache all data
            CacheRegisteration.Register();

            // -------- schedule --------
            ScheduledTasksRegistry.Init();
        }

        #endregion

        #region Application_BeginRequest

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // -------- schedule --------
            if (string.IsNullOrWhiteSpace(App.SiteRootUrl))
            {
                App.SiteRootUrl = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
            }
        }

        #endregion


        #region Application_EndRequest

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            HttpContextLifecycle.DisposeAndClearAll();
        }

        #endregion


        // ------------------ online user ------------------

        #region Session_Start

        protected void Session_Start(Object sender, EventArgs E)
        {
            // save visit
            var _uow = StructureMapObjectFactory.Container.GetInstance<IUnitOfWork>();
            var _siteVisitService = StructureMapObjectFactory.Container.GetInstance<ISiteVisitService>();
            _siteVisitService.SaveNewVisit(HttpContext.Current.Request.UserHostAddress);
            _uow.SaveChanges();

            // increase online user
            OnlineUsersCacheManager.GetInstance().Plus();
        }

        #endregion


        #region Session_End

        protected void Session_End(Object sender, EventArgs E)
        {
            // decrease online user
            OnlineUsersCacheManager.GetInstance().Minus();
        }

        #endregion

        // ------------------ App Class schedule ------------------
        public static class App
        {
            public static string SiteRootUrl;
        }

        #region Application_End

        protected void Application_End()
        {
            // -------- schedule --------
            ScheduledTasksRegistry.End();
            //نکته مهم این روش نیاز به سرویس پینگ سایت برای زنده نگه داشتن آن است
            ScheduledTasksRegistry.WakeUp(App.SiteRootUrl);
        }

        #endregion
    }
}