﻿using DatabaseContext.Context;
using Infrastructure.Common;
using Infrastructure.Exceptions;
using Infrastructure.Messages;
using Services.Interfaces;
using System;
using System.IO;
using System.Text;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class TransactionExcelFileController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly ITransactionExcelFileService _transactionExcelFileService;
        #endregion

        #region ctor
        public TransactionExcelFileController(
            IUnitOfWork uow,
            ITransactionExcelFileService transactionExcelFileService)
        {
            _uow = uow;
            _transactionExcelFileService = transactionExcelFileService;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _transactionExcelFileService.GetAll(page.Value);
            return View(list);
        }


        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string c = "")
        {
            try
            {
                _transactionExcelFileService.Add(Request.Files);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessInsert.Description() };
                return RedirectToAction("Index");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        
    }
}