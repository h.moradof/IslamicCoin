﻿using Services.Interfaces;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class DashboardController : Controller
    {

        #region props

        private readonly ISiteVisitService _siteVisitService;

        #endregion

        #region ctor

        public DashboardController(ISiteVisitService siteVisitService)
        {
            _siteVisitService = siteVisitService;
        }

        #endregion


        [HttpGet]
        public ActionResult Show()
        {
            var vmSiteVisitMonthly = _siteVisitService.GetMonthlyVisits();
            return View(vmSiteVisitMonthly);
        }


	}
}