﻿using DomainModels.Entities.Site;
using Services.Interfaces;
using System.Web.Mvc;
using Infrastructure.Common;
using Infrastructure.Messages;
using DatabaseContext.Context;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class SiteSeoSettingController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly ISiteSeoSettingService _siteSeoSettingService;

        #endregion

        #region ctor

        public SiteSeoSettingController(IUnitOfWork uow, ISiteSeoSettingService siteSeoSettingService)
        {
            _siteSeoSettingService = siteSeoSettingService;
            _uow = uow;
        }

        #endregion

        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var siteSetting = _siteSeoSettingService.Find(id);

            if (siteSetting == null)
                return HttpNotFound();

            return View(siteSetting);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SiteSeoSetting model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var updatingItem =  _siteSeoSettingService.Find(model.Id);

            model.CopyTo<SiteSeoSetting>(ref updatingItem);

            _siteSeoSettingService.Edit(updatingItem);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
            return RedirectToAction("Show", "Message", new { area = "Admin" });

        }


	}
}