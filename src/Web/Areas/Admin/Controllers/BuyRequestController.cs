﻿using Services.Interfaces;
using System;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class BuyRequestController : Controller
    {

        #region props
        private readonly IBuyRequestService _buyRequestService;
        #endregion

        #region ctor
        public BuyRequestController(IBuyRequestService buyRequestService)
        {
            _buyRequestService = buyRequestService;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _buyRequestService.GetAll(page.Value);
            return View(list);
        }



        // ---------------------------------------------- Search
        [HttpGet]
        public ActionResult Search(Guid? code, int? page = 1)
        {
            var list = _buyRequestService.SearchByRowGuid(code, page.Value);
            return View(list);
        }


    }
}