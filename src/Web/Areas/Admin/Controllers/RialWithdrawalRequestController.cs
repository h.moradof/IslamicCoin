﻿using DatabaseContext.Context;
using DomainModels.Entities.Financial;
using Services.Interfaces;
using System;
using System.Web.Mvc;
using ViewModels.API;

namespace Web.Areas.Admin.Controllers
{

    [Authorize(Roles = "Administrators")]
    public class RialWithdrawalRequestController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IRialWithdrawalRequestService _rialWithdrawalRequestService;
        private readonly IPaymentFileService _paymentFileService;
        #endregion

        #region ctor
        public RialWithdrawalRequestController(
            IUnitOfWork uow,
            IRialWithdrawalRequestService rialWithdrawalRequestService,
            IPaymentFileService paymentFileService)
        {
            _uow = uow;
            _rialWithdrawalRequestService = rialWithdrawalRequestService;
            _paymentFileService = paymentFileService;
        }
        #endregion


        // ---------------------------------------------- Pending Requests
        [HttpGet]
        public ActionResult PendingRequests()
        {
            return View();
        }


        [HttpGet]
        public JsonResult GetRequests(string status)
        {
            var statusName = (WithdrawalRequestStatusName)Enum.Parse(typeof(WithdrawalRequestStatusName), status);

            var list = _rialWithdrawalRequestService.GetListByStatusName(statusName);

            return Json(new ApiResultViewModel { Data = list }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult CreatePaymentFile(long[] requestIds)
        {
            // نمایش بستانکاران، انتخاب آنها، ساخت فایل برای بانک پاسارگاد
            // WarehouseTransaction و ثبت رکورد در جدول

            var paymentFile = _paymentFileService.Add(requestIds);
            _uow.SaveChanges();

            return Json(new ApiResultViewModel { Message = "فایل پرداخت با شناسه " + paymentFile.RowGuid + " با موفقیت ایجاد شد. لطفا با استفاده از فایل پرداخت ایجاد شده از طریق سامانه بانکداری مجازی بانک پاسارگاد نسبت به پرداخت مبالغ اقدام نمایید" });
        }

        // ---------------------------------------------- On Processing Requests
        [HttpGet]
        public ActionResult OnProcessingRequests()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ChangeRequestsStatusToPaid(long[] requestIds)
        {
            _rialWithdrawalRequestService.ChangeStatus(requestIds, WithdrawalRequestStatusName.Paid);
            _uow.SaveChanges();

            return Json(new ApiResultViewModel());
        }

        [HttpPost]
        public JsonResult ChangeRequestsStatusToPanding(long[] requestIds)
        {
            _rialWithdrawalRequestService.ChangeStatus(requestIds, WithdrawalRequestStatusName.Pending);
            _uow.SaveChanges();

            return Json(new ApiResultViewModel());
        }


        // ---------------------------------------------- Paid Requests
        [HttpGet]
        public ActionResult PaidRequests(int? page = 1)
        {
            var list = _rialWithdrawalRequestService.GetAllByStatusName(WithdrawalRequestStatusName.Paid, page.Value);

            return View(list);
        }



        // ---------------------------------------------- Search
        [HttpGet]
        public ActionResult Search(Guid? code, int? page = 1)
        {
            var list = _rialWithdrawalRequestService.SearchByRowGuid(code, page.Value);
            return View(list);
        }

    }
}