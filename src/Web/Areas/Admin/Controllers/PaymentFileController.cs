﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class PaymentFileController : Controller
    {

        #region props
        private readonly IPaymentFileService _paymentFileService;
        #endregion

        #region ctor
        public PaymentFileController(IPaymentFileService paymentFileService)
        {
            _paymentFileService = paymentFileService;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var paymentFiles = _paymentFileService.GetAll(page.Value);
            return View(paymentFiles);
        }



        // ---------------------------------------------- Download
        public ActionResult Download(Guid code)
        {
            var paymentFile = _paymentFileService.FindByRowGuid(code);

            var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(paymentFile.FileContent));

            return File(memoryStream, "text/plain", paymentFile.RowGuid.ToString() + ".txt");
        }


    }
}