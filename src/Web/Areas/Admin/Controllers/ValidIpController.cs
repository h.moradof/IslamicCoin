﻿using DatabaseContext.Context;
using DomainModels.Entities.Access;
using Infrastructure.Common;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class ValidIpController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IValidIpService _validIpService;
        #endregion

        #region ctor
        public ValidIpController(
            IUnitOfWork uow, 
            IValidIpService validIpService)
        {
            _uow = uow;
            _validIpService = validIpService;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var validIps = _validIpService.GetAll(page.Value);
            return View(validIps);
        }



        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ValidIp model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _validIpService.Add(model);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.ErrorInsert.Description() };
            return RedirectToAction("Index");
        }



        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var validIp = _validIpService.Find(id);
            return View(validIp);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ValidIp model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var updatingItem = _validIpService.Find(model.Id);
            model.CopyTo(ref updatingItem);
            _validIpService.Edit(updatingItem);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.ErrorUpdate.Description() };
            return RedirectToAction("Index");
        }


        // ---------------------------------------------- Delete
        [HttpGet]
        public ActionResult Delete()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            if (ModelState.IsValid)
            {
                _validIpService.Delete(id);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessDelete.Description() };
                return RedirectToAction("Index");
            }

            return View();
        }


    }
}