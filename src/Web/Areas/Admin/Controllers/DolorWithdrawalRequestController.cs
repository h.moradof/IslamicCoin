﻿using Services.Interfaces;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class DolorWithdrawalRequestController : Controller
    {

        #region props
        private readonly IDolorWithdrawalRequestService _dolorWithdrawalRequestService;
        #endregion

        #region ctor
        public DolorWithdrawalRequestController(IDolorWithdrawalRequestService dolorWithdrawalRequestService)
        {
            _dolorWithdrawalRequestService = dolorWithdrawalRequestService;
        }
        #endregion



        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _dolorWithdrawalRequestService.GetAll(page.Value);
            return View(list);
        }



        // ---------------------------------------------- Search
        [HttpGet]
        public ActionResult Search(string text, int? page = 1)
        {
            var list = _dolorWithdrawalRequestService.Search(text, page.Value);
            return View(list);
        }



    }
}