﻿using Services.Interfaces;
using System;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class UserDolorPaymentController : Controller
    {

        #region props
        private readonly IUserDolorPaymentService _userDolorPaymentService;
        #endregion

        #region ctor
        public UserDolorPaymentController(IUserDolorPaymentService userDolorPaymentService)
        {
            _userDolorPaymentService = userDolorPaymentService;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _userDolorPaymentService.GetAll(page.Value);
            return View(list);
        }



        // ---------------------------------------------- Search
        [HttpGet]
        public ActionResult Search(Guid? code, int? page = 1)
        {
            var list = _userDolorPaymentService.SearchByRowGuid(code, page.Value);
            return View(list);
        }
    }
}