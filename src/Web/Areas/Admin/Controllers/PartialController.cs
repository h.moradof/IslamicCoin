﻿using Infrastructure.Cache.Interfaces;
using Services.Interfaces;
using Web.Security.Authentication;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class PartialController : Controller
    {

        #region props

        private readonly ISiteVisitService _siteVisitService;
        private readonly ISiteSeoSettingCacheManager _siteSeoSettingCacheManager;

        #endregion

        #region ctor

        public PartialController(
            ISiteVisitService siteVisitService,
            ISiteSeoSettingCacheManager siteSeoSettingCacheManager)
        {
            _siteVisitService = siteVisitService;
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
        }

        #endregion


        [ChildActionOnly]
        public PartialViewResult _TopMenu()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public PartialViewResult _RightMenu()
        {
            return PartialView();
        }


        [ChildActionOnly]
        public PartialViewResult _DashboardTitle()
        {
            var siteSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(siteSetting);
        }


        [ChildActionOnly]
        public ActionResult _SiteVisitReport()
        {
            var vmSiteVisitReport = _siteVisitService.GetSiteVisitReport();

            return PartialView(vmSiteVisitReport);
        }
        
    }
}