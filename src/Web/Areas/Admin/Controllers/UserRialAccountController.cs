﻿using DatabaseContext.Context;
using DomainModels.Entities.Financial;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class UserRialAccountController : Controller
    {
        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserRialAccountService _userRialAccountService;
        #endregion

        #region ctor
        public UserRialAccountController(
            IUnitOfWork uow,
            IUserRialAccountService userRialAccountService)
        {
            _uow = uow;
            _userRialAccountService = userRialAccountService;
        }
        #endregion



        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(string acceptStatusName, int? page = 1)
        {
            var list = _userRialAccountService.GetAll(acceptStatusName, page.Value);
            return View(list);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Accept(long id)
        {
            _userRialAccountService.ChangeAcceptStatus(id, AccountAcceptStatusName.Accepted);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "شماره حساب با موفقیت تایید شد" };
            return RedirectToAction("Index", new { acceptStatusName = "pending" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Decline(long id)
        {
            _userRialAccountService.ChangeAcceptStatus(id, AccountAcceptStatusName.Declined);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "شماره حساب با موفقیت رد شد" };
            return RedirectToAction("Index", new { acceptStatusName = "pending" });
        }

    }
}