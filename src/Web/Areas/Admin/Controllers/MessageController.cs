﻿using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class MessageController : Controller
    {
        
        public ActionResult Show()
        {
            return View();
        }
	}
}