﻿using Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using ViewModels.API;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class SaleRequestController : Controller
    {

        #region props
        private readonly ISaleRequestService _saleRequestService;
        #endregion

        #region ctor
        public SaleRequestController(ISaleRequestService saleRequestService)
        {
            _saleRequestService = saleRequestService;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _saleRequestService.GetAll(page.Value);
            return View(list);
        }


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Childs(long parentId)
        {
            var list = _saleRequestService.GetChilds(parentId);

            return View(list);
        }



        // ---------------------------------------------- Search
        [HttpGet]
        public ActionResult Search(Guid? code, int? page = 1)
        {
            var list = _saleRequestService.SearchByRowGuid(code, page.Value);
            return View(list);
        }
    }
}