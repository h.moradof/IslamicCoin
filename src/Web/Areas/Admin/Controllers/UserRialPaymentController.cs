﻿using Services.Interfaces;
using System;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{

    // وجوه دریافتی ریالی از کاربران 
    [Authorize(Roles = "Administrators")]
    public class UserRialPaymentController : Controller
    {
       
        #region props
        private readonly IUserRialPaymentService _userRialPaymentService;
        #endregion

        #region ctor
        public UserRialPaymentController(IUserRialPaymentService userRialPaymentService)
        {
            _userRialPaymentService = userRialPaymentService;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _userRialPaymentService.GetAll(page.Value);
            return View(list);
        }



        // ---------------------------------------------- Search
        [HttpGet]
        public ActionResult Search(Guid? code, int? page = 1)
        {
            var list = _userRialPaymentService.SearchByRowGuid(code, page.Value);
            return View(list);
        }
    }
}