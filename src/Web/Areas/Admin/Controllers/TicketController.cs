﻿using DatabaseContext.Context;
using DomainModels.Entities.Support;
using Infrastructure.Messages;
using Services.Interfaces;
using System;
using System.Web.Mvc;
using ViewModels.Support;
using Web.Security.Authentication;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class TicketController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly ITicketService _ticketService;
        private readonly ITicketReplyService _ticketReplyService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public TicketController(
            IUnitOfWork uow, 
            ITicketService ticketService, 
            ITicketReplyService ticketReplyService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _ticketService = ticketService;
            _ticketReplyService = ticketReplyService;
            _currentUser = currentUser;
        }
        #endregion


        // ---------------------------------------------- OpenTickets
        [HttpGet]
        public ActionResult OpenTickets(int? page = 1)
        {
            var openTickets = _ticketService.GetTickets(true, page.Value);

            return View(openTickets);
        }


        // ---------------------------------------------- CloseTickets
        [HttpGet]
        public ActionResult CloseTickets(int? page = 1)
        {
            var closeTickets = _ticketService.GetTickets(false, page.Value);

            return View(closeTickets);
        }


        // ---------------------------------------------- Detail
        [HttpGet]
        public ActionResult Detail(long id)
        {
            var vmTicketDetail = _ticketService.GetTicketDetailViewModel(id);

            return View(vmTicketDetail);
        }


        // ---------------------------------------------- _Reply
        [ChildActionOnly]
        [HttpGet]
        public PartialViewResult _Reply(long ticketId)
        {
            return PartialView(new TicketReplyCreateViewModel { TicketId = ticketId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Reply(TicketReplyCreateViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return PartialView(model);

                _ticketReplyService.Add(model, _currentUser.Id);
                _uow.SaveChanges();

                return RedirectToAction("Detail", "Ticket", new { area = "Admin", id = model.TicketId });
            }
            catch (UnauthorizedAccessException ex)
            {
                TempData["Message"] = new Message { IsSuccess = true, MessageBody = ex.Message };
                return RedirectToAction("Show", "Message", new { area = "Admin" });
            }
        }


    }
}