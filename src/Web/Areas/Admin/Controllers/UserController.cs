﻿using DatabaseContext.Context;
using DomainModels.Entities.HumanResource;
using Infrastructure.Common;
using Infrastructure.Messages;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModels.HumanResource;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class UserController : Controller
    {
        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserService _userServie;
        #endregion

        #region ctor
        public UserController(
            IUnitOfWork uow,
            IUserService userServie)
        {
            _uow = uow;
            _userServie = userServie;
        }
        #endregion



        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _userServie.GetAll(page.Value);
            return View(list);
        }



        // ---------------------------------------------- Detail
        [HttpGet]
        public ActionResult Detail(long id)
        {
            var user = _userServie.Find(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }


        // ---------------------------------------------- Search
        [HttpGet]
        public ActionResult Search(string text, int? page = 1)
        {
            var list = _userServie.Search(text, page.Value);
            return View(list);
        }


        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var user = _userServie.Find(id);

            return View(new UserEditViewModel { Id = user.Id,MaximumRialWithdrawalPrice = user.MaximumRialWithdrawalPrice, MaximumDolorWithdrawalAmount = user.MaximumDolorWithdrawalAmount  });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserEditViewModel model)
        {
            var user = _userServie.Find(model.Id);
            user.MaximumRialWithdrawalPrice = model.MaximumRialWithdrawalPrice;
            user.MaximumDolorWithdrawalAmount = model.MaximumDolorWithdrawalAmount;
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
            return RedirectToAction("Index");
        }




        // ---------------------------------------------- Ban
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Ban(long id, string banReason)
        {
            _userServie.Ban(id, banReason);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "کاربر با موفقیت بن شد" };
            return RedirectToAction("Index");
        }


        // ---------------------------------------------- Activate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Activate(long id)
        {
            _userServie.Active(id);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "کاربر با موفقیت فعال شد" };
            return RedirectToAction("Index");
        }

        // ---------------------------------------------- Accept/Decline Identification Card Photo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AcceptIdentificationCardPhoto(long id)
        {
            var user = _userServie.Find(id);
            user.IdentificationCardAcceptStatus = AcceptStatus.Accepted;
            _uow.SaveChanges();

            _userServie.PermitUserIfPossible(id);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "تصویر شناسنامه کاربر با موفقیت تایید و دسترسی کاربر برای ویرایش تصویر شناسنامه اش بسته شد" };
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeclineIdentificationCardPhoto(long id)
        {
            var user = _userServie.Find(id);
            user.IdentificationCardAcceptStatus = AcceptStatus.Declined;
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "تصویر شناسنامه کاربر با موفقیت رد و دسترسی کاربر جهت ویرایش تصویر شناسنامه اش باز شد" };
            return RedirectToAction("Index");
        }





        // ---------------------------------------------- Accept/Decline Face And National Card Photo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AcceptFaceAndNationalCardPhoto(long id)
        {
            var user = _userServie.Find(id);
            user.FaceAndNationalCardAcceptStatus = AcceptStatus.Accepted;
            _uow.SaveChanges();

            _userServie.PermitUserIfPossible(id);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "تصویر چهره و کارت ملی کاربر با موفقیت تایید و دسترسی کاربر جهت ویرایش تصویر چهره و کارت کلی اش بسته شد" };
            return RedirectToAction("Index");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeclineFaceAndNationalCardPhoto(long id)
        {
            var user = _userServie.Find(id);
            user.FaceAndNationalCardAcceptStatus = AcceptStatus.Declined;
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "تصویر چهره و کارت ملی کاربر با موفقیت رد و دسترسی کاربر جهت ویرایش تصویر چهره و کارت کلی اش باز شد" };
            return RedirectToAction("Index");
        }



        // ---------------------------------------------- Accept Phone Number
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AcceptPhoneNumber(long id)
        {
            var user = _userServie.Find(id);
            user.PhoneNumberAcceptStatus = AcceptStatus.Accepted;
            _uow.SaveChanges();

            _userServie.PermitUserIfPossible(id);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "شماره ثابت کاربر با موفقیت تایید شد" };
            return RedirectToAction("Index");
        }


    }
}