﻿using DatabaseContext.Context;
using DomainModels.Entities.Setting;
using Infrastructure.Common;
using Infrastructure.Exceptions;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class DealProcSettingController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IDealProcSettingService _dealProcSettingService;
        #endregion

        #region ctor
        public DealProcSettingController(
            IUnitOfWork uow,
            IDealProcSettingService dealProcSettingService)
        {
            _uow = uow;
            _dealProcSettingService = dealProcSettingService;
        }
        #endregion



        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long? id)
        {
            var model = _dealProcSettingService.GetDefaultRow();

            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit()
        {
            try
            {
                _dealProcSettingService.TryToTurnOffProcedure();
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
                return RedirectToAction("Show", "Message", new { area = "Admin" });
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(_dealProcSettingService.GetDefaultRow());
            }

        }
    }
}