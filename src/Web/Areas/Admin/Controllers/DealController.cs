﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class DealController : Controller
    {

        #region props
        private readonly IDealService _dealService;
        #endregion

        #region ctor
        public DealController(IDealService dealService)
        {
            _dealService = dealService;
        }
        #endregion



        // ---------------------------------------------- Detail
        /// <summary>
        /// Show Detail of Deal
        /// </summary>
        /// <param name="code">saleRequest's rowGuid OR buyRequest's rowGuid</param>
        [HttpGet]
        public ActionResult Detail(Guid code)
        {
            var vm = _dealService.GetDealByCode(code);

            return View(vm);
        }



    }
}