﻿using DatabaseContext.Context;
using DomainModels.Entities.Setting;
using Infrastructure.Common;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class FinancialSettingController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IFinancialSettingService _financialSettingService;
        #endregion

        #region ctor
        public FinancialSettingController(
            IUnitOfWork uow, 
            IFinancialSettingService financialSettingService)
        {
            _uow = uow;
            _financialSettingService = financialSettingService;
        }
        #endregion



        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit()
        {
            var model = _financialSettingService.GetDefaultRow();

            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FinancialSetting model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var updatingItem = _financialSettingService.GetDefaultRow();

            model.CopyTo<FinancialSetting>(ref updatingItem);

            _financialSettingService.Edit(updatingItem);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
            return RedirectToAction("Show", "Message", new { area = "Admin" });

        }
    }
}