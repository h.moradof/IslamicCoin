﻿using DatabaseContext.Context;
using DomainModels.Entities.HumanResource;
using Services.Interfaces;
using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ViewModels.Account;
using Infrastructure.Security;
using Infrastructure.Cache.Interfaces;
using Infrastructure.Common;
using Infrastructure.Mail.Interfaces;
using System.Threading.Tasks;
using Web.Security;
using Infrastructure.Exceptions;
using System.Linq;
using DomainModels.Entities.Access;
using Infrastructure.Messages;

namespace Web.Areas.Account.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        private readonly IMailSender _mailSender;
        private readonly ISiteSeoSettingCacheManager _siteSeoSettingCacheManager;
        private readonly IApplicationSettingCacheManager _applicationSettingCacheManager;

        #endregion

        #region ctor

        public HomeController(
            IUnitOfWork uow,
            IUserService userService,
            IMailSender mailSender,
            ISiteSeoSettingCacheManager siteSeoSettingCacheManager,
            IApplicationSettingCacheManager applicationSettingCacheManager)
        {
            _uow = uow;
            _userService = userService;
            _mailSender = mailSender;
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
            _applicationSettingCacheManager = applicationSettingCacheManager;
        }

        #endregion


        #region sign up

        // -------------------------------- SignUp
        [HttpGet]
        public ActionResult SignUp()
        {
            return GetSignUpView(new SignUpViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(SignUpViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return GetSignUpView(model);

                if (!CaptchaManager.IsValid(model.cc, model.Captcha))
                {
                    throw new CustomException("کد امنیتی نادرست است");
                }

                _userService.SignUp(model);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = "ثبت نام شما با موفقیت انجام شد، جهت ورود به پنل کاربری <a class='blue-link' href='/Account/SignIn'>اینجا</a> کلیک نمایید" };
                return RedirectToAction("Show", "Message", new { area = "" });
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return GetSignUpView(model);
            }
        }

        // get sign up view (for captcha)
        private ActionResult GetSignUpView(SignUpViewModel vm)
        {
            vm.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(5));

            return View(vm);
        }


        #endregion

        // -------------------------------- SignIn
        #region signin

        [HttpGet]
        public ActionResult SignIn()
        {
            return GetSignInView(new SignInViewModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(SignInViewModel vmSignIn)
        {
            try
            {
                if (!ModelState.IsValid)
                    return GetSignInView(vmSignIn);

                if (!CaptchaManager.IsValid(vmSignIn.cc, vmSignIn.Captcha))
                {
                    throw new CustomException("کد امنیتی نادرست است");
                }

                var user = _userService.SignIn(vmSignIn.Username, vmSignIn.Password);

                ValidateUserOnSignIn(user);

                SetupFormsAuthTicket(user, vmSignIn.RememberMe);

                var roleNames = _userService.GetRoleNamesForUser(user.Id);

                if (roleNames.Contains(RoleName.Administrators) || roleNames.Contains(RoleName.Employees))
                {
                    return RedirectToAction("Show", "Dashboard", new { area = "Admin" });
                }
                else
                {
                    return RedirectToAction("Show", "Dashboard", new { area = "Customer" });
                }
                
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return GetSignInView(vmSignIn);
            }

        }

        private void ValidateUserOnSignIn(User user)
        {
            if (user == null)
            {
                throw new CustomException("نام کاربری یا کلمه عبور نادرست است");
            }

            if (user.AccountStatus == DomainModels.Entities.Base.AccountStatus.Baned)
            {
                throw new CustomException("حساب کاربری شما به دلیل '" + user.BanReason +"' مسدود شده است.");
            }

            if (user.AccountStatus == DomainModels.Entities.Base.AccountStatus.Deactive)
            {
                throw new CustomException("حساب کاربری شما غیرفعال است");
            }
        }


        // get signin view (for captcha)
        private ActionResult GetSignInView(SignInViewModel vmSignIn)
        {
            vmSignIn.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(5));

            return View(vmSignIn);
        }




        // create login cookie
        private void SetupFormsAuthTicket(User user, bool persistanceFlag)
        {
            var userData = user.Id.ToString(CultureInfo.InvariantCulture);

            var authTicket = new FormsAuthenticationTicket(1, //version
                                user.Username, // name
                                DateTime.Now,             //creation
                                DateTime.Now.AddMinutes(30), //Expiration
                                persistanceFlag, //Persistent
                                userData  // userId
                                );

            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));
        }

        #endregion


        // -------------------------------- ForgetPassword
        #region forgetpassword

        [HttpGet]
        public ActionResult ForgetPassword()
        {
            return GetForgetPasswordView(new ForgetPasswordViewModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPassword(ForgetPasswordViewModel vmForgetPassword)
        {
            if (!ModelState.IsValid)
            {
                return GetForgetPasswordView(vmForgetPassword);
            }


            if (string.IsNullOrEmpty(vmForgetPassword.Email) || string.IsNullOrWhiteSpace(vmForgetPassword.Email))
            {
                ModelState.AddModelError("", "آدرس ایمیل الزامی است");
                return GetForgetPasswordView(vmForgetPassword);
            }

            ModelState.Clear();

            if (!CaptchaManager.IsValid(vmForgetPassword.cc, vmForgetPassword.Captcha))
            {
                ModelState.AddModelError("", "کد امنیتی نادرست است");
                return GetForgetPasswordView(vmForgetPassword);
            }

            vmForgetPassword.Email = vmForgetPassword.Email.Trim();

            if (!_userService.IsValidEmail(vmForgetPassword.Email))
            {
                ModelState.AddModelError("", "آدرس ایمیل نادرست است");
                return GetForgetPasswordView(vmForgetPassword);
            }

            bool flag = SendForgetPasswordEmail(vmForgetPassword);

            if (flag)
            {
                return View("~/Areas/Account/Views/Home/SuccessForgetPassword.cshtml");
            }
            else
            {
                ModelState.AddModelError("", "خطایی در ارسال ایمیل رخ داده است؛ لطفا دقایقی دیگر مجددا اقدام نمایید");
                return GetForgetPasswordView(vmForgetPassword);
            }

        }


        private bool SendForgetPasswordEmail(ForgetPasswordViewModel vmForgetPassword)
        {
            bool flag = false;
            var userData = _userService.GetForgetPasswordDataByEmail(vmForgetPassword.Email);
            var siteSetting = _siteSeoSettingCacheManager.GetDefaultSetting();
            string subject = string.Format("{0} - فراموشی کلمه عبور", siteSetting.Title);

            // ==============================================[ ارسال ایمیل ]================================================
            string body = string.Format("<div style=\"direction: rtl; font-family: tahoma; font-size: 13px; background-color: #eaeaea;padding: 10px;\"><p style=\"font-family: Arial; font-size: 24px;\"><b>{0} - سیستم فراموشی کلمه عبور</b></p><br /><br /><h2 style=\"font-family: Arial;\">{3}</h2><p style=\"font-family: tahoma; font-size: 13px;\"><span>نام کاربری شما <b>{4}</b> است.</span><br/><br /><span>جهت تنظیم کلمه عبور جدید می توانید بر روی لینک روبرو کلیک نمایید:&nbsp;</span><span style=\"color: blue\"><b><a href=\"{1}/Account/Home/SP/?uc={2}\">تنظیم کلمه عبور جدید</a></b></span></p><p>» پس از کلیک بر روی لینک بالا ، وارد یکی از صفحات سایت ما خواهید شد و از طریق آن صفحه می توانید برای خود کلمه عبور جدید تنظیم نمایید.</p><br /><p style=\"font-family: tahoma; font-size: 13px; color: red;\">» در صورتی که تقاضای فراموشی کلمه عبور از جانب شما نبوده است، لطفا روی لینک بالا <u>کلیک ننمایید</u> و فقط این ایمیل را حذف نمایید.<br /><br />» توجه داشته باشید که لینک بالا <b>یکبار مصرف</b> می باشد.</p><br /><p style=\"font-family: tahoma; font-size: 13px;\">با تشکر.<br /><br /><b><a href=\"{1}\">وب سایت {0}</a></b></p></div>", siteSetting.Title, siteSetting.Url, userData.Code, userData.FullName, userData.Username);

            flag = _mailSender.Send(siteSetting.Title, vmForgetPassword.Email, subject, body);
            return flag;
        }


        // get forget password view (for captcha)
        private ActionResult GetForgetPasswordView(ForgetPasswordViewModel vmForgetPassword)
        {
            vmForgetPassword.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(5));

            return View("~/Areas/Account/Views/Home/ForgetPassword.cshtml", vmForgetPassword);
        }


        #endregion


        // -------------------------------- SetPassword
        #region setpassword

        [HttpGet]
        public ActionResult SP(string uc)
        {
            if (uc == null || string.IsNullOrEmpty(uc))
                return HttpNotFound();

            if (!_userService.IsValidActivateCode(uc.Trim()))
                return HttpNotFound();

            var vmSetPassword = new SetPasswordViewModel();
            vmSetPassword.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(5));

            return View("~/Areas/Account/Views/Home/SetPassword.cshtml", vmSetPassword);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SP(SetPasswordViewModel vmSetPassword)
        {
            if (!ModelState.IsValid)
                return GetSetPasswordView(vmSetPassword);

            ModelState.Clear();

            var passwordValidateResult = PasswordValidate.Validate(vmSetPassword.Password);

            if (!SetPasswordViewModelIsValid(vmSetPassword, passwordValidateResult))
                return GetSetPasswordView(vmSetPassword);

            long userId = _userService.GetUserIdByActivateCode(vmSetPassword.Code);

            vmSetPassword.UserId = userId;
            _userService.SetNewPassword(vmSetPassword);
            _uow.SaveChanges();

            return View("~/Areas/Account/Views/Home/SuccessSetPassword.cshtml");
        }

        // setpassword validation
        private bool SetPasswordViewModelIsValid(SetPasswordViewModel vmSetPassword, PasswordValidateResult passwordValidateResult)
        {
            if (!CaptchaManager.IsValid(vmSetPassword.cc, vmSetPassword.Captcha))
            {
                ModelState.AddModelError("", "کد امنیتی نادرست است");
                return false;
            }

            if (vmSetPassword.Password != vmSetPassword.RePassword)
            {
                ModelState.AddModelError("", "کلمه عبور و تکرار کلمه عبور یکسان نیستند");
                return false;
            }

            vmSetPassword.Code = vmSetPassword.Code.Trim();

            if (!_userService.IsValidActivateCode(vmSetPassword.Code))
            {
                ModelState.AddModelError("", "پارامترهای ورودی نامعتبرند");
                return false;
            }


            if (passwordValidateResult == PasswordValidateResult.IsInvalidLengthIsSmall)
            {
                ModelState.AddModelError("", string.Format("کلمه عبور باید حداقل {0} کاراکتر باشد", PasswordValidate.MinimumLength));
                return false;
            }

            if (passwordValidateResult == PasswordValidateResult.IsInvalidNotContainNumber)
            {
                ModelState.AddModelError("", "کلمه عبور باید حاوی یک عدد باشد");
                return false;
            }

            if (passwordValidateResult == PasswordValidateResult.IsValid)
            {
                return true;
            }
            else
            {
                ModelState.AddModelError("", "عملیات ناموفق بود");
                return false;
            }
        }

        // get setpassword view (for captcha)
        private ActionResult GetSetPasswordView(SetPasswordViewModel vmSetPassword)
        {
            vmSetPassword.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(5));

            return View("~/Areas/Account/Views/Home/SetPassword.cshtml", vmSetPassword);
        }

        #endregion


        // -------------------------------- SignOut
        #region signout

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();

            return View();
        }

        #endregion

    }
}