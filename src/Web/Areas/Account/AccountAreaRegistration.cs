﻿using System.Web.Mvc;

namespace Web.Areas.Account
{
    public class AccountAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Account";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
               "Account_singup",
               "Account/signup",
               new { controller = "Home", action = "SignUp" },
               namespaces: new string[] { "Web.Areas.Account.Controllers" }
           );

            context.MapRoute(
                "Account_singin",
                "Account/signin",
                new { controller = "Home", action = "SignIn" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_forgetpassword",
                "Account/forgetpassword",
                new { controller = "Home", action = "ForgetPassword" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_setpassword",
                "Account/SP",
                new { controller = "Home", action = "SP" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_singout",
                "Account/signout",
                new { controller = "Home", action = "SignOut" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_default",
                "Account/{controller}/{action}/{id}",
                new { controller = "Home", action = "SignIn", id = UrlParameter.Optional },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

        }
    }
}