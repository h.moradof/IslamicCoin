﻿using Services.Interfaces;
using System.Web.Mvc;
using Web.Security.Authentication;

namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "RegisteredUsers,PermitedUsers")]
    public class DashboardController : Controller
    {

        #region props
        private readonly IUserService _userService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor

        public DashboardController(
            IUserService userService,
            ICurrentUser currentUser)
        {
            _userService = userService;
            _currentUser = currentUser;
        }

        #endregion


        [HttpGet]
        public ActionResult Show()
        {
            var roleOfUser = _userService.GetRoleOfUser(_currentUser.Id);
            ViewBag.IsUserPermited = (roleOfUser.RoleName == DomainModels.Entities.Access.RoleName.PermitedUsers);

            var model = _userService.GetUserMoneyAccount(_currentUser.Id);
            return View(model);
        }


	}
}