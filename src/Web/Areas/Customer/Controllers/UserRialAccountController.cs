﻿using DatabaseContext.Context;
using DomainModels.Entities.Financial;
using Infrastructure.Common;
using Infrastructure.Exceptions;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.Financial;
using Web.Security.Authentication;

namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "PermitedUsers")]
    public class UserRialAccountController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserRialAccountService _userRialAccountService;
        private readonly IBankService _bankService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public UserRialAccountController(
            IUnitOfWork uow,
            IUserRialAccountService userRialAccountService,
            IBankService bankService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _userRialAccountService = userRialAccountService;
            _bankService = bankService;
            _currentUser = currentUser;
        }
        #endregion




        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Banks = _bankService.GetList();

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerUserRialAccountCreateViewModel model)
        {
            ViewBag.Banks = _bankService.GetList();

            try
            {
                
                if (!ModelState.IsValid)
                    return View(model);

                _userRialAccountService.Add(model, _currentUser.Id);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessInsert.Description() };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }




        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit()
        {
            ViewBag.Banks = _bankService.GetList();

            var model = _userRialAccountService.GetCustomerUserRialAccountEditViewModel(_currentUser.Id);
            if (model == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsDeclined = (_userRialAccountService.GetAcceptStatusName(model.Id) == AccountAcceptStatusName.Declined);

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerUserRialAccountEditViewModel model)
        {
            ViewBag.Banks = _bankService.GetList();
            ViewBag.IsDeclined = (_userRialAccountService.GetAcceptStatusName(model.Id) == AccountAcceptStatusName.Declined);

            try
            {

                if (!ModelState.IsValid)
                    return View(model);

                _userRialAccountService.Edit(model, _currentUser.Id);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }


    }
}