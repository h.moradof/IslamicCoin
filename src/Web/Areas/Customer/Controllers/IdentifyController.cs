﻿using DatabaseContext.Context;
using DomainModels.Entities.HumanResource;
using Infrastructure.Common;
using Infrastructure.Exceptions;
using Infrastructure.File;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.API;
using Web.Security.Attributes;
using Web.Security.Authentication;

namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "RegisteredUsers")]
    public class IdentifyController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public IdentifyController(
            IUnitOfWork uow,
            IUserService userService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _userService = userService;
            _currentUser = currentUser;
        }
        #endregion




        // ---------------------------------------------- Mobile Number
        [HttpGet]
        public ActionResult MobileNumber()
        {
            var user = _userService.Find(_currentUser.Id);

            ViewBag.AcceptStatus = user.MobileNumberAcceptStatus;
            ViewBag.ActivateCode = user.ActivateCode;
            ViewBag.UserMobileNumber = user.Username;

            return View();
        }



        // ---------------------------------------------- Phone Number
        [HttpGet]
        public ActionResult PhoneNumber()
        {
            var user = _userService.Find(_currentUser.Id);

            ViewBag.AcceptStatus = user.PhoneNumberAcceptStatus;
            ViewBag.PhoneNumber = user.PhoneNumber;

            return View();
        }


        // ---------------------------------------------- Identification Card
        [HttpGet]
        public ActionResult IdentificationCard()
        {
            var user = _userService.Find(_currentUser.Id);
            ViewBag.AcceptStatus = user.IdentificationCardAcceptStatus;
            ViewBag.PhotoName = user.IdentificationCardPhotoName;

            try
            {
                if (!user.CanUserEditIdentificationPhoto)
                {
                    throw new CustomException("تصویر شناسنامه (کارت شناسایی) شما در حال بررسی است. لطفا تا مشخص شدن نتیجه منتظر بمانید");
                }

                return View();

            }
            catch (CustomException ex)
            {
                TempData["Message"] = new Message { IsSuccess = false, MessageBody = ex.Message };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowUploadSafeFiles]
        public ActionResult IdentificationCard(string o)
        {
            var user = _userService.Find(_currentUser.Id);
            ViewBag.AcceptStatus = user.IdentificationCardAcceptStatus;
            ViewBag.PhotoName = user.IdentificationCardPhotoName;

            try
            {
                
                if (!user.CanUserEditIdentificationPhoto)
                {
                    throw new CustomException("تصویر شناسنامه (کارت شناسایی) شما در حال بررسی است. لطفا تا مشخص شدن نتیجه منتظر بمانید");
                }

                var photoName = FileManager.UploadFirstValidPhoto(Request.Files, EntityName.Certificate, true);
                user.IdentificationCardPhotoName = photoName;
                user.IdentificationCardAcceptStatus = AcceptStatus.Pending;
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = "تصویر شناسنامه (کارت شناسایی)  با موفقیت ثبت شد" };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }



        // ---------------------------------------------- Face AndN ational Card
        [HttpGet]
        public ActionResult FaceAndNationalCard()
        {
            var user = _userService.Find(_currentUser.Id);
            ViewBag.AcceptStatus = user.FaceAndNationalCardAcceptStatus;
            ViewBag.PhotoContent = user.FaceAndNationalCardPhotoName;

            try
            {
                if (!user.CanUserEditFaceAndNationalCardPhoto)
                {
                    throw new CustomException("تصویر کارت ملی و صورت شما در حال بررسی است. لطفا تا مشخص شدن نتیجه منتظر بمانید");
                }

                return View();
            }
            catch (CustomException ex)
            {
                TempData["Message"] = new Message { IsSuccess = false, MessageBody = ex.Message };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
        }


        [HttpPost]
        public JsonResult FaceAndNationalCard(string base64PhotoContent)
        {
            try
            {
                // ثبت تصویر با وبکم
                var user = _userService.Find(_currentUser.Id);
                if (!user.CanUserEditFaceAndNationalCardPhoto)
                {
                    throw new CustomException("تصویر چهره و کارت ملی شما در حال بررسی است. لطفا تا مشخص شدن نتیجه منتظر بمانید");
                }

                user.FaceAndNationalCardPhotoName = base64PhotoContent;
                user.FaceAndNationalCardAcceptStatus = AcceptStatus.Pending;
                _uow.SaveChanges();

                return Json(new ApiResultViewModel ());
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HaveError = true, Message = ex.Message });
            }
        }

    }
}