﻿using DatabaseContext.Context;
using Infrastructure.Common;
using Infrastructure.Messages;
using Services.Interfaces;
using System;
using System.Web.Mvc;
using ViewModels.Support;
using Web.Security.Authentication;

namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "RegisteredUsers,PermitedUsers")]
    public class TicketController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly ITicketService _ticketService;
        private readonly ITicketReplyService _ticketReplyService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public TicketController(
            IUnitOfWork uow, 
            ITicketService ticketService, 
            ITicketReplyService ticketReplyService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _ticketService = ticketService;
            _ticketReplyService = ticketReplyService;
            _currentUser = currentUser;
        }
        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var tickets = _ticketService.GetAll(page.Value, _currentUser.Id, page.Value);

            return View(tickets);
        }


        // ---------------------------------------------- Detail
        [HttpGet]
        public ActionResult Detail(long id)
        {
            try
            {
                var vmTicketDetail = _ticketService.GetTicketDetailViewModel(id, _currentUser.Id);

                return View(vmTicketDetail);
            }
            catch (UnauthorizedAccessException ex)
            {
                TempData["Message"] = new Message { IsSuccess = true, MessageBody = ex.Message };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
        }



        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TicketCreateViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _ticketService.Add(model, _currentUser.Id);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessInsert.Description() };
            return RedirectToAction("Index");
        }





        // ---------------------------------------------- _Reply
        [ChildActionOnly]
        [HttpGet]
        public PartialViewResult _Reply(long ticketId)
        {
            return PartialView(new TicketReplyCreateViewModel { TicketId = ticketId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Reply(TicketReplyCreateViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return PartialView(model);

                model.IsOpen = true;
                _ticketReplyService.Add(model, _currentUser.Id);
                _uow.SaveChanges();

                return RedirectToAction("Detail", "Ticket", new { area = "Customer", id = model.TicketId });
            }
            catch (UnauthorizedAccessException ex)
            {
                TempData["Message"] = new Message { IsSuccess = false, MessageBody = ex.Message };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
        }


    }
}