﻿using DatabaseContext.Context;
using DomainModels.Entities.Financial;
using Infrastructure.Common;
using Infrastructure.Exceptions;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.Financial;
using Web.Security.Authentication;

namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "PermitedUsers")]
    public class UserRialPaymentController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserRialPaymentService _userRialPaymentService;
        private readonly IUserRialAccountService _userRialAccountService;
        private readonly IAdminRialAccountService _adminRialAccountService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public UserRialPaymentController(
            IUnitOfWork uow,
            IUserRialPaymentService userRialPaymentService,
            IUserRialAccountService userRialAccountService,
            IAdminRialAccountService adminRialAccountService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _userRialPaymentService = userRialPaymentService;
            _userRialAccountService = userRialAccountService;
            _adminRialAccountService = adminRialAccountService;
            _currentUser = currentUser;
        }
        #endregion




        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var list = _userRialPaymentService.GetAllByCustomerId(_currentUser.Id, page.Value);
            return View(list);
        }


        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            #region validate rial account
            var userRialAccountStatus = _userRialAccountService.GetUserRialAccountStatus(_currentUser.Id);
            switch (userRialAccountStatus)
            {
                case UserRialAccountStatusViewModel.DontFindAnyAccount:
                    return RedirectToAction("Create", "UserRialAccount", new { area = "Customer" });
                case UserRialAccountStatusViewModel.AccountIsPending:
                    TempData["Message"] = new Message { IsSuccess = true, MessageBody = "حساب بانکی شما در حال بررسی است. لطفا تا تایید آن منتظر بمانید" };
                    return RedirectToAction("Show", "Message", new { area = "Customer" });
                case UserRialAccountStatusViewModel.AccountIsDeclined:
                    return RedirectToAction("Edit", "UserRialAccount", new { area = "Customer" });
            }
            #endregion

            ViewBag.AcceptedUserAccount = new UserRialAccount[] { _userRialAccountService.GetFirstAcceptedUserRialAccount(_currentUser.Id) };
            ViewBag.AdminRialAccount = _adminRialAccountService.GetFirstRow();

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerUserRialPaymentCreateViewModel model)
        {
            try
            {

                #region validate rial account
                var userRialAccountStatus = _userRialAccountService.GetUserRialAccountStatus(_currentUser.Id);
                switch (userRialAccountStatus)
                {
                    case UserRialAccountStatusViewModel.DontFindAnyAccount:
                        return RedirectToAction("Create", "UserRialAccount", new { area = "Customer" });
                    case UserRialAccountStatusViewModel.AccountIsPending:
                        TempData["Message"] = new Message { IsSuccess = true, MessageBody = "حساب بانکی شما در حال بررسی است. لطفا تا تایید آن منتظر بمانید" };
                        return RedirectToAction("Show", "Message", new { area = "Customer" });
                    case UserRialAccountStatusViewModel.AccountIsDeclined:
                        return RedirectToAction("Edit", "UserRialAccount", new { area = "Customer" });
                }
                #endregion


                ViewBag.AcceptedUserAccount = new UserRialAccount[] { _userRialAccountService.GetFirstAcceptedUserRialAccount(_currentUser.Id) };
                ViewBag.AdminRialAccount = _adminRialAccountService.GetFirstRow();

                if (!ModelState.IsValid)
                    return View(model);

                _userRialPaymentService.Add(model, _currentUser.Id);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = "درخواست شما با موفقیت ثبت گردید. کارشناسان ما پرداخت شما را بررسی و در اولین فرصت نسبت به تایید پرداخت شما اقدام خواهند نمود. گفتنی است، بررسی درخواست در کمتر از 3 ساعت انجام خواهد شد" };
                return RedirectToAction("Index");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

    }
}