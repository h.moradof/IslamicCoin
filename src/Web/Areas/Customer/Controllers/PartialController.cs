﻿using Services.Interfaces;
using System.Web.Mvc;
using Web.Security.Authentication;

namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "RegisteredUsers,PermitedUsers")]
    public class PartialController : Controller
    {

        #region props
        private readonly IUserService _userService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public PartialController(
            IUserService userService,
            ICurrentUser currentUser)
        {
            _userService = userService;
            _currentUser = currentUser;
        }
        #endregion


        [ChildActionOnly]
        public PartialViewResult _TopMenu()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public PartialViewResult _RightMenu()
        {
            var RoleOfCurrentUser = _userService.GetRoleOfUser(_currentUser.Id);

            return PartialView(RoleOfCurrentUser.RoleName);
        }


        [ChildActionOnly]
        public PartialViewResult _DashboardTitle()
        {
            return PartialView();
        }

    }
}