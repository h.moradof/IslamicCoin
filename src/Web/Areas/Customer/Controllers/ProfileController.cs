﻿using DatabaseContext.Context;
using Infrastructure.Common;
using Infrastructure.Messages;
using Services.Interfaces;
using ViewModels.Account;
using Web.Security;
using Web.Security.Authentication;
using System.Web.Mvc;
using System.Web.Security;


namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "RegisteredUsers,PermitedUsers")]
    public class ProfileController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        private readonly ICurrentUser _currentUser;

        #endregion

        #region ctor

        public ProfileController(
            IUnitOfWork uow,
            IUserService userService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _userService = userService;
            _currentUser = currentUser;
        }

        #endregion


        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit()
        {
            var vmProfile = _userService.GetProfile(_currentUser.Id);

            if (vmProfile == null)
                return HttpNotFound();

            return View(vmProfile);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProfileViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            ModelState.Clear();

            model.UserId = _currentUser.Id;

            // validate username and password
            if (!IsValidProfileViewModel(model))
                return View(model);

            _userService.EditProfile(model);
            _uow.SaveChanges();

            if (model.WantUpdateUsername)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("SignIn", "Account", new { area = "" });
            }
            else
            {
                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
                return RedirectToAction("Show", "Message", new { area = "Customer" });
            }
        }


        private bool IsValidProfileViewModel(ProfileViewModel vmProfile)
        {

            // check username
            if (vmProfile.WantUpdateUsername)
            {

                if (string.IsNullOrEmpty(vmProfile.Username))
                {
                    ModelState.AddModelError("", "نام کاربری الزامی است");
                    return false;
                }


                if (_userService.HaveSameUsername(vmProfile.Username, vmProfile.UserId))
                {
                    ModelState.AddModelError("", "نام کاربری در سیستم موجود است، لطفا نام کاربری دیگری برگزینید");
                    return false;
                }
            }


            // check password
            if (vmProfile.WantUpdatePassword)
            {
                if (string.IsNullOrEmpty(vmProfile.Password))
                {
                    ModelState.AddModelError("", "کلمه عبور الزامی است");
                    return false;
                }

                if (vmProfile.Password != vmProfile.RePassword)
                {
                    ModelState.AddModelError("", "کلمه عبور و تکرار کلمه عبور یکسان نیستند");
                    return false;
                }

                var passwordValidateResult = PasswordValidate.Validate(vmProfile.Password);

                if (passwordValidateResult == PasswordValidateResult.IsInvalidLengthIsSmall)
                {
                    ModelState.AddModelError("", string.Format("کلمه عبور باید حداقل {0} کاراکتر باشد", PasswordValidate.MinimumLength));
                    return false;
                }
                else if (passwordValidateResult == PasswordValidateResult.IsInvalidNotContainNumber)
                {
                    ModelState.AddModelError("", "کلمه عبور باید حاوی یک عدد باشد");
                    return false;
                }
            }

            return true;
        }

    }
}