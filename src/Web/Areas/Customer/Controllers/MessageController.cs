﻿using System.Web.Mvc;

namespace Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "RegisteredUsers,PermitedUsers")]
    public class MessageController : Controller
    {
        
        public ActionResult Show()
        {
            return View();
        }
	}
}