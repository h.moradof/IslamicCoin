﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.File;
using Web.DependencyResolution;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{

    public static class LinkHelper
    {

        public static MvcHtmlString ShowShareLisks(this HtmlHelper htmlHelper, string url, string title, string shareText)
        {
            const string pattern = @"
            <div class='share-links-wrap'>
				<span class='share-text'>
                    <i class='fa fa-share-alt'></i>
                    {2}
                </span>
                <span>
					<a target='_blank' title='share on linkedin' href='https://www.linkedin.com/shareArticle?url={0}'>
						<i class='fa fa-linkedin'></i>
					</a>
				</span>
				<span>
					<a target='_blank' title='share on facebook' href='http://www.facebook.com/sharer/sharer.php?u={0}'>
						<i class='fa fa-facebook'></i>
					</a>
				</span>
				<span>
					<a target='_blank' title='share on twitter' href='http://twitter.com/share?text={1}&amp;url={0}'>
						<i class='fa fa-twitter'></i>
					</a>
				</span>
				<span>
					<a target='_blank' title='share on google plus' href='https://plus.google.com/share?url={0}'>
						<i class='fa fa-google-plus'></i>
					</a>
				</span>
			</div>";

            var siteSeoSettingCacheManager = StructureMapObjectFactory.Container.GetInstance<ISiteSeoSettingCacheManager>();

            string siteBaseUrl = siteSeoSettingCacheManager.GetDefaultSetting().Url;
            string fullUrlPattern = (siteBaseUrl.EndsWith("/") || url.StartsWith("/")) ? "{0}{1}" : "{0}/{1}";

            string fullUrl = string.Format(fullUrlPattern, siteBaseUrl, url);

            return MvcHtmlString.Create(string.Format(pattern, fullUrl, title, shareText));
        }



        // --------------------------------------------- [Show direct link] ---------------------------------------------
        public static MvcHtmlString ShowFileDirectLink(
            this HtmlHelper htmlHelper,
            string linkText,
            string fileName,
            EntityName entityName)
        {
            var aTag = new TagBuilder("a");

            aTag.InnerHtml = linkText;

            var fileUploadPath = UploadFilePathManager.GetUploadPath(entityName).Replace("~", "");
            aTag.MergeAttribute("href", string.Format("{0}{1}", fileUploadPath, fileName));

            return MvcHtmlString.Create(aTag.ToString());
        }


        public static MvcHtmlString ShowFileDirectLink(
            this HtmlHelper htmlHelper,
            string linkText,
            string fileName,
            EntityName entityName,
            object htmlAttributes)
        {
            var aTag = new TagBuilder("a");

            aTag.InnerHtml = linkText;

            var fileUploadPath = UploadFilePathManager.GetUploadPath(entityName).Replace("~", "");
            aTag.MergeAttribute("href", string.Format("{0}{1}", fileUploadPath, fileName));

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    aTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            return MvcHtmlString.Create(aTag.ToString());
        }

    }

}