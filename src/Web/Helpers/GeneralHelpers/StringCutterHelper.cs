﻿using Infrastructure.Common;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{

    public static class StringCutterHelper
    {
        public static MvcHtmlString ShowCuttedString(
            this HtmlHelper htmlHelper,
            string value,
            int cutLength)
        {
            return MvcHtmlString.Create(value.Cut(cutLength, " ..."));
        }
    }
}