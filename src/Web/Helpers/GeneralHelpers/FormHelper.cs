﻿using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class FormHelper
    {

        private const string FORM_PATTERN = "<form action='/{0}/{1}/{2}' method='post' class='{3}'>{4}"
                    + "<input type='hidden' name='{5}' value='{6}' />"
                    + "<input type='submit' value='{7}' class='{8}' />"
                    + "</form>";

        public static MvcHtmlString ShowForm(this HtmlHelper htmlHelper,
            string hiddenFieldName, string hiddenFieldValue,
            string btnText, string btnCssClass,
            string actionName, string controllerName, string areaName,
            string formCssClass,
            bool isUseAntiForgeryToken = true)
        {

            var antiForgeryToken = isUseAntiForgeryToken ? htmlHelper.AntiForgeryToken() : MvcHtmlString.Create(string.Empty);

            string form = string.Format(FORM_PATTERN,
                areaName, controllerName, actionName,
                formCssClass,
                antiForgeryToken,
                hiddenFieldName, hiddenFieldValue,
                btnText, btnCssClass);

            return MvcHtmlString.Create(form);
        }
    }
}