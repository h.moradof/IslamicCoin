﻿using Infrastructure.File;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class FileHelper
    {
        public static MvcHtmlString ShowValidFileExtensions(this HtmlHelper htmlHelper)
        {
            return MvcHtmlString.Create(string.Format(" فرمت های مجاز برای فایل <b class='tr'>{0}</b> می باشد", FileValidator.ValidFileExtensions));
        }


        public static MvcHtmlString ShowValidPictureExtensions(this HtmlHelper htmlHelper)
        {
            return MvcHtmlString.Create(string.Format(" فرمت های مجاز برای تصویر <b class='tr'>{0}</b> می باشد", FileValidator.ValidPictureExtensions));
        }

    }
}