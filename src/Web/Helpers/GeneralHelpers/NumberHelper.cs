﻿using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{

    public static class NumberHelper
    {

        public static MvcHtmlString ShowNumberWithCommaSeperator(this HtmlHelper htmlHelper, long number)
        {
            return MvcHtmlString.Create(number.ToString("#,##0", new System.Globalization.CultureInfo("en-US")));
        }

        public static MvcHtmlString ShowNumberWithCommaSeperator(this HtmlHelper htmlHelper, int number)
        {
            return MvcHtmlString.Create(number.ToString("#,##0", new System.Globalization.CultureInfo("en-US")));
        }

    }
}