﻿using System.Threading;
using System.Web.Mvc;


namespace Web.Helpers.GeneralHelpers
{

    public static class MetaTagHelper
    {
        public static MvcHtmlString ShowCanonicalMetaTag(this HtmlHelper htmlHelper, string siteUrl)
        {
            var metaTag = new TagBuilder("meta");
            metaTag.MergeAttribute("rel", "canonical");
            string url = siteUrl;

            if (!siteUrl.Contains("www"))
            {
                url = siteUrl.Replace("http://", "http://www.");
            }

            metaTag.MergeAttribute("href", url);
            return MvcHtmlString.Create(metaTag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString ShowContentLanguageMetaTag(this HtmlHelper htmlHelper)
        {
            string currentLanguage = Thread.CurrentThread.CurrentCulture.Parent.Name;

            var metaTag = new TagBuilder("meta");
            metaTag.MergeAttribute("http-equiv", "Content-Language");
            metaTag.MergeAttribute("content", currentLanguage);
            return MvcHtmlString.Create(metaTag.ToString(TagRenderMode.SelfClosing));
        }

    }

}