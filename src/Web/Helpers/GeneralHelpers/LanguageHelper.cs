﻿using System.Threading;
using System.Web.Mvc;


namespace Web.Helpers.GeneralHelpers
{
    public static class LanguageHelper
    {
        public static string GetCurrentLanguage(this HtmlHelper htmlHelper)
        {
            string currentLanguage = Thread.CurrentThread.CurrentCulture.Parent.Name;
            return currentLanguage;
        }
    }
}