﻿using Infrastructure.Pagination.Base;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Collections;
using System;

namespace Web.Helpers.GeneralHelpers
{
    /// <summary>
    /// Create Page Links
    /// Copyright 2016 Moradof.ir
    /// </summary>
    public static class PagerHelper
    {
        const int TotalShowPagerLinks = 10;

        public static MvcHtmlString ShowEmptyLabel(this HtmlHelper htmlHelper, dynamic model)
        {
            if ((model as IEnumerable) == null)
                throw new ArgumentException("model should be IEnumerable");

            if(model.Count > 0)
                return MvcHtmlString.Create(string.Empty);

            return MvcHtmlString.Create("<p class='empty-label'>موردی یافت نشد ...<p>");
        }

        public static MvcHtmlString ShowPagerDescription(
            this HtmlHelper htmlHelper,
            BasePagedList model)
        {
            if (model == null || model.PageCount == 0 || model.PageCount < 2)
                return MvcHtmlString.Create(string.Empty);

            var pagerDescription = new TagBuilder("div");
            pagerDescription.MergeAttribute("class", "pager-description");
            pagerDescription.InnerHtml = string.Format("صفحه {0} از {1}", model.PageNumber, model.PageCount);

            return MvcHtmlString.Create(pagerDescription.ToString());
        }

        public static MvcHtmlString ShowPager(
            this HtmlHelper htmlHelper,
            BasePagedList model,
            string url,
            bool haveUrlQueryString = false)
        {
            if (model == null)
                return MvcHtmlString.Create(string.Empty);

            return ShowPager(htmlHelper, url, model.PageNumber, model.PageSize, model.TotalItemCount, haveUrlQueryString);
        }

        private static MvcHtmlString ShowPager(
            this HtmlHelper htmlHelper,
            string url,
            int currentPageNumber, int countPerPage, int totalRowCount,
            bool haveUrlQueryString)
        {
            var containerTag = new TagBuilder("div");
            containerTag.MergeAttribute("class", "pager-container");
            containerTag.InnerHtml = string.Empty;

            int neededPages = CalcNeededPages(countPerPage, totalRowCount);

            if (neededPages == 1)
                return MvcHtmlString.Create(string.Empty);

            var hrefPattern = haveUrlQueryString ? "{0}&page={1}" : "{0}/?page={1}";

            if (currentPageNumber > 1)
                CreateFirstArrowLink(ref containerTag, url, currentPageNumber, hrefPattern);

            if (neededPages <= TotalShowPagerLinks)
            {
                CreateLinks_ZeroToTen(ref containerTag, url, currentPageNumber, neededPages, hrefPattern);
            }
            else
            {
                if (currentPageNumber < TotalShowPagerLinks)
                {
                    CreateLinks_ZeroToTotalShowPagerLinks(ref containerTag, url, currentPageNumber, neededPages, hrefPattern);
                }
                else if (currentPageNumber > (neededPages - TotalShowPagerLinks))
                {
                    CreateLinks_TenToEnd_ToEnd(ref containerTag, url, currentPageNumber, neededPages, hrefPattern);
                }
                else
                {
                    CreateLinks_OverCurrentPage(ref containerTag, url, currentPageNumber, neededPages, hrefPattern);
                }
            }

            if (currentPageNumber < neededPages)
                CreateLastArrowLink(ref containerTag, url, currentPageNumber, hrefPattern);


            return MvcHtmlString.Create(containerTag.ToString());
        }


        #region private methods
        private static int CalcNeededPages(int countPerPage, int totalRowCount)
        {
            int neededPages = totalRowCount / countPerPage;
            if (totalRowCount % countPerPage > 0)
                neededPages++;
            return neededPages;
        }

        private static void CreateFirstArrowLink(ref TagBuilder containerTag, string url, int currentPageNumber, string hrefPattern)
        {
            var arrowLink = new TagBuilder("a");
            arrowLink.MergeAttribute("class", "pager-link");
            arrowLink.MergeAttribute("href", string.Format(hrefPattern, url, currentPageNumber - 1));
            arrowLink.InnerHtml = "«";
            containerTag.InnerHtml += arrowLink.ToString();
        }

        private static void CreateLastArrowLink(ref TagBuilder containerTag, string url, int currentPageNumber, string hrefPattern)
        {
            var arrowLink = new TagBuilder("a");
            arrowLink.MergeAttribute("class", "pager-link");
            arrowLink.MergeAttribute("href",  string.Format(hrefPattern, url, currentPageNumber + 1));
            arrowLink.InnerHtml = "»";
            containerTag.InnerHtml += arrowLink.ToString();
        }

        private static void CreateLinks_ZeroToTen(ref TagBuilder containerTag, string url, int currentPageNumber, int neededPages, string hrefPattern)
        {
            for (int i = 1; i <= neededPages; i++)
            {
                var pageLink = new TagBuilder("a");
                pageLink.MergeAttribute("class", i == currentPageNumber ? "pager-link-active" : "pager-link");
                pageLink.MergeAttribute("href", string.Format(hrefPattern, url, i));

                pageLink.InnerHtml = i.ToString();

                containerTag.InnerHtml += pageLink.ToString();
            }
        }

        private static void CreateLinks_ZeroToTotalShowPagerLinks(ref TagBuilder containerTag, string url, int currentPageNumber, int neededPages, string hrefPattern)
        {
            for (int i = 1; i <= TotalShowPagerLinks; i++)
            {
                var pageLink = new TagBuilder("a");
                pageLink.MergeAttribute("class", i == currentPageNumber ? "pager-link-active" : "pager-link");
                pageLink.MergeAttribute("href", string.Format(hrefPattern, url, i));
                pageLink.InnerHtml = i.ToString();

                containerTag.InnerHtml += pageLink.ToString();
            }

            // seperator
            containerTag.InnerHtml += GetSeperatorLink();

            // last
            containerTag.InnerHtml += GetLastLink(url, neededPages, hrefPattern);
        }

        private static void CreateLinks_TenToEnd_ToEnd(ref TagBuilder containerTag, string url, int currentPageNumber, int neededPages, string hrefPattern)
        {
            // first
            containerTag.InnerHtml += GetFirstLink(url, hrefPattern);

            // seperator
            containerTag.InnerHtml += GetSeperatorLink();

            // other links
            for (int i = neededPages - TotalShowPagerLinks; i <= neededPages; i++)
            {
                var pageLink = new TagBuilder("a");
                pageLink.MergeAttribute("class", i == currentPageNumber ? "pager-link-active" : "pager-link");
                pageLink.MergeAttribute("href", string.Format(hrefPattern, url, i));
                pageLink.InnerHtml = i.ToString();

                containerTag.InnerHtml += pageLink.ToString();
            }
        }

        private static void CreateLinks_OverCurrentPage(ref TagBuilder containerTag, string url, int currentPageNumber, int neededPages, string hrefPattern)
        {
            // first
            containerTag.InnerHtml += GetFirstLink(url, hrefPattern);

            // seperator
            containerTag.InnerHtml += GetSeperatorLink();

            // other links
            for (int i = currentPageNumber - (TotalShowPagerLinks / 2); i <= currentPageNumber + (TotalShowPagerLinks / 2); i++)
            {
                var pageLink = new TagBuilder("a");
                pageLink.MergeAttribute("class", i == currentPageNumber ? "pager-link-active" : "pager-link");
                pageLink.MergeAttribute("href", string.Format(hrefPattern, url, i));
                pageLink.InnerHtml = i.ToString();

                containerTag.InnerHtml += pageLink.ToString();
            }

            // seperator
            containerTag.InnerHtml += GetSeperatorLink();

            // last
            containerTag.InnerHtml += GetLastLink(url, neededPages, hrefPattern);
        }


        private static string GetFirstLink(string url, string hrefPattern)
        {
            var firstPageLink = new TagBuilder("a");
            firstPageLink.MergeAttribute("class", "pager-link");
            firstPageLink.MergeAttribute("href", string.Format(hrefPattern, url, 1));
            firstPageLink.InnerHtml = "1";
            return firstPageLink.ToString();
        }

        private static string GetLastLink(string url, int neededPages, string hrefPattern)
        {
            var lastPageLink = new TagBuilder("a");
            lastPageLink.MergeAttribute("class", "pager-link");
            lastPageLink.MergeAttribute("href", string.Format(hrefPattern, url, neededPages)); ;
            lastPageLink.InnerHtml = neededPages.ToString();
            return lastPageLink.ToString();
        }

        private static string GetSeperatorLink()
        {
            var seperatorLink = new TagBuilder("a");
            seperatorLink.MergeAttribute("class", "pager-seperator");
            seperatorLink.MergeAttribute("disabled", "disabled");
            seperatorLink.MergeAttribute("href", "javascript:void(0)");
            seperatorLink.InnerHtml = "...";

            return seperatorLink.ToString();
        }

        #endregion

    }
}