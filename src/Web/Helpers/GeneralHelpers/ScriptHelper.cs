﻿using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class ScriptHelper
    {

        public static MvcHtmlString ShowScript(this HtmlHelper htmlHelper, string scriptName)
        {
            TagBuilder scriptTag = new TagBuilder("script");
            scriptTag.MergeAttribute("src", "/Scripts/" + scriptName);
            scriptTag.MergeAttribute("type", "text/javascript");

            return MvcHtmlString.Create(scriptTag.ToString());
        }

    }
}