﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class StatusHelper
    {

        public static MvcHtmlString ShowStatus(this HtmlHelper htmlHelper, string statusName, object htmlAttributes)
        {
            var spanTag = new TagBuilder("span");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    spanTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            spanTag.InnerHtml = statusName;

            return MvcHtmlString.Create(spanTag.ToString());
        }
    }
}