﻿using Infrastructure.Common;
using System;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class DateTimeHelper
    {

        #region miladi

        public static MvcHtmlString ShowMiladiDateTime(
            this HtmlHelper htmlHelper,
            DateTime miladiDate)
        {
            var span = new TagBuilder("div");
            span.MergeAttribute("class", "shamsi-date");
            span.InnerHtml = miladiDate.ToMiladiDateTime();
            return MvcHtmlString.Create(span.ToString());
        }


        public static MvcHtmlString ShowMiladiDateTime(
            this HtmlHelper htmlHelper,
            DateTime? miladiDate)
        {
            var span = new TagBuilder("div");
            span.MergeAttribute("class", "shamsi-date");
            span.InnerHtml = miladiDate.ToMiladiDateTime();
            return MvcHtmlString.Create(span.ToString());
        }

        #endregion


        #region shamsi

        public static MvcHtmlString ShowDateTime(
            this HtmlHelper htmlHelper,
            DateTime miladiDate,
            ShamsiDateFormatType outputType,
            bool convertToPersiamNumber = false)
        {
            var output = string.Empty;

            switch (outputType)
            {
                case ShamsiDateFormatType.ToPersian:
                    output = miladiDate.ToPersian();
                    break;
                case ShamsiDateFormatType.ToPersianWithTime:
                    output = miladiDate.ToPersianWithTime();
                    break;
                case ShamsiDateFormatType.ToPersianMonthChar:
                    output = miladiDate.ToPersianMonthChar();
                    break;
                case ShamsiDateFormatType.ToPersianChar:
                    output = miladiDate.ToPersianChar();
                    break;
                case ShamsiDateFormatType.ToPersianChar2:
                    output = miladiDate.ToPersianChar2();
                    break;
                case ShamsiDateFormatType.ToPersianPro:
                    output = miladiDate.ToPersianPro();
                    break;
                default:
                    output = "---";
                    break;
            }

            var span = new TagBuilder("div");
            span.MergeAttribute("class", "shamsi-date");
            span.InnerHtml = convertToPersiamNumber ? output.ToPersianNumber() : output;
            return MvcHtmlString.Create(span.ToString());
        }


        public static MvcHtmlString ShowDateTime(
            this HtmlHelper htmlHelper,
            DateTime? miladiDate,
            ShamsiDateFormatType outputType,
            bool convertToPersiamNumber = false)
        {
            var output = string.Empty;
            
            switch (outputType)
            {
                case ShamsiDateFormatType.ToPersian:
                    output = miladiDate.ToPersian();
                    break;
                case ShamsiDateFormatType.ToPersianWithTime:
                    output = miladiDate.ToPersianWithTime();
                    break;
                case ShamsiDateFormatType.ToPersianMonthChar:
                    output = miladiDate.ToPersianMonthChar();
                    break;
                case ShamsiDateFormatType.ToPersianChar:
                    output = miladiDate.ToPersianChar();
                    break;
                case ShamsiDateFormatType.ToPersianChar2:
                    output = miladiDate.ToPersianChar2();
                    break;
                case ShamsiDateFormatType.ToPersianPro:
                    output = miladiDate.ToPersianPro();
                    break;
                default:
                    output = "---";
                    break;
            }

            var span = new TagBuilder("div");
            span.MergeAttribute("class", "shamsi-date");
            span.InnerHtml = convertToPersiamNumber ? output.ToPersianNumber() : output;
            return MvcHtmlString.Create(span.ToString());
        }

        #endregion


        #region hijri

        public static MvcHtmlString ShowDateTime(
            this HtmlHelper htmlHelper,
            DateTime miladiDate,
            HijriDateFormatType outputType)
        {
            var output = string.Empty;

            switch (outputType)
            {
                case HijriDateFormatType.ToHijri:
                    output = miladiDate.ToHijri();
                    break;
                case HijriDateFormatType.ToHijriWithTime:
                    output = miladiDate.ToHijriWithTime();
                    break;
                default:
                    output = "---";
                    break;
            }

            var span = new TagBuilder("div");
            span.MergeAttribute("class", "shamsi-date");
            span.InnerHtml = output;
            return MvcHtmlString.Create(span.ToString());
        }


        public static MvcHtmlString ShowDateTime(
            this HtmlHelper htmlHelper,
            DateTime? miladiDate,
            HijriDateFormatType outputType)
        {
            var output = string.Empty;

            switch (outputType)
            {
                case HijriDateFormatType.ToHijri:
                    output = miladiDate.ToHijri();
                    break;
                case HijriDateFormatType.ToHijriWithTime:
                    output = miladiDate.ToHijriWithTime();
                    break;
                default:
                    output = "---";
                    break;
            }

            var span = new TagBuilder("div");
            span.MergeAttribute("class", "shamsi-date");
            span.InnerHtml = output;
            return MvcHtmlString.Create(span.ToString());
        }


        #endregion

    }
}