﻿using Infrastructure.Cache;
using Infrastructure.Cache.Interfaces;
using Infrastructure.File;
using Web.DependencyResolution;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{

    public static class ImageHelper
    {


        // --------------------------------------------- [Show Image] ---------------------------------------------
        public static MvcHtmlString ShowImage(
            this HtmlHelper htmlHelper,
            string imageFileName,
            EntityName entityName)
        {
            return ShowImageByMvcAction(htmlHelper, imageFileName, entityName);
        }


        public static MvcHtmlString ShowImage(
            this HtmlHelper htmlHelper,
            string imageFileName,
            EntityName entityName,
            object htmlAttributes)
        {
            return ShowImageByMvcAction(htmlHelper, imageFileName, entityName, htmlAttributes);
        }



        public static MvcHtmlString ShowImage(
            this HtmlHelper htmlHelper,
            string imageFileName)
        {
            return ShowImageByMvcAction(htmlHelper, imageFileName);
        }


        public static MvcHtmlString ShowImage(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes)
        {
            return ShowImageByMvcAction(htmlHelper, imageFileName, htmlAttributes);
        }






        // --------------------------------------------- [Show Image By Mvc Action] ---------------------------------------------

        private const string actionName = "Image";
        private const string controllerName = "File";


        public static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            EntityName entityName)
        {
            var imgTag = new TagBuilder("img");

            imgTag.MergeAttribute("src", string.Format("/{0}/{1}/?fileName={2}&entityName={3}", controllerName, actionName, imageFileName, entityName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }


        public static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            EntityName entityName,
            object htmlAttributes)
        {
            var imgTag = new TagBuilder("img");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            imgTag.MergeAttribute("src", string.Format("/{0}/{1}/?fileName={2}&entityName={3}", controllerName, actionName, imageFileName, entityName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }




        public static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName)
        {
            var imgTag = new TagBuilder("img");

            imgTag.MergeAttribute("src", string.Format("/{0}/{1}/?fileName={2}", controllerName, actionName, imageFileName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }


        public static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes)
        {
            var imgTag = new TagBuilder("img");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            imgTag.MergeAttribute("src", string.Format("/{0}/{1}/?fileName={2}", controllerName, actionName, imageFileName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }


        public static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes,
            string areaName)
        {
            var imgTag = new TagBuilder("img");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            if (string.IsNullOrEmpty(areaName))
                imgTag.MergeAttribute("src", string.Format("/{0}/{1}/?fileName={2}", controllerName, actionName, imageFileName));
            else
                imgTag.MergeAttribute("src", string.Format("/{0}/{1}/{2}/?fileName={3}", areaName, controllerName, actionName, imageFileName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }



        public static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes,
            string areaName,
            string controllerName,
            string actionName)
        {
            var imgTag = new TagBuilder("img");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            if (string.IsNullOrEmpty(areaName))
                imgTag.MergeAttribute("src", string.Format("/{0}/{1}/?fileName={2}", controllerName, actionName, imageFileName));
            else
                imgTag.MergeAttribute("src", string.Format("/{0}/{1}/{2}/?fileName={3}", areaName, controllerName, actionName, imageFileName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }


    }
}