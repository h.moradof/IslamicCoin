﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.Reflection;
using System.Collections.Generic;
using System.Web.Mvc.Html;
using Infrastructure.Common;

namespace Web.Helpers.GeneralHelpers
{
    public static class EnumHelper
    {

        /// <summary>
        /// Show enum description attribute
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static MvcHtmlString ShowEnumDescription(this HtmlHelper htmlHelper, Enum value)
        {
            return MvcHtmlString.Create(value.Description());
        }


        #region EnumDropDownListFor

        private static readonly SelectListItem[] SingleEmptyItem = new[] { new SelectListItem { Text = "", Value = "" } };

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }

        private static string GetEnumDescription<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString();
        }


        /// <summary>
        /// Create DropDownMenu From Enum
        /// </summary>
        /// <param name="expression">lambda expression to enum property</param>
        public static MvcHtmlString CustomEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
        {
            return ShowEnumDropDownListFor(htmlHelper, expression, null);
        }

        /// <summary>
        /// Create DropDownMenu From Enum
        /// </summary>
        /// <param name="expression">lambda expression to enum property</typeparam>
        /// <param name="htmlAttributes">html attributes</param>
        public static MvcHtmlString ShowEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            IEnumerable<SelectListItem> items = from value in values
                                                select new SelectListItem
                                                {
                                                    Text = GetEnumDescription(value),
                                                    Value = value.ToString(),
                                                    Selected = value.Equals(metadata.Model)
                                                };

            // If the enum is nullable, add an 'empty' item to the collection
            if (metadata.IsNullableValueType)
                items = SingleEmptyItem.Concat(items);

            return htmlHelper.DropDownListFor(expression, items, htmlAttributes);
        }


        /// <summary>
        /// Create DropDownMenu From Enum
        /// </summary>
        /// <param name="expression">lambda expression to enum property</typeparam>
        /// <param name="htmlAttributes">html attributes</param>
        public static MvcHtmlString ShowEnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name , object htmlAttributes)
        {
            Type enumType = typeof(TEnum);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            IEnumerable<SelectListItem> items = from value in values
                                                select new SelectListItem
                                                {
                                                    Text = GetEnumDescription(value),
                                                    Value = value.ToString()
                                                };

            
            return htmlHelper.DropDownList(name, items, htmlAttributes);
        }

        #endregion



        #region CheckBoxesForEnumFlagsFor

        /// <summary>
        /// create multi selected checkboxlist for Enums with [Flags] Attribute
        /// </summary>
        /// <typeparam name="TModel">model with Enum property</typeparam>
        /// <typeparam name="TEnum">Enum property</typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">lambda expression for Enum property</param>
        /// <returns></returns>
        public static IHtmlString ShowCheckBoxesForEnumFlagsFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumModelType = metadata.ModelType;

            // Check to make sure this is an enum.
            if (!enumModelType.IsEnum)
            {
                throw new ArgumentException("This helper can only be used with enums. Type used was: " + enumModelType.FullName.ToString() + ".");
            }

            // Create string for Element.
            var sb = new StringBuilder();

            foreach (Enum item in Enum.GetValues(enumModelType))
            {
                if (Convert.ToInt32(item) != 0)
                {
                    var ti = htmlHelper.ViewData.TemplateInfo;
                    var id = ti.GetFullHtmlFieldId(item.ToString());

                    //Derive property name for checkbox name
                    var body = expression.Body as MemberExpression;
                    var propertyName = body.Member.Name;
                    var name = ti.GetFullHtmlFieldName(propertyName);

                    //Get currently select values from the ViewData model
                    TEnum selectedValues = expression.Compile().Invoke(htmlHelper.ViewData.Model);

                    var label = new TagBuilder("label");
                    label.Attributes["for"] = id;
                    label.Attributes["style"] = "display: inline-block;";
                    var field = item.GetType().GetField(item.ToString());

                    // Add checkbox.
                    var checkbox = new TagBuilder("input");
                    checkbox.Attributes["id"] = id;
                    checkbox.Attributes["name"] = name;
                    checkbox.Attributes["type"] = "checkbox";
                    checkbox.Attributes["value"] = item.ToString();

                    if ((selectedValues as Enum != null) && ((selectedValues as Enum).HasFlag(item)))
                    {
                        checkbox.Attributes["checked"] = "checked";
                    }
                    sb.AppendLine(checkbox.ToString());

                    // Check to see if DisplayName attribute has been set for item.
                    var displayName = field.GetCustomAttributes(typeof(DisplayNameAttribute), true)
                        .FirstOrDefault() as DisplayNameAttribute;
                    if (displayName != null)
                    {
                        // Display name specified.  Use it.
                        label.SetInnerText(displayName.DisplayName);
                    }
                    else
                    {
                        // Check to see if Display attribute has been set for item.
                        var display = field.GetCustomAttributes(typeof(DisplayAttribute), true)
                            .FirstOrDefault() as DisplayAttribute;
                        if (display != null)
                        {
                            label.SetInnerText(display.Name);
                        }
                        else
                        {
                            label.SetInnerText(item.Description());
                        }
                    }
                    sb.AppendLine(label.ToString());

                    // Add line break.
                    sb.AppendLine("");
                }
            }

            return new HtmlString(sb.ToString());
        }

        #endregion
    }
}