﻿namespace Web.Helpers.Common
{
    public static class TextAreaMaker
    {
        public static string CreateWithWraper(string title, string name, string value, string cssClass, string additionalAttributes)
        {
            const string pattern = @"
            <div class='row'>
                <div class='form-group'>
			        <div class='col-lg-12'>
                        <span class='label-control'>{0}</span>
			        </div>
		        </div>
		        <div class='form-group'>
                    <div class='col-lg-12'>
				        <textarea class='{3}' {4} name='{1}'>{2}</textarea>
                    </div>
                </div>
	        </div>";

            return string.Format(pattern, title, name, value, cssClass, additionalAttributes);
        }
    }
}