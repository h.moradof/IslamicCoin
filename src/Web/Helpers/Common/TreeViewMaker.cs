﻿using ViewModels.Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Web.Helpers.Common
{
    public class TreeViewMaker
    {

        #region props

        private const string ChildNodeCssClass = "";
        private const string ArrowIconHasNotChildCssClass = "fa fa-caret-left tree-icon";
        private const string ArrowIconHasChildCssClass = "fa fa-caret-down tree-icon";
        private const string LinksCssClass = "tree-node";

        private string _targetUrl;
        private bool _appendIdToTargetUrl;
        private bool _putIdsInCustomAttribute;
        private string _customAttributeNameForIds;
        private StringBuilder _treeViewHtml;
        private IList<BaseTreeViewViewModel> _localTreeItems;
        private IList<BaseTreeViewViewModel> _dbTreeItems;

        #endregion

        #region ctor

        public TreeViewMaker(
            IList<BaseTreeViewViewModel> TreeItems,
            string targetUrl, 
            bool appendIdToTargetUrl, 
            bool putIdsInCustomAttribute, 
            string customAttributeNameForIds)
        {
            _treeViewHtml = new StringBuilder();
            _localTreeItems = new List<BaseTreeViewViewModel>();

            _targetUrl = targetUrl;
            _dbTreeItems = _localTreeItems = TreeItems;
            _appendIdToTargetUrl = appendIdToTargetUrl;
            _putIdsInCustomAttribute = putIdsInCustomAttribute;
            _customAttributeNameForIds = customAttributeNameForIds;
        }

        #endregion


        #region GetTreeView

        /// <summary>
        /// Create treeview html
        /// </summary>
        /// <param name="TreeItems"></param>
        /// <param name="targetUrl">sample: /Admin/User/Detail or javascript:void();</param>
        public string Create(long rootItemId)
        {
            //FillTreeViewItems(rootItemId);

            var _firstMember = _dbTreeItems.First(c => c.Id == rootItemId);

            FillTree(_firstMember.Id, _firstMember.Title, _firstMember.HaveChild);

            return _treeViewHtml.ToString();
        }

        #endregion


        #region -FillTreeViewMembers

        /// <summary>
        /// Fill localtreeitems to order items
        /// Maybe some top item be child of the bottom item
        /// </summary>
        /// <param name="parentId"></param>
        private void FillLocalTreeItems(long parentId)
        {
            foreach (var item in _dbTreeItems.Where(c => c.ParentId == parentId).ToList())
            {
                _localTreeItems.Add(item);

                FillLocalTreeItems(item.Id);
            }
        }

        #endregion


        #region -FillTree

        /// <summary>
        /// Add root item and execute recursive method to fill child items
        /// </summary>
        private void FillTree(long rootId, string rootTitle, bool haveChild)
        {
            // append wrapper start tags
            _treeViewHtml.Append("<div id='treeview'><ul class='tree-ul-child-node'>");

            // append root item
            if (_putIdsInCustomAttribute)
                _treeViewHtml.Append(string.Format("<li><i class='fa {2} tree-icon'></i><a {3}='{4}' href='{0}' class='tree-node'> {1}</a><ul class='tree-ul-child-node'>", _targetUrl, rootTitle, haveChild ? "fa-caret-down" : "fa-caret-left", _customAttributeNameForIds, rootId));
            else
                _treeViewHtml.Append(string.Format("<li><i class='fa {2} tree-icon'></i><a href='{0}' class='tree-node'> {1}</a><ul class='tree-ul-child-node'>", _targetUrl, rootTitle, haveChild ? "fa-caret-down" : "fa-caret-left"));
                
            // recursive
            AppendTreeViewChildItems(rootId);

            // append wrapper end tags
            _treeViewHtml.Append("</ul></li>");
            _treeViewHtml.Append("</ul></div>");
        }

        #endregion


        #region -CreateTreeView

        /// <summary>
        /// Recursive method to create treeview items
        /// </summary>
        private void AppendTreeViewChildItems(long parentId)
        {
            string ul_li = string.Empty;

            foreach (BaseTreeViewViewModel item in _localTreeItems.Where(c => c.ParentId == parentId).ToList())
            {
                if (item.HaveChild)
                {
                    if (_appendIdToTargetUrl)
                    { 
                        if(_putIdsInCustomAttribute)
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-down tree-icon'></i><a {3}='{2}' href='{1}/{2}' class='{4}'> {0}</a><ul class='tree-ul-child-node'>", item.Title, _targetUrl, item.Id, _customAttributeNameForIds, item.IsActive ? "have-child active" : "have-child deactive"));
                        else
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-down tree-icon'></i><a href='{1}/{2}' class='{3}'> {0}</a><ul class='tree-ul-child-node'>", item.Title, _targetUrl, item.Id, item.IsActive ? "have-child active" : "have-child deactive"));
                    }
                    else
                    { 
                        if (_putIdsInCustomAttribute)
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-down tree-icon'></i><a {2}='{3}' href='{1}' class='{4}'> {0}</a><ul class='tree-ul-child-node'>", item.Title, _targetUrl, _customAttributeNameForIds, item.Id, item.IsActive ? "have-child active" : "have-child deactive"));
                        else
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-down tree-icon'></i><a href='{1}' class='{2}'> {0}</a><ul class='tree-ul-child-node'>", item.Title, _targetUrl, item.IsActive ? "have-child active" : "have-child deactive"));
                    }

                    ul_li = "</ul></li>";
                }
                else
                {
                    if (_appendIdToTargetUrl)
                    {
                        if (_putIdsInCustomAttribute)
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-left tree-icon'></i><a {3}='{2}' href='{1}/{2}' class='{4}'> {0}</a></li>", item.Title, _targetUrl, item.Id, _customAttributeNameForIds, item.IsActive ? "active" : "deactive"));
                        else
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-left tree-icon'></i><a href='{1}/{2}' class='{3}'> {0}</a></li>", item.Title, _targetUrl, item.Id, item.IsActive ? "active" : "deactive"));
                    }
                    else
                    {
                        if (_putIdsInCustomAttribute)
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-left tree-icon'></i><a {2}='{3}' href='{1}' class='{4}'> {0}</a></li>", item.Title, _targetUrl, _customAttributeNameForIds, item.Id, item.IsActive ? "active" : "deactive"));
                        else
                            _treeViewHtml.Append(string.Format("<li><i class='fa fa-caret-left tree-icon'></i><a href='{1}' class='{2}'> {0}</a></li>", item.Title, _targetUrl, item.IsActive ? "active" : "deactive"));
                    }

                    ul_li = string.Empty;
                }

                AppendTreeViewChildItems(item.Id);
                _treeViewHtml.Append(ul_li);
            }
        }

        #endregion
    }
}
