﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;

namespace Web.Helpers.Common
{
    public static class FormMaker
    {
        public static TagBuilder GetNewForm(string action, string method)
        {
            var form = new TagBuilder("form");
            form.MergeAttribute("action", action);
            form.MergeAttribute("method", method);

            return form;
        }

        public static TagBuilder GetNewForm(string action, string method, string submitButtonTitle, string submitButtonCssClass, IDictionary<string, string> hiddenFields, string antiforgeryTokenHiddenField)
        {
            var form = new TagBuilder("form");
            form.MergeAttribute("action", action);
            form.MergeAttribute("method", method);

            if (!string.IsNullOrEmpty(antiforgeryTokenHiddenField))
                form.InnerHtml += antiforgeryTokenHiddenField;

            foreach (var hiddenField in hiddenFields)
                form.InnerHtml += CreateHiddenField(hiddenField.Key, hiddenField.Value);

            form.InnerHtml += GetSubmitButton(submitButtonCssClass, submitButtonTitle);

            return form;
        }


        public static TagBuilder GetNewForm(string action, string method, string submitButtonTitle, string submitButtonCssClass, IDictionary<string, string> hiddenFields, IEnumerable<string> additionalFields, string antiforgeryTokenHiddenField)
        {
            var form = new TagBuilder("form");
            form.MergeAttribute("action", action);
            form.MergeAttribute("method", method);

            if (!string.IsNullOrEmpty(antiforgeryTokenHiddenField))
                form.InnerHtml += antiforgeryTokenHiddenField;

            foreach (var hiddenField in hiddenFields)
                form.InnerHtml += CreateHiddenField(hiddenField.Key, hiddenField.Value);

            foreach (var field in additionalFields)
                form.InnerHtml += field;

            form.InnerHtml += GetSubmitButton(submitButtonCssClass, submitButtonTitle);

            return form;
        }


        #region private methods

        private static string GetSubmitButton(string submitButtonCssClass, string submitButtonTitle)
        {
            const string pattern = @"
            <div class='row'>
                <div class='form-group'>
			        <div class='col-lg-12'>
                        <input type='submit' id='submitButton' class='{0}' value='{1}' />
			        </div>
		        </div>
	        </div>";

            return string.Format(pattern, submitButtonCssClass, submitButtonTitle).ToString();
        }

        private static string CreateHiddenField(string name , string value)
        {
            var hiddenInput = new TagBuilder("input");
            hiddenInput.MergeAttribute("type", "hidden");
            hiddenInput.MergeAttribute("name", name);
            hiddenInput.MergeAttribute("value", value);
            return hiddenInput.ToString();
        }

        #endregion

    }
}