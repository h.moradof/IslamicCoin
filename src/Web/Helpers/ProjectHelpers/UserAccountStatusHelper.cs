﻿using DomainModels.Entities.Base;
using Infrastructure.Common;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class UserAccountStatusHelper
    {

        public static MvcHtmlString ShowUserAccountStatus(this HtmlHelper htmlHelper, AccountStatus accountStatus)
        {
            var div = new TagBuilder("div");
            div.MergeAttribute("class", string.Format("account-status account-status-{0}", accountStatus.ToString().ToLowerInvariant()));
            div.InnerHtml = accountStatus.Description();

            return MvcHtmlString.Create(div.ToString());
        }


    }
}