﻿using DomainModels.Entities.Financial;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class PaymentStatusHelper
    {
        public static MvcHtmlString ShowPaymentStatus(this HtmlHelper htmlHelper, PaymentStatus paymentStatus)
        {
            var div = new TagBuilder("div");
            div.MergeAttribute("class", string.Format("payment-status payment-status-{0}", paymentStatus.IdentityName.ToLowerInvariant()));
            div.InnerHtml = paymentStatus.DisplayName;

            return MvcHtmlString.Create(div.ToString());
        }

    }
}