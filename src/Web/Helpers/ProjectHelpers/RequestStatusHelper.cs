﻿using DomainModels.Entities.Shop;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class RequestStatusHelper
    {
        public static MvcHtmlString ShowRequestStatus(this HtmlHelper htmlHelper, RequestStatus requestStatus)
        {
            var div = new TagBuilder("div");
            div.MergeAttribute("class", string.Format("request-status request-status-{0}", requestStatus.IdentityName.ToLowerInvariant()));
            div.InnerHtml = requestStatus.DisplayName;

            return MvcHtmlString.Create(div.ToString());
        }

    }
}