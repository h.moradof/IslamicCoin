﻿using DomainModels.Entities.Financial;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class WithdrawalRequestStatusHelper
    {
        public static MvcHtmlString ShowWithdrawalRequestStatus(this HtmlHelper htmlHelper, WithdrawalRequestStatus status)
        {
            var div = new TagBuilder("div");
            div.MergeAttribute("class", string.Format("withdrawal-request-status withdrawal-request-status-{0}", status.IdentityName.ToLowerInvariant()));
            div.InnerHtml = status.DisplayName;

            return MvcHtmlString.Create(div.ToString());
        }
    }
}