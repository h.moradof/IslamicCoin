﻿using DomainModels.Entities.Financial;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class AccountAcceptStatusHelper
    {
        public static MvcHtmlString ShowAccountAcceptStatus(this HtmlHelper htmlHelper, AccountAcceptStatus status, string style = null)
        {
            var div = new TagBuilder("div");
            div.MergeAttribute("class", string.Format("account-accept-status account-accept-status-{0}", status.IdentityName.ToLowerInvariant()));
            if (!string.IsNullOrEmpty(style))
            {
                div.MergeAttribute("style", style);
            }

            div.InnerHtml = status.DisplayName;

            return MvcHtmlString.Create(div.ToString());
        }
    }
}