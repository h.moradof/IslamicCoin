﻿using DomainModels.Entities.HumanResource;
using Infrastructure.Common;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class UserAcceptStatusHelper
    {
        public static MvcHtmlString ShowAcceptStatus(this HtmlHelper htmlHelper, AcceptStatus acceptStatus, string style = null)
        {
            var div = new TagBuilder("div");
            div.MergeAttribute("class", string.Format("accept-status accept-status-{0}", acceptStatus.ToString().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(style))
            {
                div.MergeAttribute("style", style);
            }

            div.InnerHtml = acceptStatus.Description();

            return MvcHtmlString.Create(div.ToString());
        }
    }
}