﻿using Web.DependencyResolution;
using Web.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using System;
using System.Web.Security;
using System.Security.Principal;
using Services.Interfaces;

namespace Web.Security.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class MvcAuthorizeAttribute : AuthorizeAttribute
    {

        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    bool skipAuthorization = filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Count() > 0
        //        || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Count() > 0;

        //    if (skipAuthorization)
        //        return;

        //    base.OnAuthorization(filterContext);
        //}


        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return IsValidResourceAccess(httpContext);
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var viewResult = new ViewResult();
            viewResult.ViewName = "~/Views/Error/Forbidden.cshtml";
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            filterContext.HttpContext.Response.StatusCode = 403;
            filterContext.Result = viewResult;
        }



        #region private methods

        /// <summary>
        /// set signed in user principal into current context
        /// and then check access of user to resource (MVC action)
        /// </summary>
        /// <param name="filterContext"></param>
        /// <exception cref="UnauthorizedAccessException">throw when user doesnt have access to action</exception>
        private bool IsValidResourceAccess(HttpContextBase httpContex)
        {
            try
            {
                SetUserPrincipal();

                ValidateResourceAccess();
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// validate access to resource for signed in user
        /// </summary>
        /// <exception cref="UnauthorizedAccessException">throw when user does'nt have access to resource</exception>
        private void ValidateResourceAccess()
        {
            var userService = StructureMapObjectFactory.Container.GetInstance<IUserService>();
            var currentUser = StructureMapObjectFactory.Container.GetInstance<ICurrentUser>();

            var context = new HttpContextWrapper(HttpContext.Current);
            var rd = context.Request.RequestContext.RouteData;

            var resourceUrl = string.Empty;
            var areaName = rd.DataTokens["area"].ToString();

            if (!string.IsNullOrEmpty(areaName))
            {
                resourceUrl = string.Format("/{0}/{1}/{2}", areaName, rd.Values["controller"], rd.Values["action"]);
            }
            else
            {
                resourceUrl = string.Format("/{0}/{1}", rd.Values["controller"], rd.Values["action"]);
            }

            bool userhasAccessToResource = false;// userService.HasAccessToResource(currentUser.Id, resourceUrl);

            if (!userhasAccessToResource)
                throw new UnauthorizedAccessException("You doesn't access to resource");
        }


        /// <summary>
        /// set user principal from form cookie to current context
        /// </summary>
        /// <exception cref="UnauthorizedAccessException">throw when user does'nt signed in</exception>
        private void SetUserPrincipal()
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie == null)
                throw new UnauthorizedAccessException("You should sign in");

           string encTicket = authCookie.Value;

            if (!string.IsNullOrEmpty(encTicket))
            {
                var ticket = FormsAuthentication.Decrypt(encTicket);
                var identity = new FormsIdentity(ticket);
                var principal = new GenericPrincipal(identity, null);
                HttpContext.Current.User = principal;
            }
        }

        #endregion

    }
}