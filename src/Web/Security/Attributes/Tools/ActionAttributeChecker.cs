﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace Web.Security.Attributes.Tools
{
    public static class ActionAttributeChecker
    {
        public static bool HasAttribute(this AuthorizationContext filterContext ,Type attributeType)
        {
            return filterContext.ActionDescriptor.GetCustomAttributes(attributeType, true).Count() > 0
                || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(attributeType, true).Count() > 0;
        }

        public static bool HasAttribute(this ActionExecutingContext filterContext, Type attributeType)
        {
            return filterContext.ActionDescriptor.GetCustomAttributes(attributeType, true).Count() > 0
                || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(attributeType, true).Count() > 0;
        }

    }
}