﻿using DatabaseContext.Context;
using Infrastructure.Exceptions;
using Services.Interfaces;
using System;
using System.Web;
using System.Web.Mvc;
using Web.DependencyResolution;
using Web.Security.Attributes.Tools;

namespace Web.Security.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class ValidateIpAttribute : ActionFilterAttribute
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IValidIpService _validIpService;
        #endregion

        #region ctor
        public ValidateIpAttribute()
            : this(StructureMapObjectFactory.Container.GetInstance<IUnitOfWork>(),
                  StructureMapObjectFactory.Container.GetInstance<IValidIpService>())
        {

        }

        public ValidateIpAttribute(IUnitOfWork uow, IValidIpService validaIpService)
        {
            _uow = uow;
            _validIpService = validaIpService;
        }
        #endregion


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var actionIsPublic = filterContext.HasAttribute(typeof(AllowAnonymousAttribute));
            if (!actionIsPublic)
                return;

            var clientIpAddress = HttpContext.Current.Request.UserHostAddress;

            try
            {
                var hasAccessToResource = _validIpService.HasAccess(clientIpAddress);
                _uow.SaveChanges();

                if (hasAccessToResource)
                    return;
            }
            catch (CustomException)
            {
                return;
            }

            // show not access view
            var viewResult = new ViewResult();
            viewResult.ViewName = "~/Views/Error/JustValidIps.cshtml";
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            filterContext.HttpContext.Response.StatusCode = 403;
            filterContext.Result = viewResult;
        }

    }
}