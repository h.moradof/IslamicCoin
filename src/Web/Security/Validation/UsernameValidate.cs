﻿using System.Text.RegularExpressions;

namespace Web.Security
{

    /// <summary>
    /// validate username that be a mobile number
    /// </summary>
    public static class UsernameValidate
    {
        public const int ValidLength = 11;

        public static UsernameValidateResult Validate(string username)
        {

            if (username.Length != ValidLength)
                return UsernameValidateResult.LengthIsInvalid;

            if (!IsNumrical(username))
                return UsernameValidateResult.IsNotAnNumber;

            if (!username.ToString().StartsWith("09"))
                return UsernameValidateResult.InvalidMobileFormat;

            return UsernameValidateResult.IsValid;
        }

        private static bool IsNumrical(string value)
        {
            return new Regex("[0-9]{11}").IsMatch(value);
        }
    }

    public enum UsernameValidateResult 
    {
        IsValid,
        LengthIsInvalid,
        IsNotAnNumber,
        InvalidMobileFormat
    }
 
}