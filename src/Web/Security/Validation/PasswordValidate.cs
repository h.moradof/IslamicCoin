﻿using Infrastructure.Common;

namespace Web.Security
{
    public static class PasswordValidate
    {
        public const PasswordValidationMode ValidationMode = PasswordValidationMode.MustBigerThanMinimumLength;
        public const int MinimumLength = 6;

        public static PasswordValidateResult Validate(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return PasswordValidateResult.PasswordIsRequired;
            }


            switch (ValidationMode)
	        {
		        case PasswordValidationMode.None:
                    return PasswordValidateResult.IsValid;

                case PasswordValidationMode.MustContainNumber:
                    if (password.ContainsNumber())
                        return PasswordValidateResult.IsValid;
                    else
                        return PasswordValidateResult.IsInvalidNotContainNumber;

                case PasswordValidationMode.MustBigerThanMinimumLength:
                    if (password.Length >= MinimumLength)
                        return PasswordValidateResult.IsValid;
                    else
                        return PasswordValidateResult.IsInvalidLengthIsSmall;          
                 
                case PasswordValidationMode.MustContainNumberAndMustBigerThanMinimumLength:
                    if (password.Length >= MinimumLength)
                        if (password.ContainsNumber())
                            return PasswordValidateResult.IsValid;
                        else
                            return PasswordValidateResult.IsInvalidNotContainNumber;
                    else
                        return PasswordValidateResult.IsInvalidLengthIsSmall;
                    
	        }

        }
    }

    public enum PasswordValidateResult 
    {
        IsValid,
        IsInvalidLengthIsSmall,
        IsInvalidNotContainNumber,
        PasswordIsRequired
    }


    public enum PasswordValidationMode
    {
        None,
        MustContainNumber,
        MustBigerThanMinimumLength,
        MustContainNumberAndMustBigerThanMinimumLength
    }
}