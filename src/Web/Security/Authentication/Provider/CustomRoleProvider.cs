﻿using System;
using System.Web.Security;
using Services.Interfaces;
using Web.DependencyResolution;

namespace Web.Security.Authentication.Provider
{
    public class CustomRoleProvider : RoleProvider
    {

        public override bool IsUserInRole(string username, string roleName)
        {
            var _userService = StructureMapObjectFactory.Container.GetInstance<IUserService>();
            var isUserInRole = _userService.IsUserInRole(username, roleName);
            return isUserInRole;
        }

        public override string[] GetRolesForUser(string username)
        {
            var _userService = StructureMapObjectFactory.Container.GetInstance<IUserService>();
            var roles = _userService.GetRolesForUser(username);
            return roles;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            var _roleService = StructureMapObjectFactory.Container.GetInstance<IRoleService>();
            return _roleService.GetUsersInRole(roleName);
        }

        public override string[] GetAllRoles()
        {
            var _roleService = StructureMapObjectFactory.Container.GetInstance<IRoleService>();
            return _roleService.GetAllRoles();
        }

        public override bool RoleExists(string roleName)
        {
            var _roleService = StructureMapObjectFactory.Container.GetInstance<IRoleService>();
            return _roleService.RoleExists(roleName);
        }


        #region Not Implemented Methods

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }


        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}