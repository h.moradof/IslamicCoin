﻿using Infrastructure.Exceptions;
using System.Web;
using System.Web.Security;

namespace Web.Security.Authentication
{
    public class CurrentUser : ICurrentUser
    {
        public long Id
        {
            get
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new CustomException("Unauthorized");

                return long.Parse(((FormsIdentity)HttpContext.Current.User.Identity).Ticket.UserData);
            }
        }

    }
}