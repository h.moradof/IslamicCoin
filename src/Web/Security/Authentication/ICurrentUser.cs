﻿namespace Web.Security.Authentication
{
    public interface ICurrentUser
    {
        long Id { get; }
    }
}