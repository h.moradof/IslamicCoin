﻿using DatabaseContext.Context;
using Services.Interfaces;
using System.Text;
using System.Web.Mvc;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class SmsController : Controller
    {
        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        #endregion

        #region ctor
        public SmsController(
            IUnitOfWork uow,
            IUserService userService)
        {
            _uow = uow;
            _userService = userService;
        }
        #endregion

        // GET: http://xxxxx.com/Sms/Asanak/eu09wufsjd0294fkny598eldkn0odjlmxnza/
        [HttpGet]
        public ActionResult Asanak(string code, string Destination, string Source, string ReceiveTime, string MsgBody)
        {
            if (string.IsNullOrEmpty(code))
            {
                return Content("error (1)");
            }

            if (code.Trim() != "eu09wufsjd0294fkny598eldkn0odjlmxnza")
            {
                return Content("error (2)");
            }


            var needToSaveChange = _userService.VerifyMobileNumber(CorrectSource(Source), MsgBody);

            if (needToSaveChange)
                _uow.SaveChanges();

            return Content("success");
        }

        private string CorrectSource(string source)
        {
            var sb = new StringBuilder(source);
            sb.Insert(0, "+");
            sb.Replace("+98", "0");
            return sb.ToString();
        }
    }
}