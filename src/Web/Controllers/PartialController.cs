﻿using DomainModels.Entities.Site;
using Infrastructure.Cache.Interfaces;
using Services.Interfaces;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class PartialController : Controller
    {

        #region props

        private readonly ISiteSeoSettingCacheManager _siteSeoSettingCacheManager;

        #endregion

        #region ctor

        public PartialController(ISiteSeoSettingCacheManager siteSeoSettingCacheManager)
        {
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
        }

        #endregion
        
                        
        [ChildActionOnly]
        public PartialViewResult _Meta()
        {
            var siteSeoSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(siteSeoSetting);
        }

        [ChildActionOnly]
        public PartialViewResult  _Title()
        {
            var siteSeoSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(model: siteSeoSetting.Title);
        }

        [ChildActionOnly]
        public PartialViewResult _Copyright()
        {
            var siteSeoSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(model: siteSeoSetting);
        }


    }
}