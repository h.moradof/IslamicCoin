﻿using DatabaseContext.Context;
using Infrastructure.Cache.Interfaces;
using System.Web.Mvc;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class AjaxController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly IApplicationSettingCacheManager _applicationSettingCacheManager;
        private readonly bool AllowNotAjaxRequests;
        #endregion

        #region ctor

        public AjaxController(
            IUnitOfWork uow,
            IApplicationSettingCacheManager applicationSettingCacheManager)
        {
            _uow = uow;

            AllowNotAjaxRequests = _applicationSettingCacheManager.Get().AllowNotAjaxRequests;
        }

        #endregion


        

    }
}