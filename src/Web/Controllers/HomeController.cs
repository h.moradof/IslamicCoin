﻿using Infrastructure.Messages;
using Services.Interfaces;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


    }
}