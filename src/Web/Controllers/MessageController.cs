﻿using System.Web.Mvc;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class MessageController : Controller
    {
        public ActionResult Show()
        {
            return View();
        }
	}
}