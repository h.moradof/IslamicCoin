﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.File;
using System.IO;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class FileController : Controller
    {
        #region props

        private readonly IApplicationSettingCacheManager _applicationSettingCacheManager;

        #endregion

        #region ctor

        public FileController(IApplicationSettingCacheManager applicationSettingCacheManager)
        {
            _applicationSettingCacheManager = applicationSettingCacheManager;
        }

        #endregion


        // --------------------------------------------- Image
        [HttpGet]
        public ActionResult Image(string fileName, EntityName entityName = EntityName.Photo)
        {
            if (string.IsNullOrEmpty(fileName))
                return PicNotFound();
            
            fileName = Path.GetFileName(fileName); // تمیز سازی امنیتی است
            var filePath = Path.Combine(UploadFilePathManager.GetUploadPath(entityName), fileName);

            if (!FileValidator.IsValidPicture(fileName) || !System.IO.File.Exists(Server.MapPath(filePath)))
                return PicNotFound();


            return File(filePath, MimeMapping.GetMimeMapping(fileName), fileName);
        }



        // --------------------------------------------- Pdf
        [HttpGet]
        public ActionResult Pdf(string fileName, EntityName entityName)
        {
            fileName = Path.GetFileName(fileName); // تمیز سازی امنیتی است
            var filePath = Path.Combine(UploadFilePathManager.GetUploadPath(entityName), fileName);

            // اگر پسوند درست نبود و یا فایل وجود نداشت
            if ((Path.GetExtension(fileName) != ".pdf") || !System.IO.File.Exists(Server.MapPath(filePath)))
                return HttpNotFound();

            return File(filePath, MediaTypeNames.Application.Pdf, fileName);
        }



        // --------------------------------------------- Zip
        [HttpGet]
        public ActionResult Zip(string fileName, EntityName entityName)
        {
            fileName = Path.GetFileName(fileName); // تمیز سازی امنیتی است
            var filePath = Path.Combine(UploadFilePathManager.GetUploadPath(entityName), fileName);

            // اگر پسوند درست نبود و یا فایل وجود نداشت
            if ((Path.GetExtension(fileName) != ".zip") || !System.IO.File.Exists(Server.MapPath(filePath)))
                return HttpNotFound();

            return File(filePath, MediaTypeNames.Application.Zip, fileName);
        }


        #region private methods

        private ActionResult PicNotFound()
        {
            var fileName = _applicationSettingCacheManager.Get().DefaultPicName;
            var filePath = Path.Combine(UploadFilePathManager.RootPath, fileName);
            return File(filePath, MediaTypeNames.Image.Gif, fileName);
        }

        #endregion

    }
}