﻿using Web.Security.Attributes;
using System.Web;
using System.Web.Mvc;

namespace Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new ElmahHandledErrorLoggerFilter());
            //filters.Add(new HandleErrorAttribute());

            //filters.Add(new MvcAuthorizeAttribute());

            filters.Add(new ValidateIpAttribute());
        }
    }
}
