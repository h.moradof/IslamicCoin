﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.Cache.Model;
using Services.Interfaces;
using Web.DependencyResolution;
using System.Configuration;

namespace Web.App_Start
{
    public static class CacheRegisteration
    {
        public static async void Register()
        {

            // cache application settings (web.config)
            var applicationSettingCacheManager = StructureMapObjectFactory.Container.GetInstance<IApplicationSettingCacheManager>();
            var applicationSetting = new ApplicationSetting();
            applicationSetting.DefaultPicName = ConfigurationManager.AppSettings["DefaultPicName"].ToString();
            applicationSetting.AllowNotAjaxRequests = bool.Parse(ConfigurationManager.AppSettings["AllowNotAjaxRequests"].ToString());
            applicationSetting.IsActiveIpValidation = bool.Parse(ConfigurationManager.AppSettings["IsActiveIpValidation"].ToString());
            applicationSetting.IsActiveTwoLevelLogin = bool.Parse(ConfigurationManager.AppSettings["IsActiveTwoLevelLogin"].ToString());

            // mail setting (web.config)
            var mailSetting = new MailSetting
            {
                From = ConfigurationManager.AppSettings["MailSetting_From"].ToString(),
                Host = ConfigurationManager.AppSettings["MailSetting_Host"].ToString(),
                Password = ConfigurationManager.AppSettings["MailSetting_Password"].ToString(),
                PortNumber = int.Parse(ConfigurationManager.AppSettings["MailSetting_PortNumber"].ToString()),
                UseSsl = bool.Parse(ConfigurationManager.AppSettings["MailSetting_UseSsl"].ToString())
            };
            applicationSetting.MailSetting = mailSetting;

            // sms setting (web.config)
            var smsSetting = new SmsSetting
            {
                Username = ConfigurationManager.AppSettings["SMSSetting_Username"].ToString(),
                Password = ConfigurationManager.AppSettings["SMSSetting_Password"].ToString(),
                SenderPhoneNumber = ConfigurationManager.AppSettings["SMSSetting_SenderPhoneNumber"].ToString()
            };
            applicationSetting.SmsSetting = smsSetting;

            applicationSettingCacheManager.Cache(applicationSetting);

            // cache site seo setting (db)
            var siteSeoSettingService = StructureMapObjectFactory.Container.GetInstance<ISiteSeoSettingService>();
            var siteSeoSettingCacheManager = StructureMapObjectFactory.Container.GetInstance<ISiteSeoSettingCacheManager>();
            siteSeoSettingCacheManager.Cache(await siteSeoSettingService.GetListAsync());
        }
    }
}