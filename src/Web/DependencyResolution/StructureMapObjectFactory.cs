﻿using System;
using StructureMap;
using StructureMap.Web;
using System.Threading;
using Services.Interfaces;
using DatabaseContext.Context;
using Infrastructure.Cache.Interfaces;
using Infrastructure.Cache;
using Infrastructure.Mail.Interfaces;
using Infrastructure.Mail;
using Web.Security.Authentication;
using Infrastructure.Mail.Core;
using Infrastructure.Sms.Interfaces;
using Infrastructure.Sms;
using Infrastructure.Sms.Providers;
using Infrastructure.WebServices;
using Infrastructure.API.IP;

namespace Web.DependencyResolution
{
    /// <summary>
    /// object factory
    /// </summary>
    public static class StructureMapObjectFactory
    {
        private static readonly Lazy<Container> _containerBuilder =
            new Lazy<Container>(defaultContainer, LazyThreadSafetyMode.ExecutionAndPublication);

        public static IContainer Container
        {
            get { return _containerBuilder.Value; }
        }

        private static Container defaultContainer()
        {
            return new Container(x =>
            {
                x.For<IUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<DefaultDatabaseContext>();

                x.Scan(scan =>
                {
                    scan.AssemblyContainingType<IUserService>();
                    scan.WithDefaultConventions();
                });

                // user
                x.For<ICurrentUser>().Use<CurrentUser>();

                // ip checker
                x.For<IIpChecker>().Singleton().Use<IpChecker>();

                // cache managers
                x.For<IApplicationSettingCacheManager>().Singleton().Use<ApplicationSettingCacheManager>();
                x.For<ISiteSeoSettingCacheManager>().Singleton().Use<SiteSeoSettingCacheManager>();

                // mail & sms
                x.For<IMailSender>().Singleton().Use<MailSender>();
                x.For<ISmsSender>().Singleton().Use<SmsSender>();
                x.For<IUserAccountMailSender>().Singleton().Use<UserAccountMailSender>();

                // sms provider
                x.For<ISmsProvider>().Singleton().Use<KaveNegarSmsProvider>().Ctor<string>("templateName").Is("Together-Vertification");

                // webservice
                x.For<IWebServiceManager>().Singleton().Use<WebServiceManager>();
            });
        }

    }
}