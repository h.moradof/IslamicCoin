﻿using Infrastructure.Exceptions;
using Infrastructure.Sms.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Infrastructure.Sms.Providers
{
    public class KaveNegarSmsProvider : ISmsProvider
    {

        #region props
        private readonly string _templateName;
        #endregion

        #region ctor
        public KaveNegarSmsProvider(string templateName)
        {
            _templateName = templateName;
        }
        #endregion


        public SendSmsResult SendSms(string receiverMobileNumber, string message, string username, string password, string senderPhoneNumber)
        {
            try
            {
                var jsonResult = SendSmsByTemplate(receiverMobileNumber, message);

                var statusCode = GetStatusCodeFromRequestJsonResult(jsonResult);

                var smsResult = CreateSmsResultByStatusCode(statusCode);

                return smsResult;
            }
            catch (Exception ex)
            {
                return new SendSmsResult(false, ex.Message);
            }
        }


        private int GetStatusCodeFromRequestJsonResult(string requestJsonResult)
        {
            var json = JObject.Parse(requestJsonResult);

            string statusCode = json["return"]["status"].ToString();

            return int.Parse(statusCode);
        }

        private SendSmsResult CreateSmsResultByStatusCode(int statusCode)
        {
            var smsResult = new SendSmsResult(false, string.Empty);

            switch (statusCode)
            {
                case 200:
                    smsResult.IsSuccess = true;
                    smsResult.Message = "با موفقیت ارسال شد";
                    break;
                case 400:
                    smsResult.Message = "پارامترها ناقص هستند";
                    break;
                case 401:
                    smsResult.Message = "حساب کاربری غیرفعال شده است";
                    break;
                case 402:
                    smsResult.Message = "عملیات ناموفق بود";
                    break;
                case 404:
                    smsResult.Message = "متد نامشخص است";
                    break;
                case 405:
                    smsResult.Message = "متد Get/Post اشتباه است";
                    break;
                case 406:
                    smsResult.Message = "پارامترهای اجباری خالی ارسال شده اند";
                    break;
                case 407:
                    smsResult.Message = "دسترسی به اطلاعات مورد نظر برای شما امکان پذیر نیست";
                    break;
                case 409:
                    smsResult.Message = "سرور قادر به پاسخگوئی نیست بعدا تلاش کنید";
                    break;
                case 411:
                    smsResult.Message = "دریافت کننده نامعتبر است";
                    break;
                case 412:
                    smsResult.Message = "ارسال کننده نامعتبر است";
                    break;
                case 413:
                    smsResult.Message = "پیام خالی است و یا طول پیام بیش از حد مجاز می باشد. لاتین ﻛﺎراﻛﺘﺮ و ﻓﺎرﺳﻲ 268 ﻛﺎراﻛﺘﺮ";
                    break;
                case 414:
                    smsResult.Message = "حجم درخواست بیشتر از حد مجاز است ،ارسال پیامک :هر فراخوانی حداکثر 200 رکوردو کنترل وضعیت :هر فراخوانی 3000 رکورد";
                    break;
                case 415:
                    smsResult.Message = "اندیس شروع بزرگ تر از کل تعداد شماره های مورد نظر است";
                    break;
                case 417:
                    smsResult.Message = "تاریخ معتبر نمی باشد ، مقدار 0 به معنای زمان فعلی است و در غیر اینصورت فرمت UnixTime را ارسال نمایید";
                    break;
                case 418:
                    smsResult.Message = "اعتبار شما کافی نمی باشد";
                    break;
                case 419:
                    smsResult.Message = "طول آرایه متن و گیرنده و فرستنده هم اندازه نیست";
                    break;
                case 431:
                    smsResult.Message = "ساختار کد صحیح نمی باشد";
                    break;
                case 432:
                    smsResult.Message = "پارامتر کد در متن پیام پیدا نشد";
                    break;
            }

            return smsResult;
        }


        private static string TargetUrlWithTemplatePattern = "https://api.kavenegar.com/v1/35683979754272784D3070307763636C5935593465673D3D/verify/lookup.json?receptor={0}&token={1}&template={2}";

        private string SendSmsByTemplate(string receiverMobileNumber, string message)
        {
            string targetUrl = string.Format(TargetUrlWithTemplatePattern, receiverMobileNumber, message, _templateName);

            var request = (HttpWebRequest)WebRequest.Create(targetUrl);
            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream())
            {
                var reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }

    }
}
