﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.Exceptions;
using Infrastructure.Sms.Interfaces;

namespace Infrastructure.Sms
{
    public class SmsSender : ISmsSender
    {

        #region props
        private readonly IApplicationSettingCacheManager _applicationSettingCacheManager;
        private readonly ISmsProvider _smsProvider;
        #endregion

        #region ctor
        public SmsSender(IApplicationSettingCacheManager applicationSettingCacheManager, ISmsProvider smsProvider)
        {
            _applicationSettingCacheManager = applicationSettingCacheManager;
            _smsProvider = smsProvider;
        }
        #endregion


        public SendSmsResult Send(string mobileNumber, string message)
        {
            var smsSetting = _applicationSettingCacheManager.Get().SmsSetting;

            var result = _smsProvider.SendSms(mobileNumber, message, smsSetting.Username, smsSetting.Password, smsSetting.SenderPhoneNumber);

            return result;
        }

    }

}
