﻿using Infrastructure.Cache.Model;

namespace Infrastructure.Sms.Interfaces
{
    public interface ISmsProvider
    {
        SendSmsResult SendSms(string receiverMobileNumber, string message, string username, string password, string senderPhoneNumber);
    }
}
