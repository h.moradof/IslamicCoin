﻿using Infrastructure.Exceptions;
using System;
using System.IO;
using System.Net;

namespace Infrastructure.Ftp
{
    public static class FtpManager
    {

        /// <summary>
        /// upload file to server via ftp
        /// </summary>
        /// <param name="destinationPath">like "/" or "/up/"</param>
        public static void Upload(string filename, string ftpServerIP, string destinationPath, string ftpUsername, string ftpPassword)
        {
            FileInfo fileInf = new FileInfo(filename);
            FtpWebRequest reqFTP;
            Uri destinationUri = new Uri(string.Format("ftp://{0}{1}{2}", ftpServerIP, destinationPath, fileInf.Name));

            // Create FtpWebRequest object from the Uri provided
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(destinationUri);

            // Provide the WebPermission Credintials
            reqFTP.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

            // By default KeepAlive is true, where the control connection is not closed after a command is executed.
            reqFTP.KeepAlive = false;

            // Specify the command to be executed.
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // Specify the data transfer type.
            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file
            reqFTP.ContentLength = fileInf.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLength;

            // Opens a file stream (System.IO.FileStream) to read the file to be uploaded
            FileStream fs = fileInf.OpenRead();

            // Stream to which the file to be upload is written
            Stream strm = reqFTP.GetRequestStream();

            // Read from the file stream 2kb at a time
            contentLength = fs.Read(buff, 0, buffLength);

            // Until Stream content ends
            while (contentLength != 0)
            {
                // Write Content from the file stream to the FTP Upload Stream
                strm.Write(buff, 0, contentLength);
                contentLength = fs.Read(buff, 0, buffLength);
            }

            // Close the file stream and the Request Stream
            strm.Close();
            fs.Close();

        }



        /// <summary>
        /// download file and save it into 'filePath' with 'filename' name
        /// </summary>
        public static void Download(string filePath, string fileName, string ftpServerIP, string ftpUsername, string ftpPassword)
        {
            FtpWebRequest reqFTP;

            //filePath = <<The full path where the file is to be created.>>,
            //fileName = <<The Name of the file to be createdNeed not name on FTP server. name name()>>
            FileStream outputStream = new FileStream(filePath + "\\" + fileName, FileMode.Create);

            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServerIP + "/" + fileName));
            reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            Stream ftpStream = response.GetResponseStream();

            long cl = response.ContentLength;
            int bufferSize = 2048;
            int readCount;
            byte[] buffer = new byte[bufferSize];
            readCount = ftpStream.Read(buffer, 0, bufferSize);

            while (readCount > 0)
            {
                outputStream.Write(buffer, 0, readCount);
                readCount = ftpStream.Read(buffer, 0, bufferSize);
            }

            ftpStream.Close();
            outputStream.Close();
            response.Close();
        }

    }
}
