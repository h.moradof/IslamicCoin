﻿using Infrastructure.Exceptions;
using System;
using System.Net;

namespace Infrastructure.API.IP
{
    public class IpChecker : IIpChecker
    {
        public bool IsValidIp(string ipAddress)
        {
            try
            {
                var webClient = new WebClient();
                var result = webClient.DownloadString(string.Format("http://ip-api.com/json/{0}", ipAddress));

                if (result.Contains("Mobin") || result.Contains("Iran") || result.Contains("iran") ||
                    result.Contains("IRAN") || result.Contains("Google") || result.Contains("Yahoo") ||
                    result.Contains("Bing") || result.Contains("Yandex") || result.Contains("Baidu"))
                {
                    return true;
                }

                return false;
            }
            catch (WebException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (NotSupportedException ex)
            {
                throw new CustomException(ex.Message);
            }
        }

    }
}
