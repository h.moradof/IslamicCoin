﻿namespace Infrastructure.API.IP
{
    public interface IIpChecker
    {
        bool IsValidIp(string ipAddress);
    }
}