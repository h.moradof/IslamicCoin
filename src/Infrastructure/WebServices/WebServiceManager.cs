﻿using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System.Text;

namespace Infrastructure.WebServices
{
    public class WebServiceManager : IWebServiceManager
    {
        // Returns JSON string
        public JObject GET(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream())
            {
                var reader = new StreamReader(responseStream, Encoding.UTF8);
                return JObject.Parse(reader.ReadToEnd());
            }
        }

        public JArray GetArray(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream())
            {
                var reader = new StreamReader(responseStream, Encoding.UTF8);
                return JArray.Parse(reader.ReadToEnd());
            }
        }

        public JObject POST(string url, string postData)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            var data = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = data.Length;
            request.ContentType = @"application/json";

            using (var dataStream = request.GetRequestStream())
            {
                dataStream.Write(data, 0, data.Length);
            }
            
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd());
            }
        }

        public JArray PostArray(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = @"application/json";
            
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return JArray.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd());
            }
        }
    }
}
