﻿using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System;

namespace Infrastructure.Pagination
{
    public static class Extension
    {
        public static async Task<PagedList<T>> ToPagedListAsync<T>(this IQueryable<T> query, int pageNumber, int pageSize)
        {
            if (pageNumber < 1)
                throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "PageNumber cannot be below 1.");
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException("pageSize", pageSize, "PageSize cannot be less than 1.");

            var result = new PagedList<T>();            
            result.Data = await query.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
            result.TotalItemCount = query.Count();
            result.PageCount = CalcPageCount(result.TotalItemCount, pageSize);
            result.IsFirstPage = (pageNumber == 1);
            result.IsLastPage = (pageNumber == result.PageCount);
            result.HasNextPage = (pageNumber < result.PageCount);
            result.HasPreviousPage = (pageNumber > 1);
            result.PageNumber = pageNumber;
            result.PageSize = pageSize;

            return result;
        }

        public static PagedList<T> ToPagedList<T>(this IQueryable<T> query, int pageNumber, int pageSize)
        {
            if (pageNumber < 1)
                throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "PageNumber cannot be below 1.");
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException("pageSize", pageSize, "PageSize cannot be less than 1.");

            var result = new PagedList<T>();
            result.Data = query.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            result.TotalItemCount = query.Count();
            result.PageCount = CalcPageCount(result.TotalItemCount, pageSize);
            result.IsFirstPage = (pageNumber == 1);
            result.IsLastPage = (pageNumber == result.PageCount);
            result.HasNextPage = (pageNumber < result.PageCount);
            result.HasPreviousPage = (pageNumber > 1);
            result.PageNumber = pageNumber;
            result.PageSize = pageSize;

            return result;
        }

        private static int CalcPageCount(int totalItemCount, int pageSize)
        {
            int pageCount = totalItemCount / pageSize;
            if (totalItemCount % pageSize > 0)
                pageCount++;
            return (pageCount > 0) ? pageCount : 1;
        }
    }

    public enum OrderByDirection : byte
    {
        Ascending = 1,
        Descending
    }
}
