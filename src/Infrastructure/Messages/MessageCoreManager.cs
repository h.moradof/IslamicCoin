﻿using System.ComponentModel;

namespace Infrastructure.Messages
{
    public static class MessageCoreManager
    {
        public enum MessageType
        {
            [Description("اطلاعات با موفقیت ثبت شد.")]
            SuccessInsert,
            [Description("اطلاعات با موفقیت ویرایش شد.")]
            SuccessUpdate,
            [Description("اطلاعات با موفقیت حذف شد.")]
            SuccessDelete,
            [Description("ثبت اطلاعات موفق نبود")]
            ErrorInsert,
            [Description("ویرایش اطلاعات موفق نبود")]
            ErrorUpdate,
            [Description("حذف اطلاعات موفق نبود")]
            ErrorDelete,
        }

    }
}