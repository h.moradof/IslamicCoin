﻿
namespace Infrastructure.Messages
{
    public class Message
    {
        public string MessageBody { get; set; }
        public bool IsSuccess { get; set; }
    }
}