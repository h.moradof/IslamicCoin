﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.Mail.Interfaces;
using System.Text;

namespace Infrastructure.Mail
{
    public class UserAccountMailSender: IUserAccountMailSender
    {

        #region props
        private readonly IMailSender _mailSender;
        private readonly ISiteSeoSettingCacheManager _siteSeoSettingCacheManager;
        #endregion

        #region ctor
        public UserAccountMailSender(IMailSender mailSender, ISiteSeoSettingCacheManager siteSeoSettingCacheManager)
        {
            _mailSender = mailSender;
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
        }
        #endregion


        public void SendEditUserEmail(string userEmailAddress)
        {
            var siteSeoSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            var body = new StringBuilder(EmailPatternManager.GetEditUserEmailPattern());
            body.Replace("[SiteUrl]", siteSeoSetting.Url);
            body.Replace("[SiteTitle]", siteSeoSetting.Title);

            _mailSender.Send(siteSeoSetting.Title, userEmailAddress, "تغییر اطلاعات کاربری", body.ToString());
        }
    }
}
