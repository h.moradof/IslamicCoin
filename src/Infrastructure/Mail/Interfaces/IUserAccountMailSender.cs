﻿namespace Infrastructure.Mail.Interfaces
{
    public interface IUserAccountMailSender
    {
        void SendEditUserEmail(string userEmailAddress);
    }
}
