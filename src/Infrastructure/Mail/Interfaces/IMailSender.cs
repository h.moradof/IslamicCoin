﻿namespace Infrastructure.Mail.Interfaces
{
    public interface IMailSender
    {
        string ErrorMessage { get; set; }
        bool Send(string title, string to, string subject, string body);
        bool Send(string title, string to, string[] cc, string[] bcc, string subject, string body);
        bool Send(string title, string to, string[] cc, string[] bcc, string[] attachmentsPhysicalAddress, string subject, string body);
    }
}