﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.Cache.Model;
using Infrastructure.Mail.Interfaces;
using System;
using System.Net.Mail;

namespace Infrastructure.Mail.Core
{
    public class MailSender : IMailSender
    {
        #region props

        private readonly MailSetting _mailSetting;
        public string ErrorMessage { get; set; }

        #endregion

        #region ctor

        public MailSender(IApplicationSettingCacheManager applicationSettingCacheManager)
        {
            _mailSetting = applicationSettingCacheManager.Get().MailSetting;
        }

        #endregion

        /// <summary>
        /// without attachment
        /// </summary>
        public bool Send(string title, string to, string subject, string body)
        {
            try
            {
                var mail = new MailMessage();
                mail.To.Add(to);
                mail.From = new MailAddress(_mailSetting.From, title, System.Text.Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.IsBodyHtml = true;

                mail.Priority = MailPriority.High;
                var smtp = new SmtpClient
                {
                    Credentials = new System.Net.NetworkCredential(_mailSetting.From, _mailSetting.Password),
                    Port = _mailSetting.PortNumber,
                    Host = _mailSetting.Host,
                    EnableSsl = _mailSetting.UseSsl
                };

                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
        }


        /// <summary>
        /// with cc and Bcc
        /// </summary>
        public bool Send(string title, string to, string[] cc, string[] bcc, string subject, string body)
        {
            try
            {
                var mail = new MailMessage();
                mail.To.Add(to);

                // add cc
                if (cc != null && cc.Length > 0)
                    foreach (var t in cc)
                        mail.CC.Add(t);

                // add bcc
                if (bcc != null && bcc.Length > 0)
                    for (int i = 0; i < bcc.Length; i++)
                        mail.Bcc.Add(cc[i]);


                mail.From = new MailAddress(_mailSetting.From, title, System.Text.Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.IsBodyHtml = true;

                mail.Priority = MailPriority.High;
                var smtp = new SmtpClient
                {
                    Credentials = new System.Net.NetworkCredential(_mailSetting.From, _mailSetting.Password),
                    Port = _mailSetting.UseSsl ? 587 : 25,
                    Host = _mailSetting.Host,
                    EnableSsl = _mailSetting.UseSsl
                };

                smtp.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }

        }


        /// <summary>
        /// with cc and Bcc and attachment
        /// </summary>
        public bool Send(string title, string to, string[] cc, string[] bcc, string[] attachmentsPhysicalAddress, string subject, string body)
        {
            try
            {
                var mail = new MailMessage();
                mail.To.Add(to);

                // add cc
                if (cc != null && cc.Length > 0)
                    foreach (var t in cc)
                        mail.CC.Add(t);

                // add bcc
                if (bcc != null && bcc.Length > 0)
                    for (int i = 0; i < bcc.Length; i++)
                        mail.Bcc.Add(cc[i]);

                mail.From = new MailAddress(_mailSetting.From, title, System.Text.Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.IsBodyHtml = true;

                // add attachment
                if(attachmentsPhysicalAddress != null && attachmentsPhysicalAddress.Length > 0)
                    foreach (var t in attachmentsPhysicalAddress)
                        mail.Attachments.Add(new Attachment(t));

                mail.Priority = MailPriority.High;
                var smtp = new SmtpClient
                {
                    Credentials = new System.Net.NetworkCredential(_mailSetting.From, _mailSetting.Password),
                    Port = _mailSetting.UseSsl ? 587 : 25,
                    Host = _mailSetting.Host,
                    EnableSsl = _mailSetting.UseSsl
                };

                smtp.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }

        }

    }

}