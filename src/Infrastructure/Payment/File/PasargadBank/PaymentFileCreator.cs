﻿using Infrastructure.Common;
using Infrastructure.Payment.File.PasargadBank.Model;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Hosting;

namespace Infrastructure.Payment.File.PasargadBank
{
    public static class PaymentFileCreator
    {

        private const string FilePath = "~/up/temp/";



        /// <summary>
        /// Create body of payment file
        /// </summary>
        /// <param name="data">the model that we need to create file</param>
        /// <returns>body of file</returns>
        public static string GetFileBody(IList<PasargadBankPaymentFileData> data)
        {
            var fileBody = new StringBuilder();

            foreach (var item in data)
            {
                fileBody.Append(string.Format("{0},{1},{2},{3},{4},{5}\r\n",
                    item.ShabaNumber,
                    item.Price,
                    item.CustomerFirstName,
                    item.CustomerLastName,
                    item.Description,
                    item.TransactionId));
            }

            return fileBody.ToString();
        }




        /// <summary>
        /// Create physical text file to pay money to people
        /// </summary>
        /// <returns>path of created file. e.g. "~/folder/file.txt"</returns>
        public static string CreateFile(string body)
        {
            var fileName = new RandomMaker().CreateRandomFileName("_PaymentFile.txt");
            var filePath = FilePath + fileName;

            using (var writer = new StreamWriter(HostingEnvironment.MapPath(filePath), true, Encoding.UTF8))
            {
                writer.Write(body);
            }

            return filePath;
        }

    }
}
