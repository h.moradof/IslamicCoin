﻿namespace Infrastructure.Payment.File.PasargadBank.Model
{
    public class PasargadBankPaymentFileData
    {
        public string ShabaNumber { get; set; }
        public int Price { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string Description { get; set; }
        public long? TransactionId { get; set; }
    }
}
