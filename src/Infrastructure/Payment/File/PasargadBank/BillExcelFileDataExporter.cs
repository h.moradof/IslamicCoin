﻿using DomainModels.Entities.Financial;
using Infrastructure.Exceptions;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Infrastructure.Payment.File.PasargadBank
{
    public static class BillExcelFileDataExporter
    {

        public static List<TransactionExcelFile> ExportFirstExcelFile(HttpFileCollectionBase files, string[] validExtensions, bool checkExtension = true, bool isFileRequired = true)
        {
            var rowList = new List<TransactionExcelFile>();

            foreach (string fileItem in files)
            {
                HttpPostedFileBase file = files[fileItem];

                if (file == null)
                    continue;

                if (file.ContentLength <= 0)
                    continue;

                if (checkExtension && !validExtensions.Contains(Path.GetExtension(file.FileName)))
                    continue;

                if (string.IsNullOrEmpty(file.FileName))
                    continue;

                string fileName = file.FileName;
                string fileContentType = file.ContentType;
                byte[] fileBytes = new byte[file.ContentLength];
                var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                using (var package = new ExcelPackage(file.InputStream))
                {
                    var currentSheet = package.Workbook.Worksheets;
                    var workSheet = currentSheet.First();

                    for (int row = 4; row < workSheet.Dimension.End.Row; row++)
                    {
                        var payPrice = Convert.ToInt32(workSheet.Cells[row, 11].Value);

                        if (payPrice > 0)
                        {
                            rowList.Add(new TransactionExcelFile
                            {
                                DocumentNumber = Convert.ToInt32(workSheet.Cells[row, 6].Value),
                                Description = workSheet.Cells[row, 9].Value.ToString(),
                                PayPrice = payPrice
                            });
                        }
                    }
                }

            }

            if (isFileRequired && rowList.Count == 0)
                throw new CustomException("انتخاب فایل الزامی است");

            return rowList;
        }
    }
}
