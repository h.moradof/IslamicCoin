﻿namespace Infrastructure.Cache.Model
{
    public class SmsSetting
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string SenderPhoneNumber { get; set; }
    }
}
