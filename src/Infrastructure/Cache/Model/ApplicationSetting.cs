﻿namespace Infrastructure.Cache.Model
{
    public sealed class ApplicationSetting
    {
        public string DefaultPicName { get; set; }
        public bool AllowNotAjaxRequests { get; set; }
        public bool IsActiveIpValidation { get; set; }
        public bool IsActiveTwoLevelLogin { get; set; }
        public MailSetting MailSetting { get; set; }
        public SmsSetting SmsSetting { get; set; }
    }
}
