﻿namespace Infrastructure.Cache.Model
{
    public class MailSetting
    {
        public string Host { get; set; }
        public string From { get; set; }
        public string Password { get; set; }
        public bool UseSsl { get; set; }
        public int PortNumber { get; set; }
    }
}
