﻿using DomainModels.Entities.Site;
using Infrastructure.Cache.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Cache
{
    public class SiteSeoSettingCacheManager : ISiteSeoSettingCacheManager
    {

        #region fileds

        private IEnumerable<SiteSeoSetting> _data;

        #endregion


        public void Cache(IEnumerable<SiteSeoSetting> data)
        {
            _data = data;
        }

        public SiteSeoSetting GetDefaultSetting()
        {
            return _data.FirstOrDefault();
        }

        public List<SiteSeoSetting> GetAll()
        {
            return _data.ToList<SiteSeoSetting>();
        }

    }
}
