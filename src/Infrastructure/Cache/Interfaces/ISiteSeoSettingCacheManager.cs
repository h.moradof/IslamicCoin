﻿using System.Collections.Generic;
using DomainModels.Entities.Site;

namespace Infrastructure.Cache.Interfaces
{
    public interface ISiteSeoSettingCacheManager
    {
        void Cache(IEnumerable<SiteSeoSetting> data);
        List<SiteSeoSetting> GetAll();
        SiteSeoSetting GetDefaultSetting();
    }
}