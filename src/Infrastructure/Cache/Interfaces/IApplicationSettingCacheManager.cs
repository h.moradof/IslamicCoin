﻿using Infrastructure.Cache.Model;

namespace Infrastructure.Cache.Interfaces
{
    public interface IApplicationSettingCacheManager
    {
        void Cache(ApplicationSetting data);
        ApplicationSetting Get();
    }
}