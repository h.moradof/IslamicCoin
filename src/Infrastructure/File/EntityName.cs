﻿namespace Infrastructure.File
{
    public enum EntityName : byte
    {
        Photo,
        UploadCenter,
        Certificate
    }
}
