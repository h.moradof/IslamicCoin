﻿using System.Linq;
using System.Web;
using System.IO;

namespace Infrastructure.File
{
    public static class FileValidator
    {

        public static readonly string ValidPictureExtensions = ".jpg,.jpeg,.gif,.png";
        public static readonly string ValidCatalogExtensions = ".rar,.zip,.pdf";
        public static readonly string ValidFileExtensions = ".rar,.zip,.pdf,.jpg,.jpeg,.gif,.png,.jpg,.jpeg,.gif,.png,.mp3,.mp4,.flv,.doc,.docx,.txt";


        /// <summary>
        /// extension method for checking file extensions
        /// </summary>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public static FileValidatorResult IsValidCatalogFile(this HttpPostedFileBase postedFile)
        {
            string fileExtension = Path.GetExtension(postedFile.FileName).ToLowerInvariant();

            var extensions = ValidCatalogExtensions.Split(',');

            var isValid = extensions.Contains(fileExtension);

            return isValid ? FileValidatorResult.IsValid : FileValidatorResult.InvalidExtension;
        }


        /// <summary>
        /// متد الحاقی بررسی پسوند تصاویر
        /// </summary>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public static FileValidatorResult IsValidPicture(this HttpPostedFileBase postedFile)
        {
            string fileExtension = Path.GetExtension(postedFile.FileName).ToLowerInvariant();

            var extensions = ValidPictureExtensions.Split(',');

            var isValid = extensions.Contains(fileExtension);

            return isValid ? FileValidatorResult.IsValid : FileValidatorResult.InvalidExtension;
        }


        /// <summary>
        /// متد الحاقی بررسی پسوند فایل ها
        /// </summary>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public static FileValidatorResult IsValidFile(this HttpPostedFileBase postedFile, EntityName entityName)
        {
            string fileExtension = Path.GetExtension(postedFile.FileName).ToLowerInvariant();

            var isValid = ValidFileExtensions.Split(',').Contains(fileExtension);

            if (!isValid)
                return FileValidatorResult.InvalidExtension;

            if (entityName == EntityName.UploadCenter)
            {
                // check length
                if (postedFile.ContentLength > FileManager.ValidUploadCenterLength)
                    return FileValidatorResult.InvalidLength;
            }

            return FileValidatorResult.IsValid;
        }


        public static bool IsValidPicture(string pictureName)
        {
            string fileExtension = Path.GetExtension(pictureName).ToLowerInvariant();

            return ValidPictureExtensions.Split(',').Contains(fileExtension);
        }

        public static bool IsValidFile(string fileName)
        {
            string fileExtension = Path.GetExtension(fileName).ToLowerInvariant();

            return ValidFileExtensions.Split(',').Contains(fileExtension);
        }

        public static bool IsValidCatalogFile(string fileName)
        {
            string fileExtension = Path.GetExtension(fileName).ToLowerInvariant();

            return ValidCatalogExtensions.Split(',').Contains(fileExtension);
        }

        /// <summary>
        /// نتیجه ولیدیشن فایل ها
        /// </summary>
        public enum FileValidatorResult
        {
            IsValid,
            InvalidExtension,
            InvalidLength,
            InvalidSize
        }

    }
}