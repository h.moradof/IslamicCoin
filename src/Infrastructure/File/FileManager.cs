﻿using Infrastructure.Common;
using System;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Drawing;
using Infrastructure.Exceptions;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Infrastructure.File
{

    /// <summary>
    /// Manage Upload Files
    /// </summary>
    public static class FileManager
    {

        #region error messages

        private const string InvalidSizeMessage = "اندازه تصویر بیش از حد مجاز است";
        private const string InvalidExtensionMessage = "فرمت فایل غیر مجاز است";
        private const string ErrorOnUploadMessage = "در حال حاضر آپلود فایل میسر نیست، لطفا لحظاتی دیگر مجددا اقدام نمایید";
        public const int ValidUploadCenterLength = 10000000; // byte
        private static readonly string InvalidLengthMessage = string.Format("حداکثر حجم مجاز برای فایل {0} کیلوبایت می باشد", ValidUploadCenterLength / 1000);

        #endregion

        

        /// <summary>
        /// Upload first valid photo and return uploaded photo name
        /// </summary>
        /// <param name="files">Files of Request</param>
        /// <param name="entityName">name of entity</param>
        /// <param name="isPhotoRequired">flag to turn on/off throw exception when all files are invalid</param>
        /// <exception cref="CustomException">throw when photo is required and all files are invalid</exception>
        /// <returns>uploaded photo name</returns>
        public static string UploadFirstValidPhoto(HttpFileCollectionBase files, EntityName entityName, bool isPhotoRequired)
        {
            foreach (string fileName in files)
            {
                HttpPostedFileBase postedFile = files[fileName];

                if (postedFile.ContentLength > 0)
                {
                    // تصویر
                    var uploadResult = UploadPicture(postedFile, entityName);

                    if (uploadResult.UploadResult == UploadResult.Success)
                        return uploadResult.UploadedFileName;
                }
            }

            if (isPhotoRequired)
                throw new CustomException("انتخاب تصویر الزامی است");

            return null;
        }


        /// <summary>
        /// Upload first valid photo and return uploaded photo name
        /// </summary>
        /// <param name="files">Files of Request</param>
        /// <param name="entityName">name of entity</param>
        /// <param name="isPhotoRequired">flag to turn on/off throw exception when all files are invalid</param>
        /// <exception cref="CustomException">throw when photo is required and all files are invalid</exception>
        /// <returns>a list of uploaded photo names</returns>
        public static List<string> UploadValidPhotos(HttpFileCollectionBase files, EntityName entityName, bool isPhotoRequired)
        {
            var uploadedPhotos = new List<string>();

            foreach (string fileName in files)
            {
                HttpPostedFileBase postedFile = files[fileName];

                if (postedFile.ContentLength > 0)
                {
                    // تصویر
                    var uploadResult = UploadPicture(postedFile, entityName);

                    if (uploadResult.UploadResult == UploadResult.Success)
                        uploadedPhotos.Add(uploadResult.UploadedFileName);
                }
            }

            if (uploadedPhotos.Count == 0 && isPhotoRequired)
                throw new CustomException("انتخاب تصویر الزامی است");

            return uploadedPhotos;
        }


        /// <summary>
        /// Upload first valid file and return uploaded photo name
        /// </summary>
        /// <param name="files">Files of Request</param>
        /// <param name="entityName">name of entity</param>
        /// <param name="isFileRequired">flag to turn on/off throw exception when all files are invalid</param>
        /// <exception cref="CustomException">throw when file is required and all files are invalid</exception>
        /// <returns>uploaded file name</returns>
        public static string UploadFirstValidFile(HttpFileCollectionBase files, EntityName entityName, bool isFileRequired)
        {
            foreach (string fileName in files)
            {
                HttpPostedFileBase postedFile = files[fileName];

                if (postedFile.ContentLength > 0)
                {
                    // فایل
                    var uploadResult = UploadFile(postedFile, entityName);

                    if (uploadResult.UploadResult == UploadResult.Success)
                        return uploadResult.UploadedFileName;
                }
            }

            if (isFileRequired)
                throw new CustomException("انتخاب فایل الزامی است");

            return null;
        }


        /// <summary>
        /// Upload first valid files and return uploaded photo name
        /// </summary>
        /// <param name="files">Files of Request</param>
        /// <param name="entityName">name of entity</param>
        /// <param name="isFileRequired">flag to turn on/off throw exception when all files are invalid</param>
        /// <exception cref="CustomException">throw when file is required and all files are invalid</exception>
        /// <returns>a list of uploaded file names</returns>
        public static List<string> UploadValidFiles(HttpFileCollectionBase files, EntityName entityName, bool isFileRequired)
        {
            var uploadedfiles = new List<string>();

            foreach (string fileName in files)
            {
                HttpPostedFileBase postedFile = files[fileName];

                if (postedFile.ContentLength > 0)
                {
                    // فایل
                    var uploadResult = UploadFile(postedFile, entityName);

                    if (uploadResult.UploadResult == UploadResult.Success)
                        uploadedfiles.Add(uploadResult.UploadedFileName);
                }
            }

            if (uploadedfiles.Count == 0 && isFileRequired)
                throw new CustomException("انتخاب فایل الزامی است");

            return uploadedfiles;
        }


        /// <summary>
        /// delete a file from server
        /// </summary>
        /// <param name="fileName">file name that will be delete</param>
        /// <param name="entityName">entityname for finding path of file</param>
        public static void DeleteFile(string fileName, EntityName entityName)
        {
            if (string.IsNullOrEmpty(fileName))
                return;

            fileName = Path.GetFileName(fileName); // تمیز سازی امنیتی است

            var path = Path.Combine(UploadFilePathManager.GetUploadPhysicalPath(entityName), fileName);

            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
        }


        /// <summary>
        /// delete files from server
        /// </summary>
        /// <param name="fileNames">an Enumerable of file names that will be delete</param>
        /// <param name="entityName">entityname for finding path of file</param>
        public static void DeleteFiles(IEnumerable<string> fileNames, EntityName entityName)
        {
            foreach (var fileName in fileNames)
            {
                DeleteFile(fileName, entityName);
            }
        }



        #region private methods


        /// <summary>
        /// uploading picture
        /// </summary>
        /// <param name="postedFile">Posted file to the server</param>
        /// <param name="userId">Id of the user who uploading picture</param>
        /// <returns>uploading picture result. contains uploaded file name, upload result (Success/Invalid file/Error) and uploading error message</returns>
        private static FileUploadResult UploadPicture(HttpPostedFileBase postedFile, EntityName entityName)
        {
            try
            {
                #region validation picture extension and length

                var pictureValidationResult = postedFile.IsValidPicture();

                // check extension
                switch (pictureValidationResult)
                {
                    case FileValidator.FileValidatorResult.InvalidExtension:
                        return new FileUploadResult(UploadResult.InvalidFileExtension, string.Empty, InvalidExtensionMessage);
                    case FileValidator.FileValidatorResult.InvalidLength:
                        return new FileUploadResult(UploadResult.InvalidFileSize, string.Empty, InvalidLengthMessage);
                    case FileValidator.FileValidatorResult.InvalidSize:
                        return new FileUploadResult(UploadResult.InvalidFileSize, string.Empty, InvalidSizeMessage);
                }

                #endregion


                #region Upload Picture And Return PictureName

                string pictureName = CreateUniqueFileName(postedFile.FileName);

                // upload image with validation data
                using (Image image = Image.FromStream(postedFile.InputStream, true, true))
                {
                    image.Save(HostingEnvironment.MapPath(UploadFilePathManager.GetUploadPath(entityName) + pictureName));
                }

                return new FileUploadResult(UploadResult.Success, pictureName, string.Empty);

                #endregion

            }
            catch (Exception)
            {
                return new FileUploadResult(UploadResult.Error, string.Empty, ErrorOnUploadMessage);
            }
        }
        

        /// <summary>
        /// uploading file
        /// </summary>
        /// <param name="postedFile">Posted file to the server</param>
        /// <param name="entityName"></param>
        /// <returns>uploading file result. contains uploaded file name, upload result (Success/Invalid file/Error) and uploading error message</returns>
        private static FileUploadResult UploadFile(HttpPostedFileBase postedFile, EntityName entityName)
        {
            try
            {

                #region validation file extension and length

                var fileValidationResult = postedFile.IsValidFile(EntityName.UploadCenter);

                // check extension
                switch (fileValidationResult)
                {
                    case FileValidator.FileValidatorResult.InvalidExtension:
                        return new FileUploadResult(UploadResult.InvalidFileExtension, string.Empty, InvalidExtensionMessage);
                    case FileValidator.FileValidatorResult.InvalidLength:
                        return new FileUploadResult(UploadResult.InvalidFileSize, string.Empty, InvalidLengthMessage);
                    case FileValidator.FileValidatorResult.InvalidSize:
                        return new FileUploadResult(UploadResult.InvalidFileSize, string.Empty, InvalidSizeMessage);
                }

                #endregion


                #region uploading file

                string fileName = CreateUniqueFileName(postedFile.FileName);

                postedFile.SaveAs(HostingEnvironment.MapPath(UploadFilePathManager.GetUploadPath(entityName) + fileName));

                #endregion


                return new FileUploadResult(UploadResult.Success, fileName, string.Empty);

            }
            catch (Exception)
            {
                return new FileUploadResult(UploadResult.Error, string.Empty, FileManager.ErrorOnUploadMessage);
            }
        }
        

        /// <summary>
        /// create unique file name for uploading files
        /// </summary>
        /// <param name="entityName">entity name (Model name)</param>
        /// <returns>unique file name with file extension</returns>
        private static string CreateUniqueFileName(string postedFileName)
        {
            return new RandomMaker().CreateRandomFileName(Path.GetExtension(postedFileName));
        }

        #endregion


        public enum UploadResult
        {
            Success,
            InvalidFileExtension,
            InvalidFileSize,
            Error
        }

    }
}