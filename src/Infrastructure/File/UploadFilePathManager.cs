﻿//using Infrastructure.Core;
using System.IO;
using System.Web.Hosting;

namespace Infrastructure.File
{
    public static class UploadFilePathManager
    {

        public const string RootPath = "~/up/";


        #region upload control methods

        /// <summary>
        /// دریافت مسیر وبی محل آپلود فابل ها
        /// </summary>
        public static string GetUploadPath(EntityName entityName)
        {
            return string.Format("{0}{1}/", RootPath, entityName.ToString().ToLowerInvariant());
        }


        /// <summary>
        /// دریافت مسیر وبی محل آپلود فابل ها
        /// </summary>
        public static string GetUploadPathWithoutRootCharacter(EntityName entityName)
        {
            return string.Format("{0}{1}/", RootPath, entityName.ToString().ToLowerInvariant());
        }


        /// <summary>
        /// دریافت مسیر فیزیکی یا ویندوزی محل آپلود فایل ها
        /// </summary>
        public static string GetUploadPhysicalPath(EntityName entityName)
        {
            return HostingEnvironment.MapPath(GetUploadPath(entityName));
        }

        #endregion

    }
}