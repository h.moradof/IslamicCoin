﻿USE [IslamicCoin]
GO

-- ==================================================== sp_GetMonthlyVisits ====================================================
CREATE PROC sp_GetMonthlyVisits
(
	@Month_1_StartDate DATE, @Month_1_EndDate DATE, 
	@Month_2_StartDate DATE, @Month_2_EndDate DATE,
	@Month_3_StartDate DATE, @Month_3_EndDate DATE,
	@Month_4_StartDate DATE, @Month_4_EndDate DATE,
	@Month_5_StartDate DATE, @Month_5_EndDate DATE,
	@Month_6_StartDate DATE, @Month_6_EndDate DATE,
	@Month_7_StartDate DATE, @Month_7_EndDate DATE,
	@Month_8_StartDate DATE, @Month_8_EndDate DATE,
	@Month_9_StartDate DATE, @Month_9_EndDate DATE,
	@Month_10_StartDate DATE, @Month_10_EndDate DATE,
	@Month_11_StartDate DATE, @Month_11_EndDate DATE,
	@Month_12_StartDate DATE, @Month_12_EndDate DATE
)
AS
BEGIN

	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_1_StartDate AND @Month_1_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_2_StartDate AND @Month_2_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_3_StartDate AND @Month_3_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_4_StartDate AND @Month_4_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_5_StartDate AND @Month_5_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_6_StartDate AND @Month_6_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_7_StartDate AND @Month_7_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_8_StartDate AND @Month_8_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_9_StartDate AND @Month_9_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_10_StartDate AND @Month_10_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_11_StartDate AND @Month_11_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_12_StartDate AND @Month_12_EndDate   

END