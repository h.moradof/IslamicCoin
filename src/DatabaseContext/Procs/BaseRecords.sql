﻿USE [IslamicCoin]
GO

-- ------------------------------------------------------------------- [Access] ----------------------------------------------------------
IF (NOT EXISTS(SELECT * FROM [Access].[Role]))
BEGIN
	SET IDENTITY_INSERT [Access].[Role] ON;

	INSERT INTO [Access].[Role](Id, Name, RoleName, CreatedOn) 
	VALUES	(1, 'Administrators', 1, GETDATE()),
			(2, 'RegisteredUsers', 2, GETDATE()),	
			(3, 'PermitedUsers', 3, GETDATE()),	
			(4, 'Employees', 4, GETDATE());	

	SET IDENTITY_INSERT [Access].[Role] OFF;
END
GO

-- ------------------------------------------------------------------- [HumanResource] ----------------------------------------------------------
IF (NOT EXISTS(SELECT * FROM [HumanResource].[User]))
BEGIN
	SET IDENTITY_INSERT [HumanResource].[User] ON 

	INSERT [HumanResource].[User] ([Id], [FullName], [Username], [Password], [AccountStatus], [BanReason], [ActivateCode], [EmailAddress], [SecondLevelPassword], [RowGuid], [MaximumRialWithdrawalPrice], [MaximumDolorWithdrawalAmount], [IdentificationCardPhotoName], [FaceAndNationalCardPhotoName], [MobileNumberAcceptStatus], [PhoneNumberAcceptStatus], [IdentificationCardAcceptStatus], [FaceAndNationalCardAcceptStatus], [RoleId], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'مدیریت سایت', N'09354512264', N'DC9AF454F79226F429B3F47919AE9F5C', 2, NULL, N'3526', N'h.moradof@gmail.com', N'cefefb1c-1fa7-45d8-920c-ecc09458ff4f', N'b01d3ed7-96d0-407d-bb90-97c4f5912681', 80000000, CAST(2.00000000 AS Decimal(18, 8)), NULL, NULL, 3, 3, 3, 3, 1, GETDATE(), NULL, NULL)
	INSERT [HumanResource].[User] ([Id], [FullName], [Username], [Password], [AccountStatus], [BanReason], [ActivateCode], [EmailAddress], [SecondLevelPassword], [RowGuid], [MaximumRialWithdrawalPrice], [MaximumDolorWithdrawalAmount], [IdentificationCardPhotoName], [FaceAndNationalCardPhotoName], [MobileNumberAcceptStatus], [PhoneNumberAcceptStatus], [IdentificationCardAcceptStatus], [FaceAndNationalCardAcceptStatus], [RoleId], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'حمیدرضا مراداف', N'09197081078', N'DC9AF454F79226F429B3F47919AE9F5C', 2, NULL, N'8366', N'hamidreza@moradof.ir', N'8f23ea6c-4f99-41f8-ab00-245b6d5814c7', N'e829b431-59a4-442b-9c08-53762f97533b', 80000000, CAST(2.00000000 AS Decimal(18, 8)), NULL, NULL, 1, 1, 1, 1, 2, GETDATE(), NULL, NULL)
	
	SET IDENTITY_INSERT [HumanResource].[User] OFF
END
GO




-- ------------------------------------------------------------------- [Setting] ----------------------------------------------------------
IF (NOT EXISTS(SELECT * FROM [Setting].[DealProcSetting]))
BEGIN
	SET IDENTITY_INSERT [Setting].[DealProcSetting] ON;

	INSERT [Setting].[DealProcSetting] ([Id], [IsOnNow], [TryToChangeState], [IsLock], [CreatedOn]) VALUES (1, 0, 0, 0, GETDATE())

	SET IDENTITY_INSERT [Setting].[DealProcSetting] OFF;
END
GO


IF (NOT EXISTS(SELECT * FROM [Setting].[BitcoinFee]))
BEGIN
	SET IDENTITY_INSERT [Setting].[BitcoinFee] ON;

	INSERT [Setting].[BitcoinFee] ([Id], [Fee], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (1, CAST(0.010000 AS Decimal(18, 6)), CAST(N'2017-06-04 00:00:00.000' AS DateTime), NULL, NULL)
	INSERT [Setting].[BitcoinFee] ([Id], [Fee], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (2, CAST(0.020000 AS Decimal(18, 6)), CAST(N'2017-06-04 00:00:00.000' AS DateTime), NULL, NULL)

	SET IDENTITY_INSERT [Setting].[BitcoinFee] OFF;
END
GO


IF (NOT EXISTS(SELECT * FROM [Setting].[FinancialSetting]))
BEGIN
	SET IDENTITY_INSERT [Setting].[FinancialSetting] ON;

	INSERT [Setting].[FinancialSetting] ([Id], [Fee], [MaximumRialWithdrawalPrice], [MaximumDolorWithdrawalAmount], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (1, CAST(0.0100 AS Decimal(18, 4)), 1000000, CAST(10.00000000 AS Decimal(18, 8)), CAST(N'2017-06-04 00:00:00.000' AS DateTime), NULL, NULL)
	
	SET IDENTITY_INSERT [Setting].[FinancialSetting] OFF;
END
GO
-- ------------------------------------------------------------------- [Shop] ----------------------------------------------------------
IF (NOT EXISTS(SELECT * FROM [Shop].[RequestStatus]))
BEGIN
	SET IDENTITY_INSERT [Shop].[RequestStatus] ON 

	INSERT [Shop].[RequestStatus] ([Id], [Name], [IdentityName], [DisplayName], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (1, 0, N'OnProcessing', N'در حال پردازش', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, NULL)
	INSERT [Shop].[RequestStatus] ([Id], [Name], [IdentityName], [DisplayName], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (2, 1, N'Welded', N'معامله شده', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, NULL)
	INSERT [Shop].[RequestStatus] ([Id], [Name], [IdentityName], [DisplayName], [CreatedOn], [UpdatedOn], [DeletedOn]) VALUES (3, 2, N'Canceled', N'کنسل شده', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, NULL)
	
	SET IDENTITY_INSERT [Shop].[RequestStatus] OFF
END
GO


-- ------------------------------------------------------------------- [Financial] ----------------------------------------------------------
IF (NOT EXISTS(SELECT * FROM [Financial].[PaymentStatus]))
BEGIN
	SET IDENTITY_INSERT [Financial].[PaymentStatus] ON 

	INSERT [Financial].[PaymentStatus] ([Id], [Name], [IdentityName], [CreatedOn], [UpdatedOn], [DeletedOn], [DisplayName]) VALUES (1, 0, N'Pending', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, NULL, N'در حال بررسی')
	INSERT [Financial].[PaymentStatus] ([Id], [Name], [IdentityName], [CreatedOn], [UpdatedOn], [DeletedOn], [DisplayName]) VALUES (2, 1, N'Accepted', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, NULL, N'تایید شده')
	INSERT [Financial].[PaymentStatus] ([Id], [Name], [IdentityName], [CreatedOn], [UpdatedOn], [DeletedOn], [DisplayName]) VALUES (3, 2, N'Declined', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, NULL, N'رد شده')

	SET IDENTITY_INSERT [Financial].[PaymentStatus] OFF
END
GO


IF (NOT EXISTS(SELECT * FROM [Financial].[WithdrawalRequestStatus]))
BEGIN
	SET IDENTITY_INSERT [Financial].[WithdrawalRequestStatus] ON 

	INSERT [Financial].[WithdrawalRequestStatus] ([Id], [Name], [IdentityName], [CreatedOn], [UpdatedOn], [DeletedOn], [DisplayName]) VALUES (1, 0, N'Pending', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, NULL, N'در حال بررسی')
	INSERT [Financial].[WithdrawalRequestStatus] ([Id], [Name], [IdentityName], [CreatedOn], [UpdatedOn], [DeletedOn], [DisplayName]) VALUES (2, 1, N'OnProcessing', CAST(N'2016-01-01 00:00:00.000' AS DateTime), NULL, NULL, N'در حال پردازش')
	INSERT [Financial].[WithdrawalRequestStatus] ([Id], [Name], [IdentityName], [CreatedOn], [UpdatedOn], [DeletedOn], [DisplayName]) VALUES (3, 2, N'Paid', CAST(N'2016-01-01 00:00:00.000' AS DateTime), NULL, NULL, N'پرداخت شده')

	SET IDENTITY_INSERT [Financial].[WithdrawalRequestStatus] OFF
END
GO

IF (NOT EXISTS(SELECT * FROM [Financial].[AccountAcceptStatus]))
BEGIN
	SET IDENTITY_INSERT [Financial].[AccountAcceptStatus] ON 

	INSERT [Financial].[AccountAcceptStatus] ([Id], [Name], [IdentityName], [CreatedOn], [DisplayName]) 
	VALUES 
		(1, 1, N'Pending', GETDATE(), N'در حال بررسی'),
		(2, 2, N'Accepted', GETDATE(), N'تایید شده'),
		(3, 3, N'Declined', GETDATE(), N'رد شده')


	SET IDENTITY_INSERT [Financial].[AccountAcceptStatus] OFF
END
GO



IF (NOT EXISTS(SELECT * FROM [Financial].[Bank]))
BEGIN
	SET IDENTITY_INSERT [Financial].[Bank] ON 

	INSERT [Financial].[Bank] ([Id], [Name], [CreatedOn]) 
	VALUES
		(1, N'پاسارگاد', GETDATE()),
		(2, N'ملی', GETDATE()),
		(3, N'ملت', GETDATE()),
		(4, N'اقتصاد نوین', GETDATE()),
		(5, N'کشاورزی', GETDATE()),
		(6, N'سامان', GETDATE()),
		(7, N'پارسیان', GETDATE()),
		(8, N'مسکن', GETDATE()),
		(9, N'تجارت', GETDATE()),
		(10, N'سپه', GETDATE()),
		(11, N'صادرات', GETDATE())

	SET IDENTITY_INSERT [Financial].[Bank] OFF
END
GO

-- ------------------------------------------------------------------- [Site] ----------------------------------------------------------
IF (NOT EXISTS(SELECT * FROM [Site].[SiteSeoSetting]))
BEGIN
	SET IDENTITY_INSERT [Site].[SiteSeoSetting] ON;

	INSERT INTO [Site].[SiteSeoSetting](Id, Title, Url, MetaKeywords, MetaDescription, CreatedOn) 
	VALUES (1, N'سایت من', N'http://www.mybourse.com', N'خرید و فروش ارز اینترنتی,ارز,ارز اینترنتی', N'به سایت ارزی من خوش آمدید' , GETDATE());

	SET IDENTITY_INSERT [Site].[SiteSeoSetting] OFF;
END
GO


IF (NOT EXISTS(SELECT * FROM [Site].[SiteVisit]))
BEGIN
	SET IDENTITY_INSERT [Site].[SiteVisit] ON;

	INSERT [Site].[SiteVisit] ([Id], [Ip], [VisitDate], [Number], [CreatedOn], [UpdatedOn], [DeletedOn]) 
	VALUES (1, N'1.1.1.1', CAST(N'2017-06-04' AS Date), 1, CAST(N'2017-06-04 00:00:00.000' AS DateTime), NULL, NULL)

	SET IDENTITY_INSERT [Site].[SiteVisit] OFF;
END