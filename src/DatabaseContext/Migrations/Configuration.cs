namespace DatabaseContext.Migrations
{
    using FillDB;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext.Context.DefaultDatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DatabaseContext.Context.DefaultDatabaseContext context)
        {
            var dbFiller = new DbFiller();

           
           // dbFiller.InsertBaseData(context);
            // dbFiller.InsertTestData(context);
            // dbFiller.AddElmahToDb(context);
        }
    }
}
