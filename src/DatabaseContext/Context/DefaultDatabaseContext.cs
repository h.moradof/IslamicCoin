﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using System.Linq;
using DomainModels.Entities.Base;
using System.Data.Entity.Validation;
using DatabaseContext.ErrorFormater;
using System.Threading.Tasks;
using System.Threading;
using DomainModels.Entities.Financial;
using DomainModels.Entities.Setting;
using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Shop;
using DatabaseContext.Migrations;

namespace DatabaseContext.Context
{
    public class DefaultDatabaseContext : DbContext, IUnitOfWork
    {

        #region ctor

        public DefaultDatabaseContext(string connectionStringName)
            : base(connectionStringName)
        { }

        public DefaultDatabaseContext()
            : base("Default")
        { }

        #endregion


        #region static ctor

        static DefaultDatabaseContext()
        {
            // On server
            // Database.SetInitializer<DefaultDatabaseContext>(null);

            // On local host
            Database.SetInitializer<DefaultDatabaseContext>(new MigrateDatabaseToLatestVersion<DefaultDatabaseContext, Configuration>());

            // On Test
            // Database.SetInitializer<DefaultDatabaseContext>(new DropCreateDatabaseAlways<DefaultDatabaseContext>());
        }

        #endregion


        #region OnModelCreating

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // remove PluralizingTableNameConvention
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // decimal precision
            CorrectiveDecimalPrecisions(modelBuilder);

            // delete strategies
            SetDeleteStrategies(modelBuilder);

            // load props
            var asm = Assembly.GetAssembly(typeof(DomainModels.Entities.Base.BaseEntity));
            LoadEntities(asm, modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        #endregion


        #region Set Delete Strategies

        private static void SetDeleteStrategies(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        #endregion


        #region Corrective decimal properties

        /// <summary>
        /// Corrective decimal precision
        /// </summary>
        /// <param name="modelBuilder"></param>
        private static void CorrectiveDecimalPrecisions(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(c => c.MaximumDolorWithdrawalAmount).HasPrecision(18, 8);
            modelBuilder.Entity<BitcoinFee>().Property(c => c.Fee).HasPrecision(18, 6);
            modelBuilder.Entity<DolorWithdrawalRequest>().Property(c => c.Amount).HasPrecision(18, 8);
            modelBuilder.Entity<DolorWithdrawalRequest>().Property(c => c.BitcoinFee).HasPrecision(18, 6);
            modelBuilder.Entity<MoneyAccount>().Property(c => c.TotalDolorAmount).HasPrecision(18, 8);
            modelBuilder.Entity<UserDolorPayment>().Property(c => c.Amount).HasPrecision(18, 8);
            modelBuilder.Entity<SaleRequest>().Property(c => c.Amount).HasPrecision(18, 8);
            modelBuilder.Entity<BuyRequest>().Property(c => c.Amount).HasPrecision(18, 8);
            modelBuilder.Entity<FinancialSetting>().Property(c => c.Fee).HasPrecision(18, 4);
            modelBuilder.Entity<FinancialSetting>().Property(c => c.MaximumDolorWithdrawalAmount).HasPrecision(18, 8);

        }

        #endregion


        #region LoadEntities

        /// <summary>
        /// Load Entities into db context
        /// </summary>
        /// <param name="asm"></param>
        /// <param name="modelBuilder"></param>
        private void LoadEntities(Assembly asm, DbModelBuilder modelBuilder)
        {
            var entityTypes = asm.GetTypes()
                                    .Where(type => type.BaseType != null &&
                                           type.BaseType.IsAbstract &&
                                           !type.IsAbstract &&
                                           (type.BaseType == typeof(BaseEntity) || type.BaseType.BaseType == typeof(BaseEntity))
                                           )
                                    .ToList();

            var entityMethod = typeof(DbModelBuilder).GetMethod("Entity");
            entityTypes.ForEach(type =>
            {
                entityMethod.MakeGenericMethod(type).Invoke(modelBuilder, new object[] { });
            });
        }

        #endregion


        #region IUnitOfWork Members

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                throw new FormattedDbEntityValidationException(e);
            }
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            try
            {
                return base.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException e)
            {
                throw new FormattedDbEntityValidationException(e);
            }
        }

        #endregion

    }
}