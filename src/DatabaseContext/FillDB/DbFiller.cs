﻿using System;
using System.IO;
using System.Linq;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using DatabaseContext.Context;

//Microsoft.SqlServer.Smo.dll
using Microsoft.SqlServer.Management.Smo;
//Microsoft.SqlServer.ConnectionInfo.dll
using Microsoft.SqlServer.Management.Common;
using DomainModels.Entities.Site;
using DomainModels.Entities.Access;
using System.Text;

namespace DatabaseContext.FillDB
{
    public class DbFiller
    {

        #region props

        private static string ProjectRootPath = System.AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Debug\", "").Replace(@"bin\Release\", "");

        private static string[] BasicScriptFiles = 
        {
            "BaseRecords.sql",
            "Procedures.sql"
        };

        private static string[] TestScriptFiles =
        {
            "TestRecords.sql"
        };

        #endregion


        #region Insert Data

        public void InsertBaseData(IUnitOfWork db)
        {
            if (ProjectRootPath.Contains("Web"))
                return;

            ExecuteAllSqlFiles(db.Database.Connection.ConnectionString, BasicScriptFiles);
        }

        public void AddElmahToDb(IUnitOfWork db)
        {
            if (ProjectRootPath.Contains("Web"))
                return;

            if( !db.Set<Role>().Any())
                ExecuteAllSqlFiles(db.Database.Connection.ConnectionString, new string[] { "ELMAH.sql" });
        }

        public void InsertTestData(IUnitOfWork db)
        {
            if (ProjectRootPath.Contains("Web"))
                return;

            ExecuteAllSqlFiles(db.Database.Connection.ConnectionString, TestScriptFiles);
        }

        #endregion


        #region private methods

        private void ExecuteAllSqlFiles(string connectionString, string[] scriptFilesToRun)
        {
            var procDirectory = new StringBuilder(ProjectRootPath).Replace(@"Web\", "");
            procDirectory.Append(@"Procs\\");

            var filePath = string.Empty;

            foreach (string fileName in scriptFilesToRun)
            {
                filePath = procDirectory + fileName;
                var fileInfo = new FileInfo(filePath);
                var script = fileInfo.OpenText().ReadToEnd();
                var connection = new SqlConnection(connectionString);
                var server = new Server(new ServerConnection(connection));
                server.ConnectionContext.ExecuteNonQuery(script);
            }
        }

        #endregion

    }
}
