﻿using DomainModels.Entities.Base;
using DatabaseContext.Context;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Threading.Tasks;
using Infrastructure.IQueryableExtensions;
using System.Collections.Generic;
using Infrastructure.Pagination;

namespace Services.Base
{
    public abstract class GenericService<T> : IGenericService<T> where T : BaseEntity
    {

        #region props

        protected readonly IUnitOfWork _uow;
        protected readonly IDbSet<T> _dbSet;
        protected readonly Expression<Func<T, bool>> DefaultPredicate;

        #endregion

        #region ctor

        public GenericService(IUnitOfWork uow)
        {
            _uow = uow;
            _dbSet = _uow.Set<T>();
            DefaultPredicate = (c => c.DeletedOn == null);
        }

        #endregion


        /// <summary>
        /// Get Count of an entity Set
        /// </summary>
        /// <returns></returns>
        public virtual long Count()
        {
            return AddBasicConventions().Where(c => c.DeletedOn == null).Count();
        }

        /// <summary>
        /// Get Count of an entity Set
        /// </summary>
        /// <returns></returns>
        public async virtual Task<long> CountAsync()
        {
            return await AddBasicConventions().CountAsync();
        }


        /// <summary>
        /// Get Count of an entity Set
        /// </summary>
        /// <param name="predicate">condition to filter result</param>
        /// <returns></returns>
        public virtual long Count(Expression<Func<T, bool>> predicate)
        {
            return AddBasicConventions().Where(predicate).Count();
        }

        /// <summary>
        /// Get Count of an entity Set
        /// </summary>
        /// <param name="predicate">condition to filter result</param>
        /// <returns></returns>
        public virtual async Task<long> CountAsync(Expression<Func<T , bool>> predicate)
        {
            return await AddBasicConventions().Where(predicate).CountAsync();
        }


        /// <summary>
        /// Add an entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual T Add(T model)
        {
            return _dbSet.Add(model);
        }


        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="entity">entity to edit</param>
        public virtual T Edit(T model)
        {
            model.UpdatedOn = DateTime.Now;
            _uow.Entry<T>(model).State = EntityState.Modified;
            return _dbSet.FirstOrDefault(c => c.Id == model.Id);
        }

        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="entity">entity to edit</param>
        public virtual async Task<T> EditAsync(T model)
        {
            model.UpdatedOn = DateTime.Now;
            _uow.Entry<T>(model).State = EntityState.Modified;
            return await _dbSet.FirstOrDefaultAsync(c => c.Id == model.Id);
        }


        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="id">primary key property of the entity</param>
        public virtual void Delete(long id)
        {
            var entityToDelete = _dbSet.FirstOrDefault(c => c.Id == id);
            if (entityToDelete != null)
                entityToDelete.DeletedOn = DateTime.Now;
        }

        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="id">primary key property of the entity</param>
        public virtual async Task DeleteAsync(long id)
        {
            var entityToDelete = await _dbSet.FirstOrDefaultAsync(c => c.Id == id);
            if(entityToDelete != null)
                entityToDelete.DeletedOn = DateTime.Now;
        }


        /// <summary>
        /// Find an entity
        /// </summary>
        /// <param name="id">primary key property of the entity</param>
        /// <returns>an entity</returns>
        public virtual T Find(long id)
        {
            return AddBasicConventions().FirstOrDefault(c => c.Id == id);
        }

        /// <summary>
        /// Find an entity
        /// </summary>
        /// <param name="id">primary key property of the entity</param>
        /// <returns>an entity</returns>
        public virtual async Task<T> FindAsync(long id)
        {
            return await AddBasicConventions().FirstOrDefaultAsync(c => c.Id == id);
        }


        /// <summary>
        /// Get all entities with BasicConventions
        /// </summary>
        /// <returns>all entities</returns>
        public virtual  List<T> GetList()
        {
            return AddBasicConventions().ToList();
        }

        /// <summary>
        /// Get all entities with BasicConventions
        /// </summary>
        /// <returns>all entities</returns>
        public virtual async Task<List<T>> GetListAsync()
        {
            return await AddBasicConventions().ToListAsync();
        }


        /// <summary>
        /// Get all entities with filter, sort and pagination and BasicConventions
        /// </summary>
        /// <param name = "orderBy" > orderby clause</param>
        /// <param name = "pageNumber" > number of current page</param>
        /// <param name = "pageSize" > count of records in every pages</param>
        /// <returns></returns>
        public virtual PagedList<T> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending")
        {
            // filter
            var query = AddBasicConventions();

            // sort
            query = query.OrderBy(orderBy.Replace("_", " "));

            // pagination
            return query.ToPagedList(pageNumber, pageSize);
        }

       
        /// <summary>
        /// Get all entities with filter, sort and pagination and BasicConventions
        /// </summary>
        /// <param name = "orderBy" > orderby clause</param>
        /// <param name = "pageNumber" > number of current page</param>
        /// <param name = "pageSize" > count of records in every pages</param>
        /// <returns></returns>
        public virtual async Task<PagedList<T>> GetAllAsync(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending")
        {
            // filter
            var query = AddBasicConventions();

            // sort
            query = query.OrderBy(orderBy.Replace("_", " "));

            // pagination
            return await query.ToPagedListAsync(pageNumber, pageSize);
        }


        /// <summary>
        /// Get all entities with filter, sort and pagination and BasicConventions
        /// </summary>
        /// <param name = "filter" > filter clause</param>
        /// <param name = "orderBy" > orderby clause</param>
        /// <param name = "pageNumber" > number of current page</param>
        /// <param name = "pageSize" > count of records in every pages</param>
        /// <returns></returns>
        public virtual PagedList<T> GetAll(object filter, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending")
        {
            if (filter == null) throw new ArgumentNullException("filter");

            // filter
            var query = AddBasicConventions();

            var quaryableFilter = QueryableFilterConverter.ConvertEntityToQueryableFilter<object>(filter);
            query = query.Where(quaryableFilter.Predicate, quaryableFilter.Parameters.ToArray());

            // sort
            query = query.OrderBy(orderBy.Replace("_", " "));

            // pagination
            return query.ToPagedList<T>(pageNumber, pageSize);
        }

        /// <summary>
        /// Get all entities with filter, sort and pagination and BasicConventions
        /// </summary>
        /// <param name = "filter" > filter clause</param>
        /// <param name = "orderBy" > orderby clause</param>
        /// <param name = "pageNumber" > number of current page</param>
        /// <param name = "pageSize" > count of records in every pages</param>
        /// <returns></returns>
        public virtual async Task<PagedList<T>> GetAllAsync(object filter, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending")
        {
            if (filter == null) throw new ArgumentNullException("filter");

            // filter
            var query = AddBasicConventions();

            var quaryableFilter = QueryableFilterConverter.ConvertEntityToQueryableFilter<object>(filter);
            query = query.Where(quaryableFilter.Predicate, quaryableFilter.Parameters.ToArray());

            // sort
            query = query.OrderBy(orderBy.Replace("_", " "));

            // pagination
            return await query.ToPagedListAsync<T>(pageNumber, pageSize);
        }


        /// <summary>
        /// Get all entities with filter, sort and pagination and BasicConventions
        /// </summary>
        /// <param name = "query" > query </param>
        /// <param name = "filter" > filter clause</param>
        /// <param name = "orderBy" > orderby clause</param>
        /// <param name = "pageNumber" > number of current page</param>
        /// <param name = "pageSize" > count of records in every pages</param>
        /// <returns></returns>
        public virtual PagedList<T> GetAll(IQueryable<T> query, object filter, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending")
        {
            // filter
            query = AddBasicConventions(query);

            if (filter != null)
            {
                var quaryableFilter = QueryableFilterConverter.ConvertEntityToQueryableFilter<object>(filter);
                query = query.Where(quaryableFilter.Predicate, quaryableFilter.Parameters.ToArray());
            }

            // sort
            query = query.OrderBy(orderBy.Replace("_", " "));

            // pagination
            return query.ToPagedList<T>(pageNumber, pageSize);
        }

        /// <summary>
        /// Get all entities with filter, sort and pagination and BasicConventions
        /// </summary>
        /// <param name = "query" > query </param>
        /// <param name = "filter" > filter clause</param>
        /// <param name = "orderBy" > orderby clause</param>
        /// <param name = "pageNumber" > number of current page</param>
        /// <param name = "pageSize" > count of records in every pages</param>
        /// <returns></returns>
        public virtual async Task<PagedList<T>> GetAllAsync(IQueryable<T> query, object filter, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending")
        {
            // filter
            query = AddBasicConventions(query);

            if (filter != null)
            {
                var quaryableFilter = QueryableFilterConverter.ConvertEntityToQueryableFilter<object>(filter);
                query = query.Where(quaryableFilter.Predicate, quaryableFilter.Parameters.ToArray());
            }

            // sort
            query = query.OrderBy(orderBy.Replace("_", " "));

            // pagination
            return await query.ToPagedListAsync<T>(pageNumber, pageSize);
        }


        /// <summary>
        /// Add basic conventions to an IQueryable
        /// </summary>
        /// <param name = "query" > query </param>
        /// <param name="predicate"></param>
        /// <returns>IQueryable with basic conventions</returns>
        public virtual IQueryable<T> AddBasicConventions(IQueryable<T> query = null, Expression<Func<T,bool>> predicate = null)
        {
            IQueryable<T> q = (query != null) ? 
                query.Where(DefaultPredicate) : 
                _dbSet.Where(DefaultPredicate);

            return predicate == null ? q : q.Where(predicate);
        }

    }
}
