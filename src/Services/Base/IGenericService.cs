﻿using DomainModels.Entities.Base;
using System.Linq.Expressions;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Infrastructure.Pagination;

namespace Services.Base
{
    public interface IGenericService<T> where T : BaseEntity
    {
        long Count();
        Task<long> CountAsync();

        long Count(Expression<Func<T, bool>> predicate);
        Task<long> CountAsync(Expression<Func<T, bool>> predicate);

        T Add(T model);

        T Edit(T model);
        Task<T> EditAsync(T model);

        void Delete(long id);
        Task DeleteAsync(long id);

        T Find(long id);
        Task<T> FindAsync(long id);

        List<T> GetList();
        Task<List<T>> GetListAsync();

        PagedList<T> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");
        Task<PagedList<T>> GetAllAsync(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");

        PagedList<T> GetAll(object filter, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");
        Task<PagedList<T>> GetAllAsync(object filter, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");

        PagedList<T> GetAll(IQueryable<T> query, object filter = null, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");
        Task<PagedList<T>> GetAllAsync(IQueryable<T> query, object filter = null, int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");

        IQueryable<T> AddBasicConventions(IQueryable<T> query = null, Expression<Func<T, bool>> predicate = null);
    }
}
