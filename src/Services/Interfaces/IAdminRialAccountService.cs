﻿using DomainModels.Entities.Financial;

namespace Services.Interfaces
{
    public interface IAdminRialAccountService
    {
        AdminRialAccount GetFirstRow();
    }
}
