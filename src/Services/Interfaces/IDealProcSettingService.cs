﻿using DomainModels.Entities.Setting;

namespace Services.Interfaces
{
    public interface IDealProcSettingService
    {
        void TryToTurnOffProcedure();
        DealProcSetting GetDefaultRow();
    }
}
