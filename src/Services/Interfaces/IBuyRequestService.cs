﻿using System;
using DomainModels.Entities.Shop;
using Services.Base;
using Infrastructure.Pagination;

namespace Services.Interfaces
{
    public interface IBuyRequestService
    {
        PagedList<BuyRequest> GetAll(int pageNumber = 1, int pageSize = 15);

        PagedList<BuyRequest> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15);
    }
}
