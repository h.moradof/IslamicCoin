﻿using DomainModels.Entities.Financial;
using Services.Base;


namespace Services.Interfaces
{
    public interface IBankService : IGenericService<Bank>
    {

    }
}
