﻿using DomainModels.Entities.Access;
using Services.Base;

namespace Services.Interfaces
{
    public interface IRoleService : IGenericService<Role>
    {
        #region  custom role provider

        string[] GetAllRoles();
        bool RoleExists(string roleName);
        string[] GetUsersInRole(string roleName);
        Role GetByRoleName(string name);
        #endregion custom role provider

    }
}
