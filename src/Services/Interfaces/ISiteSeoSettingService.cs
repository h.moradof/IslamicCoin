﻿using DomainModels.Entities.Site;
using Services.Base;

namespace Services.Interfaces
{
    public interface ISiteSeoSettingService : IGenericService<SiteSeoSetting>
    {

    }
}
