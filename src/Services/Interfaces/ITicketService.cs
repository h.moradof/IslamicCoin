﻿using Services.Base;
using DomainModels.Entities.Support;
using Infrastructure.Pagination;
using ViewModels.Support;

namespace Services.Interfaces
{
    public interface ITicketService : IGenericService<Ticket>
    {
        PagedList<Ticket> GetTickets(bool isOpen, int? pageNumber = 1, int? pageSize = 15);
        TicketDetailViewModel GetTicketDetailViewModel(long id);
        TicketDetailViewModel GetTicketDetailViewModel(long id, long userId);
        void Add(TicketCreateViewModel model, long senderId);
        PagedList<Ticket> GetAll(int value, long userId, int pageNumber = 1, int pageSize = 15);
    }
}
