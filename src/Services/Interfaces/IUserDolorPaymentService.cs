﻿using DomainModels.Entities.Financial;
using Infrastructure.Pagination;
using System;


namespace Services.Interfaces
{
    public interface IUserDolorPaymentService 
    {
        PagedList<UserDolorPayment> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");

        PagedList<UserDolorPayment> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15);
    }
}
