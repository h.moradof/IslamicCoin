﻿using DomainModels.Entities.Financial;
using Infrastructure.Pagination;
using Services.Base;


namespace Services.Interfaces
{
    public interface IDolorWithdrawalRequestService : IGenericService<DolorWithdrawalRequest>
    {
        PagedList<DolorWithdrawalRequest> Search(string text, int pageNumber = 1 , int pageSize = 15);
    }
}
