﻿using DomainModels.Entities.Site;
using Services.Base;
using ViewModels.Site;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface ISiteVisitService : IGenericService<SiteVisit>
    {
        void SaveNewVisit(string ip);

        IEnumerable<SiteVisitMonthlyViewModel> GetMonthlyVisits();

        SiteVisitReportViewModel GetSiteVisitReport();
    }
}
