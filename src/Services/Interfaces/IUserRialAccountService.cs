﻿using DomainModels.Entities.Financial;
using Infrastructure.Pagination;
using System.Collections.Generic;
using ViewModels.Financial;

namespace Services.Interfaces
{
    public interface IUserRialAccountService
    {
        AccountAcceptStatusName GetAcceptStatusName(long id);
        PagedList<UserRialAccount> GetAll(string acceptStatusName, int pageNumber = 1, int pageSize = 15);
        UserRialAccountStatusViewModel GetUserRialAccountStatus(long customerId);
        void Add(CustomerUserRialAccountCreateViewModel model, long customerId);
        List<UserRialAccount> GetListByCustomerId(long customerId);
        UserRialAccount GetFirstAcceptedUserRialAccount(long customerId);
        CustomerUserRialAccountEditViewModel GetCustomerUserRialAccountEditViewModel(long customerId);
        void Edit(CustomerUserRialAccountEditViewModel model, long customerId);
        void ChangeAcceptStatus(long id, AccountAcceptStatusName newStatus);
    }
}
