﻿using DomainModels.Entities.Access;
using Services.Base;

namespace Services.Interfaces
{
    public interface IValidIpService : IGenericService<ValidIp>
    {
        bool HasAccess(string ipAddress);
    }
}
