﻿using Services.Base;
using ViewModels.Account;
using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Access;
using Infrastructure.Pagination;
using ViewModels.Financial;
using DomainModels.Entities.Financial;

namespace Services.Interfaces
{
    public interface IUserService : IGenericService<User>
    {
        #region custom role provider
        bool IsUserInRole(string username, string roleName);
        string[] GetRolesForUser(string username);
        #endregion

        #region admin panel
        void Ban(long id, string banReason);
        void Active(long id);
        void Deactive(long id);
        void PermitUserIfPossible(long userId);
        #endregion

        #region panels
        bool HaveSameUsername(string username, long userId);
        bool HaveSameUsername(string username);
        ProfileViewModel GetProfile(long id);
        void EditProfile(ProfileViewModel model);
        #endregion

        #region sign in
        User SignIn(string username, string encyptedPassword);
        RoleName[] GetRoleNamesForUser(long id);
        #endregion

        #region forget password
        bool IsValidEmail(string email);
        ForgetPasswordSendEmail GetForgetPasswordDataByEmail(string email);
        #endregion

        #region set password
        bool IsValidActivateCode(string activateCode);
        long GetUserIdByActivateCode(string activateCode);
        void SetNewPassword(SetPasswordViewModel model);
        #endregion

        #region sign up
        void SignUp(SignUpViewModel model);
        #endregion

        #region site
        bool VerifyMobileNumber(string mobileNumber, string activateCode);
        PagedList<User> Search(string text, int pageNumber = 1, int pageSize = 15);
        #endregion

        #region customer panel
        Role GetRoleOfUser(long id);
        MoneyAccount GetUserMoneyAccount(long id);
        #endregion
    }
}