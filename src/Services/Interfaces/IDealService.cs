﻿using DomainModels.Entities.Shop;
using System;
using ViewModels.Shop;

namespace Services.Interfaces
{
    public interface IDealService
    {
        DealDetailViewModel GetDealByCode(Guid code);
    }
}
