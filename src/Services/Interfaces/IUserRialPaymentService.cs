﻿using DomainModels.Entities.Financial;
using Infrastructure.Pagination;
using System;
using ViewModels.Financial;

namespace Services.Interfaces
{
    public interface IUserRialPaymentService
    {
        PagedList<UserRialPayment> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");
        PagedList<UserRialPayment> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15);

        void Add(CustomerUserRialPaymentCreateViewModel model, long customerId);
        PagedList<UserRialPayment> GetAllByCustomerId(long customerId, int pageNumber = 1, int pageSize = 15);
    }
}
