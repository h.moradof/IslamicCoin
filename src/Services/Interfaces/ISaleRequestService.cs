﻿using DomainModels.Entities.Shop;
using Infrastructure.Pagination;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface ISaleRequestService
    {
        PagedList<SaleRequest> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");

        PagedList<SaleRequest> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15);

        List<SaleRequest> GetChilds(long parentId);
    }
}
