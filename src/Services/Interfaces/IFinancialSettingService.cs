﻿using DomainModels.Entities.Setting;

namespace Services.Interfaces
{
    public interface IFinancialSettingService
    {
        FinancialSetting Edit(FinancialSetting model);
        FinancialSetting GetDefaultRow();
    }
}
