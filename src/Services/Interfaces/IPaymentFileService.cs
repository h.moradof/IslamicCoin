﻿using System;
using DomainModels.Entities.Financial;
using Infrastructure.Pagination;

namespace Services.Interfaces
{
    public interface IPaymentFileService
    {
        PagedList<PaymentFile> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");
        PaymentFile FindByRowGuid(Guid code);
        PaymentFile Add(long[] rialWithdrawalRequestIds);
    }
}
