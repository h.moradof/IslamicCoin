﻿using DomainModels.Entities.Financial;
using Infrastructure.Pagination;
using System.Collections.Generic;
using ViewModels.Financial;
using System;

namespace Services.Interfaces
{
    public interface IRialWithdrawalRequestService
    {
        PagedList<RialWithdrawalRequestListViewModel> GetAllByStatusName(WithdrawalRequestStatusName statusName, int pageNumber = 1, int pageSize = 15);
        List<RialWithdrawalRequestListViewModel> GetListByStatusName(WithdrawalRequestStatusName statusName);
        void ChangeStatus(long[] requestIds, WithdrawalRequestStatusName statusName);
        PagedList<RialWithdrawalRequestListViewModel> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15);
    }
}
