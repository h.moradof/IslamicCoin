﻿using System;
using System.Web;
using DomainModels.Entities.Financial;
using Infrastructure.Pagination;

namespace Services.Interfaces
{
    public interface ITransactionExcelFileService
    {
        void Delete(long id);
        PagedList<TransactionExcelFile> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending");
        void Add(HttpFileCollectionBase files);
    }
}
