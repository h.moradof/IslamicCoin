﻿using Services.Base;
using DomainModels.Entities.Support;
using ViewModels.Support;

namespace Services.Interfaces
{
    public interface ITicketReplyService : IGenericService<TicketReply>
    {
        void Add(TicketReplyCreateViewModel model, long senderId);
    }
}
