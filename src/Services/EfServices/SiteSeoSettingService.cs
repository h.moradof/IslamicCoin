﻿using DatabaseContext.Context;
using DomainModels.Entities.Site;
using Infrastructure.Cache.Interfaces;
using Services.Base;
using Services.Interfaces;
using System.Threading.Tasks;

namespace Services.EfServices
{
    public class SiteSeoSettingService : GenericService<SiteSeoSetting>, ISiteSeoSettingService
    {
        #region props
        private readonly ISiteSeoSettingCacheManager _siteSeoSettingCacheManager;
        #endregion


        #region ctor

        public SiteSeoSettingService(IUnitOfWork uow, ISiteSeoSettingCacheManager siteSeoSettingCacheManager) 
            : base(uow)
        {
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
        }

        #endregion



        public override SiteSeoSetting Edit(SiteSeoSetting model)
        {
            var updatedItem = base.Edit(model);

            // refreshing cache
            _siteSeoSettingCacheManager.Cache(GetList());

            return updatedItem;
        }

        public override async Task<SiteSeoSetting> EditAsync(SiteSeoSetting model)
        {
            var updatedItem = await base.EditAsync(model);

            // refreshing cache
            _siteSeoSettingCacheManager.Cache(GetList());

            return updatedItem;
        }

    }
}
