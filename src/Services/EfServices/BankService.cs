﻿using DomainModels.Entities.Financial;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Collections.Generic;
using System.Linq;

namespace Services.EfServices
{
    public class BankService : GenericService<Bank>, IBankService
    {

        #region ctor
        public BankService(IUnitOfWork uow) 
            : base(uow)
        {
        }
        #endregion


        public override List<Bank> GetList()
        {
            return AddBasicConventions().OrderBy(c => c.Name).ToList();
        }



    }
}
