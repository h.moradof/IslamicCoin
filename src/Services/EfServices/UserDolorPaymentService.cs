﻿using DomainModels.Entities.Financial;
using Services.Base;
using Services.Interfaces;
using System;
using Infrastructure.Pagination;
using DatabaseContext.Context;
using System.Linq;

namespace Services.EfServices
{
    public class UserDolorPaymentService : GenericService<UserDolorPayment>, IUserDolorPaymentService
    {

        #region ctor
        public UserDolorPaymentService(IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion



        public PagedList<UserDolorPayment> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15)
        {
            var query = AddBasicConventions();

            if (code.HasValue)
            {
                query = query.Where(c => c.RowGuid == code.Value);
            }

            return query
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber, pageSize);
        }


    }
}
