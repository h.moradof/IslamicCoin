﻿using DatabaseContext.Context;
using DomainModels.Entities.Site;
using Services.Base;
using Services.Interfaces;
using ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Services.EfServices
{
    public class SiteVisitService : GenericService<SiteVisit>, ISiteVisitService
    {

        #region ctor

        public SiteVisitService(IUnitOfWork uow) : base(uow)
        { }

        #endregion

        /// <summary>
        /// ثبت آمار بازدید سایت
        /// </summary>
        public void SaveNewVisit(string ip)
        {
            var now = DateTime.Now.Date;

            var currentDateVisit = _dbSet.FirstOrDefault(c => DbFunctions.TruncateTime(c.VisitDate) == now && c.Ip == ip);

            if (currentDateVisit != null)
            {
                currentDateVisit.Number++;
            }
            else
            {
                _dbSet.Add(new SiteVisit
                {
                    Number = 1,
                    VisitDate = now,
                    Ip = ip
                });
            }

        }


        /// <summary>
        /// آمار بازدید ماهانه سال جاری سایت
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SiteVisitMonthlyViewModel> GetMonthlyVisits()
        {
            var now = DateTime.Now;
            var pCalendar = new PersianCalendar();
            var sqlParameters = new List<SqlParameter>();
            int endDateOfMonth = 31;
            string startDateParameterName = string.Empty, endDateParameterName = string.Empty;

            var query = new StringBuilder("sp_GetMonthlyVisits ");

            for (int shamsiMonthNumber = 1; shamsiMonthNumber <= 12; shamsiMonthNumber++)
            {
                endDateOfMonth = GetLastDayOfShamsiMonth(shamsiMonthNumber);

                startDateParameterName = string.Format("@Month_{0}_StartDate", shamsiMonthNumber);
                endDateParameterName = string.Format("@Month_{0}_EndDate", shamsiMonthNumber);

                sqlParameters.Add(new SqlParameter(startDateParameterName, pCalendar.ToDateTime(pCalendar.GetYear(now), shamsiMonthNumber, 1, 0, 0, 0, 0)));
                sqlParameters.Add(new SqlParameter(endDateParameterName, pCalendar.ToDateTime(pCalendar.GetYear(now), shamsiMonthNumber, endDateOfMonth, 0, 0, 0, 0)));

                query.Append(startDateParameterName);
                query.Append(",");
                query.Append(endDateParameterName);

                if (shamsiMonthNumber != 12)
                    query.Append(",");
            }

            var vmSiteVisitMonthly = _uow.Database.SqlQuery<SiteVisitMonthlyViewModel>(query.ToString(),
                sqlParameters[0], sqlParameters[1],
                sqlParameters[2], sqlParameters[3],
                sqlParameters[4], sqlParameters[5],
                sqlParameters[6], sqlParameters[7],
                sqlParameters[8], sqlParameters[9],
                sqlParameters[10], sqlParameters[11],
                sqlParameters[12], sqlParameters[13],
                sqlParameters[14], sqlParameters[15],
                sqlParameters[16], sqlParameters[17],
                sqlParameters[18], sqlParameters[19],
                sqlParameters[20], sqlParameters[21],
                sqlParameters[22], sqlParameters[23]
                ).ToList();

            return vmSiteVisitMonthly;
        }


        /// <summary>
        /// آمار کامل بازدید های سایت
        /// </summary>
        /// <returns></returns>
        public SiteVisitReportViewModel GetSiteVisitReport()
        {
            var vmSiteVisitReport = new SiteVisitReportViewModel();
            var now = DateTime.Now.Date;
            var yesterday = now.AddDays(-1);

            // today
            vmSiteVisitReport.TodayVisitCount = _dbSet.Any(c => c.VisitDate == now) ? _dbSet.First(c => c.VisitDate == now).Number : 0;

            // yesterday
            vmSiteVisitReport.YesterdayVisitCount = _dbSet.Any(c => c.VisitDate == yesterday) ? _dbSet.First(c => c.VisitDate == yesterday).Number : 0;

            // current month
            vmSiteVisitReport.CurrentMonthVisitCount = GetCurrentShamsiMonthVisitCount();

            // current year
            vmSiteVisitReport.CurrentYearVisitCount = GetCurrentShamsiYearVisitCount();

            // total visits
            vmSiteVisitReport.TotalVisitCount = _dbSet.Any() ? _dbSet.Sum(c => c.Number) : 0;

            return vmSiteVisitReport;
        }

        private int GetCurrentShamsiMonthVisitCount()
        {
            var now = DateTime.Now.Date;
            var pCalendar = new PersianCalendar();
            int currentShamsiMonth = pCalendar.GetMonth(now);
            int currentShamsiYear = pCalendar.GetYear(now);
            int lastDayOfCurrentShamsiMonth = GetLastDayOfShamsiMonth(currentShamsiMonth);

            var startDateOfCurrrentMonth = pCalendar.ToDateTime(currentShamsiYear, currentShamsiMonth, 1, 0, 0, 0, 0);
            var endDateOfCurrrentMonth = pCalendar.ToDateTime(currentShamsiYear, currentShamsiMonth, lastDayOfCurrentShamsiMonth, 0, 0, 0, 0);

            return _dbSet.Any(c => c.VisitDate >= startDateOfCurrrentMonth && c.VisitDate < endDateOfCurrrentMonth) ?
                _dbSet.Where(c => c.VisitDate >= startDateOfCurrrentMonth && c.VisitDate < endDateOfCurrrentMonth).Sum(c => c.Number) : 0;
        }

        private int GetCurrentShamsiYearVisitCount()
        {
            var now = DateTime.Now.Date;
            var pCalendar = new PersianCalendar();
            int currentShamsiYear = pCalendar.GetYear(now);

            var startDateOfCurrrentMonth = pCalendar.ToDateTime(currentShamsiYear, 1, 1, 0, 0, 0, 0); // from:  /1/1
            var endDateOfCurrrentMonth = pCalendar.ToDateTime(currentShamsiYear, 12, 29, 0, 0, 0, 0); // to:    /12/29

            return _dbSet.Any(c => c.VisitDate >= startDateOfCurrrentMonth && c.VisitDate < endDateOfCurrrentMonth) ?
                _dbSet.Where(c => c.VisitDate >= startDateOfCurrrentMonth && c.VisitDate < endDateOfCurrrentMonth).Sum(c => c.Number) : 0;
        }


        private static int GetLastDayOfShamsiMonth(int monthNumber)
        {
            if (monthNumber >= 7 && monthNumber <= 11)
                return 30;
            else if (monthNumber == 12)
                return 29;
            else
                return 31;
        }



    }
}
