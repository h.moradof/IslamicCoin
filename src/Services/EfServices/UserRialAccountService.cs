﻿using DomainModels.Entities.Financial;
using Services.Base;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using ViewModels.Financial;
using DatabaseContext.Context;
using System.Linq;
using Infrastructure.Exceptions;
using Infrastructure.Pagination;

namespace Services.EfServices
{
    public class UserRialAccountService : GenericService<UserRialAccount>, IUserRialAccountService
    {

        #region ctor
        public UserRialAccountService(IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion


        public void Add(CustomerUserRialAccountCreateViewModel model, long customerId)
        {
            var pendingAcceptStatus = GetAccountAcceptStatusByStatusName(AccountAcceptStatusName.Pending);

            Add(new UserRialAccount
            {
                AccountAcceptStatusId = pendingAcceptStatus.Id,
                BankId = model.BankId,
                AccountNumber = model.AccountNumber,
                CardNumber = model.CardNumber,
                ShabaNumber = model.ShabaNumber,
                UserId = customerId
            });
        }


        private AccountAcceptStatus GetAccountAcceptStatusByStatusName(AccountAcceptStatusName statusName)
        {
            return _uow.Set<AccountAcceptStatus>().First(c => c.Name == statusName);
        }


        public UserRialAccount GetFirstAcceptedUserRialAccount(long customerId)
        {
            return AddBasicConventions()
                .Join(_uow.Set<AccountAcceptStatus>(), a => a.AccountAcceptStatusId, st => st.Id, (a, st) => new { a, st })
                .Where(c => c.a.UserId == customerId && c.st.Name == AccountAcceptStatusName.Accepted)
                .Select(c => c.a)
                .FirstOrDefault();
        }

        public List<UserRialAccount> GetListByCustomerId(long customerId)
        {
            return AddBasicConventions().Where(c => c.UserId == customerId).ToList();
        }

        public UserRialAccountStatusViewModel GetUserRialAccountStatus(long customerId)
        {
            var a_s = AddBasicConventions()
                .Join(_uow.Set<AccountAcceptStatus>(), a => a.AccountAcceptStatusId, st => st.Id, (a, st) => new { a , st })
                .FirstOrDefault(c => c.a.UserId == customerId);

            if (a_s == null)
            {
                return UserRialAccountStatusViewModel.DontFindAnyAccount;
            }

            switch (a_s.st.Name)
            {
                case AccountAcceptStatusName.Pending:
                    return UserRialAccountStatusViewModel.AccountIsPending;
                case AccountAcceptStatusName.Accepted:
                    return UserRialAccountStatusViewModel.AccountIsAccepted;
                case AccountAcceptStatusName.Declined:
                    return UserRialAccountStatusViewModel.AccountIsDeclined;
                default:
                    throw new ArgumentOutOfRangeException("acceptStatus");
            }
        }

        public CustomerUserRialAccountEditViewModel GetCustomerUserRialAccountEditViewModel(long customerId)
        {
            return AddBasicConventions()
                .Where(c => c.UserId == customerId)
                .Select(c => new CustomerUserRialAccountEditViewModel
                {
                    Id = c.Id,
                    AccountNumber = c.AccountNumber,
                    BankId = c.BankId,
                    CardNumber = c.CardNumber,
                    ShabaNumber = c.ShabaNumber
                })
                .FirstOrDefault();
        }

        public void Edit(CustomerUserRialAccountEditViewModel model, long customerId)
        {
            var a_s = AddBasicConventions()
                .Join(_uow.Set<AccountAcceptStatus>(), a => a.AccountAcceptStatusId , s => s.Id, (a, s) => new { a, s })
                .FirstOrDefault(c => c.a.Id == model.Id && c.a.UserId == customerId);

            if (a_s == null)
                throw new CustomException("اطلاعات یافت نشد");

            if (a_s.s.Name != AccountAcceptStatusName.Declined)
                throw new CustomException("امکان ویرایش فقط برای حساب های رد شده میسر می باشد");

            var updatingItem = a_s.a;

            var pendingStatus = GetAccountAcceptStatusByStatusName(AccountAcceptStatusName.Pending);

            updatingItem.AccountAcceptStatusId = pendingStatus.Id;
            updatingItem.ShabaNumber = model.ShabaNumber;
            updatingItem.BankId = model.BankId;
            updatingItem.CardNumber = model.CardNumber;
        }

        public PagedList<UserRialAccount> GetAll(string acceptStatusName, int pageNumber = 1, int pageSize = 15)
        {
            var acceptStatus = (AccountAcceptStatusName)Enum.Parse(typeof(AccountAcceptStatusName), acceptStatusName, true);

            return AddBasicConventions()
                .Join(_uow.Set<AccountAcceptStatus>(), a => a.AccountAcceptStatusId, s => s.Id, (a, s) => new { a, s })
                .Where(c => c.s.Name == acceptStatus)
                .OrderByDescending(c => c.a.Id)
                .Select(c => c.a)
                .ToPagedList(pageNumber, pageSize);
        }

        public void ChangeAcceptStatus(long id, AccountAcceptStatusName newStatus)
        {
            var model = Find(id);

            if (model == null)
                return;

            var newAcceptStatus = GetAccountAcceptStatusByStatusName(newStatus);
            model.AccountAcceptStatusId = newAcceptStatus.Id;
        }

        public AccountAcceptStatusName GetAcceptStatusName(long id)
        {
            return AddBasicConventions()
                .Join(_uow.Set<AccountAcceptStatus>(), a => a.AccountAcceptStatusId, s => s.Id, (a, s) => new { a, s })
                .Where(c => c.a.Id == id)
                .Select(c => c.s.Name)
                .FirstOrDefault();
        }
    }
}
