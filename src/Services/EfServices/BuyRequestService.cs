﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Shop;
using Infrastructure.Pagination;
using Services.Interfaces;
using System.Linq;

namespace Services.EfServices
{
    public class BuyRequestService : IBuyRequestService
    {

        #region props
        private readonly IUnitOfWork _uow;
        #endregion


        #region ctor
        public BuyRequestService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        #endregion


        public PagedList<BuyRequest> GetAll(int pageNumber = 1, int pageSize = 15)
        {
            return _uow.Set<BuyRequest>()
                .Where(c => c.DeletedOn == null)
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber, pageSize);
        }


        public PagedList<BuyRequest> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15)
        {
            if (code.HasValue)
            {
                return _uow.Set<BuyRequest>()
                    .Where(c => c.DeletedOn == null && c.RowGuid == code.Value)
                    .OrderByDescending(c => c.Id)
                    .ToPagedList(pageNumber, pageSize);
            }

            return _uow.Set<BuyRequest>()
                .Where(c => c.DeletedOn == null)
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber, pageSize);
        }


    }
}
