﻿using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using DomainModels.Entities.Support;
using ViewModels.Support;
using System;
using DomainModels.Entities.HumanResource;
using System.Linq;
using DomainModels.Entities.Access;

namespace Services.EfServices
{
    public class TicketReplyService : GenericService<TicketReply>, ITicketReplyService
    {

        #region ctor
        public TicketReplyService(
            IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion


        public void Add(TicketReplyCreateViewModel model, long senderId)
        {
            ValidateTicketId(model.TicketId , senderId);

            var resultModel = new TicketReply
            {
                Message = model.Message,
                SenderId = senderId,
                TicketId = model.TicketId
            };

            _uow.Set<TicketReply>().Add(resultModel);

            var ticket = _uow.Set<Ticket>().Find(model.TicketId);
            ticket.IsOpen = model.IsOpen;
        }

        private void ValidateTicketId(long ticketId, long senderId)
        {
            // فعلا تیکت رو فقط کاربرا می تونن برای ادمین بفرستن
            var hasAccessToTicket =
                _uow.Set<Ticket>()
                .Join(_uow.Set<User>(), t => t.SenderId, u => u.Id, (t, u) => new { t, u })
                .Join(_uow.Set<Role>(), t_u => t_u.u.RoleId, r => r.Id, (t_u, r) => new { t_u.t, t_u.u, r })
                .Any(c => c.r.RoleName == RoleName.Administrators || c.r.RoleName == RoleName.Employees || c.t.SenderId == senderId);

            if (!hasAccessToTicket)
                throw new UnauthorizedAccessException("دسترسی به تیکت غیرمجاز است");
        }
    }
}
