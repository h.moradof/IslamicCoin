﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Financial;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using Infrastructure.Payment.File.PasargadBank;
using Infrastructure.Payment.File.PasargadBank.Model;
using System.Collections.Generic;
using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;

namespace Services.EfServices
{
    public class PaymentFileService : GenericService<PaymentFile>, IPaymentFileService
    {

        #region ctor
        public PaymentFileService(IUnitOfWork uow) 
            : base(uow)
        {  }

        #endregion



        public PaymentFile FindByRowGuid(Guid code)
        {
            return _dbSet.FirstOrDefault(c => c.RowGuid == code);
        }



        /// <summary>
        /// Create payment file, change status of requests to OnProcessing 
        /// </summary>
        /// <param name="warehouseIds"></param>
        public PaymentFile Add(long[] rialWithdrawalRequestIds)
        {
            var data = new List<PasargadBankPaymentFileData>();

            var newPaymentFile = new PaymentFile();

            var requests = 
                (from w in _uow.Set<RialWithdrawalRequest>()
                 join s in _uow.Set<WithdrawalRequestStatus>() on w.WithdrawalRequestStatusId equals s.Id
                 join ura in _uow.Set<UserRialAccount>() on w.DestinationRialAccountId equals ura.Id
                 join aas in _uow.Set<AccountAcceptStatus>() on ura.AccountAcceptStatusId equals aas.Id
                 join u in _uow.Set<User>() on ura.UserId equals u.Id
                 where 
                        s.Name == WithdrawalRequestStatusName.Pending &&
                        ura.DeletedOn == null &&
                        aas.Name == AccountAcceptStatusName.Accepted &&
                        u.AccountStatus == AccountStatus.Active &&
                        rialWithdrawalRequestIds.Contains(w.Id)
                 select new
                 {
                     w, ura, u
                 }).ToList();
                

            var onprocessingStatus = _uow.Set<WithdrawalRequestStatus>().First(c => c.Name == WithdrawalRequestStatusName.OnProcessing);

            foreach (var request in requests)
            {

                // complete model to create body of payment file
                data.Add(new PasargadBankPaymentFileData
                {
                    ShabaNumber = request.ura.ShabaNumber,
                    Price = request.w.Price,
                    CustomerFirstName = "مشتری",
                    CustomerLastName = request.u.FullName,
                    Description = DateTime.Now.ToString("yyyy-MM-dd_hh-mm"),
                    TransactionId = request.w.Id
                });

                // change status
                request.w.WithdrawalRequestStatusId = onprocessingStatus.Id;
            }

            // create file body and save it
            newPaymentFile.FileContent = PaymentFileCreator.GetFileBody(data);
            _uow.Set<PaymentFile>().Add(newPaymentFile);

            return newPaymentFile;
        }

    }
}
