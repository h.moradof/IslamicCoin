﻿using DomainModels.Entities.Access;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;
using Infrastructure.Exceptions;
using System.Threading.Tasks;
using Infrastructure.Pagination;
using DomainModels.Entities.HumanResource;

namespace Services.EfServices
{
    public class RoleService : GenericService<Role>, IRoleService
    {

        #region ctor

        public RoleService(
            IUnitOfWork uow)
            : base(uow)
        {  }

        #endregion


        #region custom role provider

        public string[] GetAllRoles()
        {
            return AddBasicConventions().ToList().Select(c => c.Name).ToArray();
        }

        public string[] GetUsersInRole(string roleName)
        {
            var role = GetByRoleName(roleName);

            return _uow.Set<User>().Where(c => c.RoleId == role.Id).Select(c => c.Username).ToArray();
        }

        public bool RoleExists(string roleName)
        {
            return AddBasicConventions().Any(c => c.Name == roleName);
        }
        public Role GetByRoleName(string name)
        {
            return AddBasicConventions().First(c => c.Name == name);
        }
        #endregion


        #region override
        public override void Delete(long id)
        {
            throw new CustomException("گروه های کاربری غیر قابل حذف می باشند");
        }

        public override Task DeleteAsync(long id)
        {
            throw new CustomException("گروه های کاربری غیر قابل حذف می باشند");
        }
        #endregion


    }
}
