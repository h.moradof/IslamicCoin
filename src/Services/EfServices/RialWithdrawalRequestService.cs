﻿using Services.Interfaces;
using DomainModels.Entities.Financial;
using Infrastructure.Pagination;
using Services.Base;
using DatabaseContext.Context;
using System.Linq;
using System.Collections.Generic;
using ViewModels.Financial;
using DomainModels.Entities.HumanResource;
using System;

namespace Services.EfServices
{
    public class RialWithdrawalRequestService : GenericService<RialWithdrawalRequest>, IRialWithdrawalRequestService
    {

        #region ctor
        public RialWithdrawalRequestService(IUnitOfWork uow) 
            : base(uow)
        {     }

        #endregion


        public void ChangeStatus(long[] requestIds, WithdrawalRequestStatusName statusName)
        {
            var status = _uow.Set<WithdrawalRequestStatus>().First(c => c.Name == statusName);

            foreach (var id in requestIds)
            {
                var request = Find(id);

                if(request != null)
                {
                    request.WithdrawalRequestStatusId = status.Id;
                    request.UpdatedOn = DateTime.Now;

                    if (statusName == WithdrawalRequestStatusName.Paid)
                    {
                        request.PayDate = DateTime.Now;
                    }
                }
            }
        }

        public PagedList<RialWithdrawalRequestListViewModel> GetAllByStatusName(WithdrawalRequestStatusName statusName, int pageNumber = 1, int pageSize = 15)
        {
            return AddBasicConventions()
                .Join(_uow.Set<WithdrawalRequestStatus>(), w => w.WithdrawalRequestStatusId, s => s.Id, (w, s) => new { w, s })
                .Join(_uow.Set<UserRialAccount>(), w_s => w_s.w.DestinationRialAccountId, ura => ura.Id, (w_s, ura) => new { w_s.w, w_s.s, ura })
                .Join(_uow.Set<User>(), w_s_ura => w_s_ura.ura.UserId, u => u.Id, (w_s_ura, u) => new { w_s_ura.w, w_s_ura.s, w_s_ura.ura, u })
                .Join(_uow.Set<Bank>(), w_s_ura_u => w_s_ura_u.ura.BankId, b => b.Id, (w_s_ura_u, b) => new { w_s_ura_u.w, w_s_ura_u.s, w_s_ura_u.ura, w_s_ura_u.u, b })
                .Join(_uow.Set<AccountAcceptStatus>(), w_s_ura_u_b => w_s_ura_u_b.ura.AccountAcceptStatusId, aas => aas.Id, (w_s_ura_u_b, aas) => new { w_s_ura_u_b.w, w_s_ura_u_b.s, w_s_ura_u_b.ura, w_s_ura_u_b.u, w_s_ura_u_b.b, aas })
                .Where(c => c.s.Name == statusName
                    && c.ura.DeletedOn == null
                    && c.aas.Name == AccountAcceptStatusName.Accepted
                    && c.u.AccountStatus == DomainModels.Entities.Base.AccountStatus.Active)
                .OrderBy(c => c.w.Id)
                .Select(c => new RialWithdrawalRequestListViewModel
                {
                    Id = c.w.Id,
                    UserId = c.u.Id,
                    UserFullName = c.u.FullName,
                    AccountNumber = c.ura.AccountNumber,
                    CardNumber = c.ura.CardNumber,
                    ShabaNumber = c.ura.ShabaNumber,
                    BankName = c.b.Name,
                    Price = c.w.Price,
                    PayDate = c.w.PayDate,
                    RowGuid = c.w.RowGuid,
                    CreatedOn = c.w.CreatedOn
                })
                .ToPagedList(pageNumber, pageSize);
        }

        public List<RialWithdrawalRequestListViewModel> GetListByStatusName(WithdrawalRequestStatusName statusName)
        {
            return AddBasicConventions()
                .Join(_uow.Set<WithdrawalRequestStatus>(), w => w.WithdrawalRequestStatusId, s => s.Id, (w, s) => new { w, s })
                .Join(_uow.Set<UserRialAccount>(), w_s => w_s.w.DestinationRialAccountId, ura => ura.Id, (w_s, ura) => new { w_s.w, w_s.s, ura })
                .Join(_uow.Set<User>(), w_s_ura => w_s_ura.ura.UserId, u => u.Id, (w_s_ura, u) => new { w_s_ura.w, w_s_ura.s, w_s_ura.ura, u })
                .Join(_uow.Set<Bank>(), w_s_ura_u => w_s_ura_u.ura.BankId, b => b.Id, (w_s_ura_u, b) => new { w_s_ura_u.w, w_s_ura_u.s, w_s_ura_u.ura, w_s_ura_u.u, b })
                .Join(_uow.Set<AccountAcceptStatus>(), w_s_ura_u_b => w_s_ura_u_b.ura.AccountAcceptStatusId, aas => aas.Id, (w_s_ura_u_b, aas) => new { w_s_ura_u_b.w, w_s_ura_u_b.s, w_s_ura_u_b.ura, w_s_ura_u_b.u, w_s_ura_u_b.b, aas })
                .Where(c => c.s.Name == statusName 
                    && c.ura.DeletedOn == null 
                    && c.aas.Name == AccountAcceptStatusName.Accepted
                    && c.u.AccountStatus == DomainModels.Entities.Base.AccountStatus.Active)
                .OrderBy(c => c.w.Id)
                .Select(c => new RialWithdrawalRequestListViewModel
                {
                    Id = c.w.Id,
                    UserId = c.u.Id,
                    UserFullName = c.u.FullName,
                    AccountNumber = c.ura.AccountNumber,
                    CardNumber = c.ura.CardNumber,
                    ShabaNumber = c.ura.ShabaNumber,
                    BankName = c.b.Name,
                    Price = c.w.Price,
                    PayDate = c.w.PayDate,
                    RowGuid = c.w.RowGuid,
                    CreatedOn = c.w.CreatedOn
                })
                .ToList();
        }

        public PagedList<RialWithdrawalRequestListViewModel> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15)
        {
            var query = AddBasicConventions()
                .Join(_uow.Set<WithdrawalRequestStatus>(), w => w.WithdrawalRequestStatusId, s => s.Id, (w, s) => new { w, s })
                .Join(_uow.Set<UserRialAccount>(), w_s => w_s.w.DestinationRialAccountId, ura => ura.Id, (w_s, ura) => new { w_s.w, w_s.s, ura })
                .Join(_uow.Set<User>(), w_s_ura => w_s_ura.ura.UserId, u => u.Id, (w_s_ura, u) => new { w_s_ura.w, w_s_ura.s, w_s_ura.ura, u })
                .Join(_uow.Set<Bank>(), w_s_ura_u => w_s_ura_u.ura.BankId, b => b.Id, (w_s_ura_u, b) => new { w_s_ura_u.w, w_s_ura_u.s, w_s_ura_u.ura, w_s_ura_u.u, b })
                .Where(c => c.ura.DeletedOn == null)
                .OrderBy(c => c.w.Id)
                .Select(c => new RialWithdrawalRequestListViewModel
                {
                    Id = c.w.Id,
                    UserId = c.u.Id,
                    UserFullName = c.u.FullName,
                    AccountNumber = c.ura.AccountNumber,
                    CardNumber = c.ura.CardNumber,
                    ShabaNumber = c.ura.ShabaNumber,
                    BankName = c.b.Name,
                    Price = c.w.Price,
                    PayDate = c.w.PayDate,
                    RowGuid = c.w.RowGuid,
                    CreatedOn = c.w.CreatedOn,
                    Status = c.s
                });

            if (code.HasValue)
            {
                query = query.Where(c => c.RowGuid == code.Value);
            }

            return query.ToPagedList(pageNumber, pageSize);
        }
    }
}
