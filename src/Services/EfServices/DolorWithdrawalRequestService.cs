﻿using DomainModels.Entities.Financial;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System;
using System.Linq;
using DomainModels.Entities.HumanResource;
using Infrastructure.Pagination;

namespace Services.EfServices
{
    public class DolorWithdrawalRequestService : GenericService<DolorWithdrawalRequest>, IDolorWithdrawalRequestService
    {
        
        #region ctor
        public DolorWithdrawalRequestService(IUnitOfWork uow) 
            : base(uow)
        { }
        #endregion

        public PagedList<DolorWithdrawalRequest> Search(string text, int pageNumber = 1, int pageSize = 15)
        {
            var query = AddBasicConventions()
                .Join(_uow.Set<User>(), dwr => dwr.UserId, u => u.Id, (dwr, u) => new { dwr, u });

            if (!string.IsNullOrEmpty(text))
            {
                query = query.Where(c => c.u.Username == text);
            }

            return query.OrderByDescending(c => c.dwr.Id).Select(c => c.dwr).ToPagedList(pageNumber, pageSize);
        }


    }
}
