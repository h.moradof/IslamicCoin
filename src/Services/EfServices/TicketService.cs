﻿using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;
using Infrastructure.Pagination;
using DomainModels.Entities.Support;
using ViewModels.Support;
using System;

namespace Services.EfServices
{
    public class TicketService : GenericService<Ticket>, ITicketService
    {

        #region ctor
        public TicketService(
            IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion


        public PagedList<Ticket> GetTickets(bool isOpen, int? pageNumber = 1, int? pageSize = 15)
        {
            return AddBasicConventions()
                .Where(c => c.IsOpen == isOpen)
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber.Value, pageSize.Value);
        }

        public TicketDetailViewModel GetTicketDetailViewModel(long id)
        {
            var vm = new TicketDetailViewModel();
            vm.Ticket = Find(id);
            vm.TicketReplies = _uow.Set<TicketReply>().Where(c => c.TicketId == id).ToList();
            return vm;
        }

        public TicketDetailViewModel GetTicketDetailViewModel(long id, long userId)
        {
            var vm = new TicketDetailViewModel();
            vm.Ticket = AddBasicConventions().FirstOrDefault(c => c.Id == id && c.SenderId == userId);

            if (vm.Ticket == null)
                throw new UnauthorizedAccessException("دسترسی غیر مجاز است");

            vm.TicketReplies = _uow.Set<TicketReply>().Where(c => c.TicketId == vm.Ticket.Id).ToList();
            return vm;
        }

        public void Add(TicketCreateViewModel model, long senderId)
        {
            base.Add(new Ticket
            {
                Subject = model.Subject,
                Message = model.Message,
                SenderId = senderId
            });
        }

        public PagedList<Ticket> GetAll(int value, long userId, int pageNumber = 1, int pageSize = 15)
        {
            return AddBasicConventions()
                .Where(c => c.SenderId == userId)
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber, pageSize);
        }
    }
}
