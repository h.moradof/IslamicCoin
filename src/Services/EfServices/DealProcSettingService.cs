﻿using DomainModels.Entities.Setting;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;
using System;
using Infrastructure.Exceptions;

namespace Services.EfServices
{
    public class DealProcSettingService : GenericService<DealProcSetting>, IDealProcSettingService
    {

        #region ctor
        public DealProcSettingService(IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion


        public DealProcSetting GetDefaultRow()
        {
            return AddBasicConventions().FirstOrDefault();
        }

        public void TryToTurnOffProcedure()
        {
            var updatingItem = GetDefaultRow();

            if (updatingItem.IsLock)
            {
                throw new CustomException("در حال حاضر سیستم در حال انجام دستور قبلی شما می باشد. لذا تا پایان عملیات، امکان صدور دستور جدید میسر نمی باشد");
            }

            updatingItem.TryToChangeState = true;
            updatingItem.IsLock = true;
            updatingItem.UpdatedOn = DateTime.Now;
        }

    }
}
