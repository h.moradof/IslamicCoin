﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Shop;
using Infrastructure.Pagination;
using Services.Interfaces;
using System.Linq;
using Services.Base;
using System.Collections.Generic;

namespace Services.EfServices
{
    public class SaleRequestService : GenericService<SaleRequest>, ISaleRequestService
    {

        #region ctor
        public SaleRequestService(IUnitOfWork uow) 
            : base(uow)
        {      }
        #endregion


        public override PagedList<SaleRequest> GetAll(int pageNumber = 1, int pageSize = 15, string orderBy = "Id_descending")
        {
            return AddBasicConventions()
                .Where(c => c.ParentId == null)
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber, pageSize);
        }

        public List<SaleRequest> GetChilds(long parentId)
        {
            return AddBasicConventions()
                .Where(c => c.ParentId == parentId)
                .OrderBy(c => c.Id)
                .ToList();
        }

        public PagedList<SaleRequest> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15)
        {
            var query = AddBasicConventions();

            if (code.HasValue)
            {
                query = query.Where(c => c.RowGuid == code.Value);
            }

            return query
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber, pageSize);
        }


    }
}
