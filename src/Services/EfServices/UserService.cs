﻿using DatabaseContext.Context;
using Infrastructure.Exceptions;
using Infrastructure.IQueryableExtensions;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using ViewModels.Account;
using Infrastructure.Security;
using DomainModels.Entities.Access;
using DomainModels.Entities.Base;
using System;
using DomainModels.Entities.HumanResource;
using Infrastructure.Sms.Interfaces;
using Infrastructure.Mail.Interfaces;
using Infrastructure.Pagination;
using DomainModels.Entities.Setting;
using Infrastructure.Common;
using ViewModels.Financial;
using DomainModels.Entities.Financial;

namespace Services.EfServices
{
    public class UserService : GenericService<User>, IUserService
    {

        #region props

        private readonly ISmsSender _smsSender;
        private readonly IUserAccountMailSender _userAcountMailSender;

        #endregion

        #region ctor

        public UserService(
            IUnitOfWork uow,
            ISmsSender smsSender,
            IUserAccountMailSender userAcountMailSender)
            : base(uow)
        {
            _smsSender = smsSender;
            _userAcountMailSender = userAcountMailSender;
        }

        #endregion


        #region override

        public override void Delete(long id)
        {
            throw new CustomException("عملیات غیر مجاز است");
        }

        public override Task DeleteAsync(long id)
        {
            throw new CustomException("عملیات غیر مجاز است");
        }

        public override User Add(User model)
        {
            var haveSameUser = _dbSet.Any(c => c.Username == model.Username);

            if (haveSameUser)
                throw new CustomException("نام کاربری انتخابی شما قبلا در سامانه ثبت شده است");

            return base.Add(model);
        }

        public override Task<User> EditAsync(User model)
        {
            var haveSameUser = _dbSet.Any(c => c.Id != model.Id && c.Username == model.Username);

            if (haveSameUser)
                throw new CustomException("نام کاربری انتخابی شما قبلا در سامانه ثبت شده است");

            return base.EditAsync(model);

            // TODO: send email to user

        }

        #endregion


        #region custom role provider

        public bool IsUserInRole(string username, string roleName)
        {
            RoleName roleNameEnum;
            Enum.TryParse(roleName, out roleNameEnum);

            return 
                AddBasicConventions()
                .Join(_uow.Set<Role>(), u => u.RoleId, r => r.Id , (u,r) => new { u, r })
                .Any(c => c.u.Username == username && c.r.RoleName == roleNameEnum);
        }

        public string[] GetRolesForUser(string username)
        {
            return 
                AddBasicConventions()
                .Join(_uow.Set<Role>(), u => u.RoleId, r => r.Id, (u, r) => new { u, r })
                .Where(c => c.u.Username == username).Select(c => c.r.Name).ToList().ToArray();
        }

        #endregion


        #region admin panel
        public void Ban(long id, string banReason)
        {
            var user = Find(id);
            user.AccountStatus = AccountStatus.Baned;
            user.BanReason = banReason;
        }

        public void Active(long id)
        {
            var user = Find(id);
            user.AccountStatus = AccountStatus.Active;
        }

        public void Deactive(long id)
        {
            var user = Find(id);
            user.AccountStatus = AccountStatus.Deactive;
        }

        public PagedList<User> Search(string text, int pageNumber = 1, int pageSize = 15)
        {
            var query = AddBasicConventions();

            if (!string.IsNullOrEmpty(text))
            {
                query = query.Where(c => c.Username == text);
            }

            return query.OrderByDescending(c => c.Id).ToPagedList(pageNumber, pageSize);
        }

        public void PermitUserIfPossible(long userId)
        {
            var user = Find(userId);

            if (user.IdentificationCardAcceptStatus == AcceptStatus.Accepted && 
                user.FaceAndNationalCardAcceptStatus == AcceptStatus.Accepted &&
                user.MobileNumberAcceptStatus == AcceptStatus.Accepted && 
                user.PhoneNumberAcceptStatus == AcceptStatus.Accepted)
            {
                // change role of user to permited users
                var permittedUsers = _uow.Set<Role>().First(c => c.RoleName == RoleName.PermitedUsers);

                user.RoleId = permittedUsers.Id;
            }
        }

        #endregion


        #region panels

        public bool HaveSameUsername(string username, long userId)
        {
            return _dbSet.Any(c => c.Username == username && c.Id != userId);
        }

        public bool HaveSameUsername(string username)
        {
            return _dbSet.Any(c => c.Username == username);
        }

        public ProfileViewModel GetProfile(long id)
        {
            var vm = AddBasicConventions()
                .Select(c => new ProfileViewModel
                {
                    Email = c.EmailAddress,
                    FullName = c.FullName,
                    Username = c.Username,
                    UserId = c.Id,
                    Password = "",
                    RePassword = ""
                })
                .FirstOrDefault(c => c.UserId == id);

            return vm;
        }

        public void EditProfile(ProfileViewModel model)
        {
            var user = Find(model.UserId);

            string userOldEmailAddress = user.EmailAddress;
            user.FullName = model.FullName;
            user.EmailAddress = model.Email;

            if (model.WantUpdateUsername)
                user.Username = model.Username;

            if (model.WantUpdatePassword)
                user.Password = DataEncryptor.MultiEncrypt(model.Password);

            _userAcountMailSender.SendEditUserEmail(userOldEmailAddress);
        }

        #endregion


        #region account

        #region signup

        public void SignUp(SignUpViewModel model)
        {
            if (HaveSameUsername(model.Username))
            {
                throw new CustomException("نام کاربری انتخابی شما در سامانه وجود دارد. در صورتی که قبلا ثبت نام نموده اید و کلمه عبور خود را فراموش کرده اید، روی دکمه 'قبلا ثبت نام کرده ام' کلیک نمایید");
            }

            var registeredRole = _uow.Set<Role>().First(c => c.RoleName == RoleName.RegisteredUsers);
            var setting = _uow.Set<FinancialSetting>().First();
            var rnd = new RandomMaker();

            base.Add(new User
            {
                FullName = model.FullName,
                PhoneNumber = model.PhoneNumber,
                RoleId = registeredRole.Id,
                Username = model.Username,
                Password = DataEncryptor.MultiEncrypt(model.Password),
                ActivateCode = rnd.CreateRandomNumbers(5),
                EmailAddress = model.EmailAddress,
                MaximumDolorWithdrawalAmount = setting.MaximumDolorWithdrawalAmount,
                MaximumRialWithdrawalPrice = setting.MaximumRialWithdrawalPrice
            });
        }

        #endregion

        #region signin

        public User SignIn(string username, string password)
        {
            var hashedPassword = DataEncryptor.MultiEncrypt(password);

            var user = AddBasicConventions()
                .FirstOrDefault(c => c.Username == username && c.Password == hashedPassword);

            ValidateUserOnSignIn(user);

            return user;
        }

        private void ValidateUserOnSignIn(User user)
        {
            if (user == null)
                throw new CustomException("نام کاربری یا کلمه عبور نادرست است");

            if (user.AccountStatus == DomainModels.Entities.Base.AccountStatus.Baned)
                throw new CustomException("حساب کاربری شما مسدود شده است. دلیل:" + user.BanReason);

            if (user.AccountStatus == DomainModels.Entities.Base.AccountStatus.Deactive)
                throw new CustomException("حساب کاربری شما غیرفعال است");
        }

        public RoleName[] GetRoleNamesForUser(long id)
        {
            return
                AddBasicConventions()
                .Join(_uow.Set<Role>(), u => u.RoleId, r => r.Id, (u, r) => new { u, r })
                .Where(c => c.u.Id == id)
                .Select(c => c.r.RoleName)
                .ToList()
                .ToArray();
        }

        #endregion


        #region forget password 

        public bool IsValidEmail(string email)
        {
            return AddBasicConventions().Any(c => c.EmailAddress == email);
        }

        public ForgetPasswordSendEmail GetForgetPasswordDataByEmail(string email)
        {
            var user = AddBasicConventions().FirstOrDefault(c => c.EmailAddress == email);

            if (user == null)
                throw new CustomException("کاربری با این آدرس ایمیل یافت نشد");

            return new ForgetPasswordSendEmail { FullName = user.FullName, Code = user.ActivateCode, Username = user.Username };

        }

        #endregion


        #region set password
        public bool IsValidActivateCode(string activateCode)
        {
            return AddBasicConventions().Any(c => c.ActivateCode == activateCode);
        }

        public long GetUserIdByActivateCode(string activateCode)
        {
            return AddBasicConventions().First(c => c.ActivateCode == activateCode).Id;
        }

        public void SetNewPassword(SetPasswordViewModel model)
        {
            var user = Find(model.UserId);
            user.Password = model.Password;

            // send email to user
            _userAcountMailSender.SendEditUserEmail(user.EmailAddress);
        }

        #endregion

        #endregion


        #region site
        public bool VerifyMobileNumber(string mobileNumber, string activateCode)
        {
            var user = AddBasicConventions().FirstOrDefault(c => c.Username == mobileNumber && c.ActivateCode == activateCode);

            if (user == null)
                return false;

            user.MobileNumberAcceptStatus = AcceptStatus.Accepted;
            return true;
        }
        #endregion

        
        #region customer panel

        public Role GetRoleOfUser(long id)
        {
            return _dbSet
                .Join(_uow.Set<Role>(), u => u.RoleId, r => r.Id, (u, r) => new { u, r })
                .Where(c => c.u.Id == id)
                .Select(c => c.r)
                .First();
        }

        public MoneyAccount GetUserMoneyAccount(long customerId)
        {
            return _uow.Set<MoneyAccount>().FirstOrDefault(c => c.Id == customerId);
        }

        #endregion

    }
}
