﻿using DomainModels.Entities.Setting;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;

namespace Services.EfServices
{
    public class FinancialSettingService : GenericService<FinancialSetting>, IFinancialSettingService
    {

        #region ctor
        public FinancialSettingService(IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion


        public FinancialSetting GetDefaultRow()
        {
            return AddBasicConventions().FirstOrDefault();
        }

    }
}
