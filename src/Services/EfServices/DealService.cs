﻿using Services.Interfaces;
using System;
using DomainModels.Entities.Shop;
using DatabaseContext.Context;
using System.Linq;
using ViewModels.Shop;
using System.Collections.Generic;

namespace Services.EfServices
{
    public class DealService : IDealService
    {

        #region props
        private readonly IUnitOfWork _uow;
        #endregion

        #region ctor
        public DealService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        #endregion



        public DealDetailViewModel GetDealByCode(Guid code)
        {
            var query = _uow.Set<Deal>()
                .Join(_uow.Set<SaleRequest>(), d => d.SaleRequestId, sr => sr.Id, (d, sr) => new { d, sr })
                .Join(_uow.Set<BuyRequest>(), d_sr => d_sr.d.BuyRequestId, br => br.Id, (d_sr, br) => new { d_sr.d, d_sr.sr, br });

            var vm = new DealDetailViewModel();
            
            vm.BuyRequest = query
                .Where(c => c.br.RowGuid == code || c.sr.RowGuid == code)
                .Select(c => c.br)
                .FirstOrDefault();

            if (vm.BuyRequest == null)
            {
                return vm;
            }

            vm.SaleRequestDeals = query
                .Where(c => c.br.Id == vm.BuyRequest.Id)
                .Select(c => new SaleRequestDealViewModel { SaleRequest = c.sr, Deal = c.d })
                .ToList();

            return vm;
        }

    }
}
