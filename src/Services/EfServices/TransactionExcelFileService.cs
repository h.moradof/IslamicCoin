﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Financial;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using System.Web;
using Infrastructure.Payment.File.PasargadBank;

namespace Services.EfServices
{
    public class TransactionExcelFileService : GenericService<TransactionExcelFile>, ITransactionExcelFileService
    {

        #region ctor
        public TransactionExcelFileService(IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion


        public void Add(HttpFileCollectionBase files)
        {
            var list = BillExcelFileDataExporter.ExportFirstExcelFile(files, new string[] { ".xlsx", ".xls" });

            foreach (var item in list)
            {
                base.Add(item);
            }
        }

    }
}
