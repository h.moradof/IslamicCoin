﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Access;
using Services.Base;
using Services.Interfaces;
using Infrastructure.Cache.Interfaces;
using Infrastructure.API.IP;
using System.Linq;

namespace Services.EfServices
{
    public class ValidIpService : GenericService<ValidIp>, IValidIpService
    {

        #region props
        private readonly IApplicationSettingCacheManager _applicationSettingCacheManager;
        private readonly IIpChecker _ipChecker;
        #endregion

        #region ctor
        public ValidIpService(
            IUnitOfWork uow,
            IApplicationSettingCacheManager applicationSettingCacheManager,
            IIpChecker ipChecker)
            : base(uow)
        {
            _applicationSettingCacheManager = applicationSettingCacheManager;
            _ipChecker = ipChecker;
        }
        #endregion



        public bool HasAccess(string ipAddress)
        {
            if (string.IsNullOrEmpty(ipAddress))
            {
                throw new ArgumentNullException("ipAddress");
            }

            var applicationSetting = _applicationSettingCacheManager.Get();

            // ip limited is off
            if (!applicationSetting.IsActiveIpValidation)
            {
                return true;
            }

            var ipScopes = new byte[4];
            bool isInDbValidIps = IsIpInDbValidIps(ipAddress, out ipScopes);
            if (isInDbValidIps)
            {
                return true;
            }

            var isValidIp = _ipChecker.IsValidIp(ipAddress);

            if (isValidIp)
            {
                Add(new ValidIp
                {
                    StartScope1 = ipScopes[0],
                    EndScope1 = ipScopes[0],
                    StartScope2 = ipScopes[1],
                    EndScope2 = ipScopes[1],
                    StartScope3 = ipScopes[2],
                    EndScope3 = ipScopes[2],
                    StartScope4 = ipScopes[3],
                    EndScope4 = ipScopes[3]
                });
                return true;
            }

            return false;
        }

        private bool IsIpInDbValidIps(string ipAddress, out byte[] ipScopes)
        {
            var ipScopesStr = ipAddress.Trim().Split('.');
            if (ipScopesStr.Length != 4)
            {
                throw new ArgumentException("ipAddress must be 4 scopes");
            }

            var scope1 = byte.Parse(ipScopesStr[0]);
            var scope2 = byte.Parse(ipScopesStr[1]);
            var scope3 = byte.Parse(ipScopesStr[2]);
            var scope4 = byte.Parse(ipScopesStr[3]);

            ipScopes = new byte[]{ scope1, scope2, scope3, scope4 };

            var isInValidIps = AddBasicConventions().Any(c =>
                scope1 >= c.StartScope1 && scope1 <= c.EndScope1 &&
                scope2 >= c.StartScope2 && scope2 <= c.EndScope2 &&
                scope3 >= c.StartScope3 && scope3 <= c.EndScope3 &&
                scope4 >= c.StartScope4 && scope4 <= c.EndScope4);

            return isInValidIps;
        }

    }
}
