﻿using Services.Interfaces;
using System.Linq;
using DomainModels.Entities.Financial;
using DatabaseContext.Context;

namespace Services.EfServices
{
    public class AdminRialAccountService : IAdminRialAccountService
    {

        #region props
        private readonly IUnitOfWork _uow;
        #endregion

        #region ctor
        public AdminRialAccountService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        #endregion



        public AdminRialAccount GetFirstRow()
        {
            return _uow.Set<AdminRialAccount>().FirstOrDefault();
        }
    }
}
