﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Financial;
using Infrastructure.Pagination;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using ViewModels.Financial;
using Infrastructure.Common;
using Infrastructure.Exceptions;

namespace Services.EfServices
{
    public class UserRialPaymentService : GenericService<UserRialPayment>, IUserRialPaymentService
    {

        #region ctor
        public UserRialPaymentService(IUnitOfWork uow) 
            : base(uow)
        { }

        #endregion


        public PagedList<UserRialPayment> SearchByRowGuid(Guid? code, int pageNumber = 1, int pageSize = 15)
        {
            var query = AddBasicConventions();

            if (code.HasValue)
            {
                query = query.Where(c => c.RowGuid == code.Value);
            }

            return query
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber, pageSize);
        }


        public void Add(CustomerUserRialPaymentCreateViewModel model, long customerId)
        {
            var hasUserAnyPendingequest = _uow.Set<UserRialPayment>()
                .Join(_uow.Set<PaymentStatus>(), urp => urp.PaymentStatusId, ps => ps.Id, (urp, ps) => new { urp, ps })
                .Join(_uow.Set<UserRialAccount>(), urp_ps => urp_ps.urp.UserRialAccountId, ura => ura.Id, (urp_ps, ura) => new { urp_ps.urp, urp_ps.ps, ura })
                .Any(c => c.ura.UserId == customerId && c.ps.Name == PaymentStatusName.Pending);
            if (hasUserAnyPendingequest)
            {
                throw new CustomException("امکان ثبت بیش از یک درخواست درحال بررسی وجود ندارد. لطفا تا بررسی درخواست در حال بررسی خود منتظر بمانید ");
            }


            model.TrackingNumber = model.TrackingNumber.Trim();
            var hasSameTrackingNumber = _dbSet.Any(c => c.TrackingNumber == model.TrackingNumber);
            if (hasSameTrackingNumber)
            {
                throw new CustomException("کد رهگیری واریز تکراری است");
            }

            var adminRialAccount = _uow.Set<AdminRialAccount>().First(c => c.DeletedOn == null);
            var pendingStatus = GetPaymentStatusByStatusName(PaymentStatusName.Pending);

            Add(new UserRialPayment
            {
                AdminRialAccountId = adminRialAccount.Id,
                PayDate = model.PayDate.ToMiladiDate(),
                PaymentStatusId = pendingStatus.Id,
                TrackingNumber = model.TrackingNumber,
                Price = model.Price,
                UserRialAccountId = model.UserRialAccountId
            });
        }

        private PaymentStatus GetPaymentStatusByStatusName(PaymentStatusName statusName)
        {
            return _uow.Set<PaymentStatus>().First(c => c.Name == statusName);
        }

        public PagedList<UserRialPayment> GetAllByCustomerId(long customerId, int pageNumber = 1, int pageSize = 15)
        {
            return AddBasicConventions()
                .Join(_uow.Set<UserRialAccount>(), p => p.UserRialAccountId, a => a.Id, (p, a) => new { p, a })
                .Where(c => c.a.UserId == customerId)
                .OrderByDescending(c => c.p.Id)
                .Select(c => c.p)
                .ToPagedList(pageNumber, pageSize);
        }

    }
}
