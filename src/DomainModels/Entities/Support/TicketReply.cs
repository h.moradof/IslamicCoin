using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Support 
{
	[Table("TicketReply", Schema = "Support")]
	public class TicketReply : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("متن پیام")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(2048, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string Message { get; set; }


		#region Navigation Properties

		// ----- User
		[Column(Order = 504), DisplayName("فرستنده")]
		public virtual long SenderId { get; set; }
		[ForeignKey("SenderId")]
		public virtual User Sender { get; set; }


		// ----- Ticket
		[Column(Order = 508)]
		public virtual long TicketId { get; set; }
		[ForeignKey("TicketId")]
		public virtual Ticket Ticket { get; set; }


		#endregion
	}
}