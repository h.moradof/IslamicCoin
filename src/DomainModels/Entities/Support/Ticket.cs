using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Support 
{
	[Table("Ticket", Schema = "Support")]
	public class Ticket : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("موضوع")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(256, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string Subject { get; set; }

		[Column(Order = 8)]
		[DisplayName("متن پیام")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(2048, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string Message { get; set; }

		[Column(Order = 12)]
		[DisplayName("باز است ؟")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual bool IsOpen { get; set; }


		#region Navigation Properties

		// ----- User
		[Column(Order = 504), DisplayName("فرستنده")]
		public virtual long SenderId { get; set; }
		[ForeignKey("SenderId")]
		public virtual User Sender { get; set; }


		// ----- TicketReply
		public virtual ICollection<TicketReply> TicketReplies { get; set; }


        #endregion

        public Ticket()
        {
            IsOpen = true;
        }
	}
}