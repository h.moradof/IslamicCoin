using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Setting 
{
	[Table("FinancialSetting", Schema = "Setting")]
	public class FinancialSetting : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("درصد حق السهم کسب و کار که موقع خروج پول از سایت از کاربر کسر میشه")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal Fee { get; set; }

		[Column(Order = 8)]
		[DisplayName("حداکثر مبلغ ریالی که کاربر می تونه از سایت خارج کنه")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual int MaximumRialWithdrawalPrice { get; set; }

		[Column(Order = 12)]
		[DisplayName("حداکثر مبلغ ارزی که کاربر می تونه از سایت خارج کنه")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal MaximumDolorWithdrawalAmount { get; set; }


		#region Navigation Properties

		#endregion
	}
}