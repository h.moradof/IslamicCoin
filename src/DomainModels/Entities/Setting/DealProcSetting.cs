using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Setting 
{
	[Table("DealProcSetting", Schema = "Setting")]
	public class DealProcSetting : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("آیا الان روشنه ؟")]
		public virtual bool IsOnNow { get; set; }

		[Column(Order = 8)]
		[DisplayName("تلاش کنه که وضعیت (روشن/خاموش) رو عوض کنه؟")]
		public virtual bool TryToChangeState { get; set; }


        // وقتی دکمه خاموش کردن پروسیجر یا روشن کردنش رو میزنیم، کل جدول قفل میشه
        // و تا زمانی که دستور داده شده به پروسیجر (روشن/خاموش شدنش) انجام نشه
        // قفل می مونه و نمیشه دستور جدیدی بهش بدیم
        [Column(Order = 12)]
        [DisplayName("آیا قفل است ؟")]
        public virtual bool IsLock { get; set; }


        #region Navigation Properties

        #endregion
    }
}