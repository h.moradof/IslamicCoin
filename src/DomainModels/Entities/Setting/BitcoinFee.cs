using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Setting 
{
	[Table("BitcoinFee", Schema = "Setting")]
	public class BitcoinFee : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("کارمزد شبکه بیت کوین")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal Fee { get; set; }


	}
}