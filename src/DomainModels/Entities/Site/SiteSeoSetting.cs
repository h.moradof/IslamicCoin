﻿using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Site
{
    [Table("SiteSeoSetting", Schema = "Site")]
    public class SiteSeoSetting : BaseEntity
    {
        [Column(Order = 1)]
        [DisplayName("عنوان سایت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string Title { get; set; }

        [Column(Order = 2)]
        [DisplayName("Url سایت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(64, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string Url { get; set; }

        [Column(Order = 3)]
        [DisplayName("کلمات کلیدی متا")]
        [StringLength(256, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string MetaKeywords { get; set; }

        [Column(Order = 4)]
        [DisplayName("توضیحات متا")]
        [StringLength(256, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string MetaDescription { get; set; }
    }
}
