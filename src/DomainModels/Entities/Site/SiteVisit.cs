﻿using DomainModels.Entities.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Site
{
    [Table("SiteVisit", Schema = "Site")]
    public class SiteVisit : BaseEntity
    {

        [Column(Order = 1)]
        [Required]
        [DisplayName("آی پی")]
        public virtual string Ip { get; set; }

        [Column(TypeName = "Date", Order = 2)]
        [DisplayName("تاریخ بازدید")]
        public virtual DateTime VisitDate { get; set; }

        [Column(Order = 3)]
        [DisplayName("تعداد بازدید")]
        public virtual int Number { get; set; }
    }
}
