﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Base
{
    public abstract class BaseUser : BaseEntity
    {
        [Column(Order = 1)]
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string FullName { get; set; }


        [Column(TypeName = "varchar", Order = 2)]
        [DisplayName("نام کاربری (موبایل)")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(11, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{11}$", ErrorMessage = "فرمت نام کاربری نادرست است")]
        [Index(IsUnique = true)]
        public virtual string Username { get; set; }


        [Column(TypeName = "varchar", Order = 3)]
        [DisplayName("کلمه عبور")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(2048, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string Password { get; set; }
        

        [Column(Order = 4)]
        [DisplayName("وضعیت حساب کاربری")]
        public virtual AccountStatus AccountStatus { get; set; }


        [Column(Order = 5)]
        [DisplayName("دلیل بن")]
        [StringLength(256, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string BanReason { get; set; }


    }

}
