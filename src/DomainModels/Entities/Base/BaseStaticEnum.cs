﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Base
{
    public abstract class BaseStaticEnum<TEnum> : BaseEntity where TEnum : struct, IComparable, IConvertible, IFormattable 
    {
        
        [Column(Order = 1)]
        public virtual TEnum Name { get; set; }


        [Column(Order = 2, TypeName = "varchar")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [ScaffoldColumn(false)]
        public virtual string IdentityName { get; set; }


        [Column(Order = 3)]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string DisplayName { get; set; }

    }
}
