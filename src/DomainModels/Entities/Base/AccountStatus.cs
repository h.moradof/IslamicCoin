﻿using System.ComponentModel;

namespace DomainModels.Entities.Base 
{
    public enum AccountStatus : byte
    {
        [Description("غیرفعال")]
        Deactive = 1,
        [Description("فعال")]
        Active,
        [Description("بن شده")]
        Baned
    }
}