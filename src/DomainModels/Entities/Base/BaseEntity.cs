﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Base
{
    public abstract class BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        [DisplayName("شناسه")]
        public virtual long Id { get; set; }
        
        // other props

        [Column(Order = 1001)]
        [DisplayName("تاریخ ثبت")]
        [ScaffoldColumn(false)]
        public virtual DateTime CreatedOn { get; set; }

        [Column(Order = 1002)]
        [DisplayName("تاریخ آخرین ویرایش")]
        [ScaffoldColumn(false)]
        public virtual DateTime? UpdatedOn { get; set; }

        [Column(Order = 1003)]
        [DisplayName("تاریخ حذف")]
        [ScaffoldColumn(false)]
        public virtual DateTime? DeletedOn { get; set; }


        public BaseEntity()
        {
            CreatedOn = DateTime.Now;
        }

    }
}
