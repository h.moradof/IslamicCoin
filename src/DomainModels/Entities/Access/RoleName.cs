﻿using System.ComponentModel;

namespace DomainModels.Entities.Access
{
    public enum RoleName : byte
    {
        [Description("مدیر کل")]
        Administrators = 1,
        [Description("کاربران تایید نشده")]
        RegisteredUsers, // can not have a deal
        [Description("کاربران تایید شده")]
        PermitedUsers,  // can have a deal
        [Description("کارمندان")]
        Employees
    }
}
