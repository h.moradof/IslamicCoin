﻿using DomainModels.Entities.Base;
using DomainModels.Entities.HumanResource;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Access
{
    [Table("Role", Schema = "Access")]
    public class Role : BaseEnum
    {
        [Column(Order = 3), DisplayName("عنوان")]
        public virtual RoleName RoleName
        {
            get{ return _roleName; }
            set
            {
                _roleName = value;
                Name = value.ToString();
            }
        }


        #region Navigation Properties

        //---- User
        public virtual ICollection<User> Users { get; set; }

        #endregion
        

        #region fields

        private RoleName _roleName;

        #endregion

    }
}
