﻿using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Access
{
    [Table("ValidIp", Schema = "Access")]
    public class ValidIp : BaseEntity
    {
        [Column(Order = 1)]
        [DisplayName("شروع اسکوپ 1")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte StartScope1 { get; set; }

        [Column(Order = 2)]
        [DisplayName("پایان اسکوپ 1")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte EndScope1 { get; set; }


        [Column(Order = 3)]
        [DisplayName("شروع اسکوپ 2")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte StartScope2 { get; set; }

        [Column(Order = 4)]
        [DisplayName("پایان اسکوپ 2")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte EndScope2 { get; set; }


        [Column(Order = 5)]
        [DisplayName("شروع اسکوپ 3")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte StartScope3 { get; set; }

        [Column(Order = 6)]
        [DisplayName("پایان اسکوپ 3")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte EndScope3 { get; set; }


        [Column(Order = 7)]
        [DisplayName("شروع اسکوپ 4")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte StartScope4 { get; set; }

        [Column(Order = 8)]
        [DisplayName("پایان اسکوپ 4")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Range(0, 255, ErrorMessage = "عددی بین 0 تا 255 وارد نمایید")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "لطفا یک عدد وارد نمایید")]
        public byte EndScope4 { get; set; }
    }
}
