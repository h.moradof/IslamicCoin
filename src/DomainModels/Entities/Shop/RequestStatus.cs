﻿using DomainModels.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Shop
{
    [Table("RequestStatus", Schema = "Shop")]
    public class RequestStatus : BaseStaticEnum<RequestStatusName>
    {


        #region Navigation Properties

        // ----- BuyRequest
        public virtual ICollection<BuyRequest> BuyRequests { get; set; }


        // ----- SaleRequest
        public virtual ICollection<SaleRequest> SaleRequests { get; set; }

        #endregion
    }
}
