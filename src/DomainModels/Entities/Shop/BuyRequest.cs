using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Shop 
{
	[Table("BuyRequest", Schema = "Shop")]
	public class BuyRequest : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("مبلغ ارزی درخواستی")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal Amount { get; set; }

		[Column(Order = 8)]
		[DisplayName("قیمت خرید")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual int Price { get; set; }

		[Column(Order = 16)]
		[DisplayName("کد شناسایی")]
		public virtual Guid RowGuid { get; set; }


        #region Navigation Properties

        // ----- RequestStatus
        [Column(Order = 502), DisplayName("وضعیت")]
        public virtual long RequestStatusId { get; set; }
        [ForeignKey("RequestStatusId")]
        public virtual RequestStatus RequestStatus { get; set; }


        // ----- User
        [Column(Order = 504), DisplayName("درخواست کننده")]
		public virtual long UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


		// ----- Deal
		public virtual ICollection<Deal> Deal { get; set; }


        #endregion


        public BuyRequest()
        {
            RowGuid = Guid.NewGuid();
        }

	}
}