using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Shop 
{
	[Table("SaleRequest", Schema = "Shop")]
	public class SaleRequest : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("میزان ارز")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal Amount { get; set; }

		[Column(Order = 8)]
		[DisplayName("قیمت فروش کل")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual int Price { get; set; }

        [Column(Order = 10)]
        [DisplayName("زیرمجموعه دارد ؟")]
        public virtual bool HasChild { get; set; }

        [Column(Order = 12)]
		[DisplayName("سطح")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(512, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string Level { get; set; }

		[Column(Order = 20)]
		[DisplayName("آیا درخواست انصراف داشته است ؟")]
		public virtual bool HasCancelationTry { get; set; }

		[Column(Order = 28)]
		[DisplayName("کد شناسایی")]
		public virtual Guid RowGuid { get; set; }


        #region Navigation Properties

        // ----- RequestStatus
        [Column(Order = 502), DisplayName("وضعیت")]
        public virtual long RequestStatusId { get; set; }
        [ForeignKey("RequestStatusId")]
        public virtual RequestStatus RequestStatus { get; set; }


        // ----- User
        [Column(Order = 504), DisplayName("درخواست کننده")]
		public virtual long UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


		// ----- SaleRequest
		[Column(Order = 508)]
		public virtual long? ParentId { get; set; }
		[ForeignKey("ParentId")]
		public virtual SaleRequest Parent { get; set; }
		public virtual ICollection<SaleRequest> Childs { get; set; }


		// ----- Deal
		public virtual ICollection<Deal> Deal { get; set; }


        #endregion


        public SaleRequest()
        {
            RowGuid = Guid.NewGuid();
        }

	}
}