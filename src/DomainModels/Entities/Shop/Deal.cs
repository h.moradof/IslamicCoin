using DomainModels.Entities.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Shop 
{
	[Table("Deal", Schema = "Shop")]
	public class Deal : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("کد شناسایی")]
		public virtual Guid RowGuid { get; set; }


		#region Navigation Properties

		// ----- SaleRequest
		[Column(Order = 504), DisplayName("درخواست فروش")]
		public virtual long SaleRequestId { get; set; }
		[ForeignKey("SaleRequestId")]
		public virtual SaleRequest SaleRequest { get; set; }


		// ----- BuyRequest
		[Column(Order = 508), DisplayName("درخواست خرید")]
		public virtual long BuyRequestId { get; set; }
		[ForeignKey("BuyRequestId")]
		public virtual BuyRequest BuyRequest { get; set; }


        #endregion



        public Deal()
        {
            RowGuid = Guid.NewGuid();
        }
	}
}