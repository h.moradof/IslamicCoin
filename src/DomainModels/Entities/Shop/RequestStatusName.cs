﻿namespace DomainModels.Entities.Shop
{
    public enum RequestStatusName : byte
    {
        OnProcessing = 0,
        Welded,
        Canceled
    }
}
