﻿namespace DomainModels.Entities.Financial
{
    public enum AccountAcceptStatusName  : byte
    {
        Pending = 1,
        Accepted,
        Declined
    }
}
