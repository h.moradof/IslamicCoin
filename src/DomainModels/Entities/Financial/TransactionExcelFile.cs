﻿using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Financial
{
    [Table("TransactionExcelFile", Schema = "Financial")]
    public class TransactionExcelFile : BaseEntity
    {

        [Column(Order = 4)]
        [DisplayName("شماره سند")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [Index(IsUnique = true)]
        public virtual int DocumentNumber { get; set; }

        [Column(Order = 8)]
        [DisplayName("مبلغ پرداخت شده به حساب سایت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public virtual int PayPrice { get; set; }

        [Column(Order = 12)]
        [DisplayName("شرح")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(1024)]
        public virtual string Description { get; set; }

    }
}
