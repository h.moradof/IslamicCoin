using DomainModels.Entities.Setting;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial 
{
	[Table("PaymentStatus", Schema = "Financial")]
	public class PaymentStatus : BaseStaticEnum<PaymentStatusName>
	{


		#region Navigation Properties

		// ----- UserDolorPayment
		public virtual ICollection<UserDolorPayment> UserDolorPayment { get; set; }


		// ----- UserRialPayment
		public virtual ICollection<UserRialPayment> UserRialPayment { get; set; }


		#endregion
	}
}