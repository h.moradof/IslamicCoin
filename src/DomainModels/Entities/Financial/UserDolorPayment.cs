using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace DomainModels.Entities.Financial 
{
	[Table("UserDolorPayment", Schema = "Financial")]
	public class UserDolorPayment : BaseEntity 
	{

        [Column(Order = 4)]
        [DisplayName("آدرس کیف بیت کوین")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string BtcWalletAddress { get; set; }

        [Column(Order = 6)]
		[DisplayName("مبلغ ارزی واریزی")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal Amount { get; set; }

        [Column(Order = 12)]
        [DisplayName("آیا کاربر پرداخت کرده ارز رو ؟")]
        public bool IsUserPaid { get; set; }


        [Column(Order = 20)]
        [DisplayName("کد شناسایی")]
        public virtual Guid RowGuid { get; set; }


        #region Navigation Properties

        // ----- PaymentStatus
        [Column(Order = 504), DisplayName("وضعیت")]
        public virtual long PaymentStatusId { get; set; }
        [ForeignKey("PaymentStatusId")]
        public virtual PaymentStatus PaymentStatus { get; set; }



        // ----- User
        [Column(Order = 508),DisplayName("واریز کننده ارز")]
        public virtual long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        #endregion


        public UserDolorPayment()
        {
            IsUserPaid = false;
            RowGuid = Guid.NewGuid();
        }
    }
}