using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial 
{
    // درخواست دریافت پول ارزی توسط کاربر
    [Table("DolorWithdrawalRequest", Schema = "Financial")]
	public class DolorWithdrawalRequest : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("آدرس کیف پول بیت کوین")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(64, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string BtcWalletAddress { get; set; }

		[Column(Order = 8)]
		[DisplayName("مبلغ ارزی")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal Amount { get; set; }

		[Column(Order = 12)]
		[DisplayName("کارمزد شبکه بیت کوین")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal BitcoinFee { get; set; }

		[Column(Order = 16)]
		[DisplayName("اطلاعات پرداخت")]
		[StringLength(512, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string PayDescription { get; set; }

		[Column(Order = 20)]
		[DisplayName("تاریخ پرداخت")]
		public virtual DateTime? PayDate { get; set; }


		#region Navigation Properties

		// ----- User
		[Column(Order = 504), DisplayName("درخواست کننده")]
		public virtual long UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


		// ----- WithdrawalRequestStatus
		[Column(Order = 508), DisplayName("وضعیت درخواست")]
		public virtual long WithdrawalRequestStatusId { get; set; }
		[ForeignKey("WithdrawalRequestStatusId")]
		public virtual WithdrawalRequestStatus WithdrawalRequestStatus { get; set; }


		#endregion
	}
}