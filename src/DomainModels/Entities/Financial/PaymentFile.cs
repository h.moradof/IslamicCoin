﻿using DomainModels.Entities.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial
{
    [Table("PaymentFile", Schema = "Financial")]
    public class PaymentFile : BaseEntity
    {

        [Column(Order = 4)]
        [DisplayName("محتوای فایل")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public virtual string FileContent { get; set; }

        [Column(Order = 8)]
        [DisplayName("کد شناسایی")]
        public virtual Guid RowGuid { get; set; }


        public PaymentFile()
        {
            RowGuid = Guid.NewGuid();
            FileContent = string.Empty;
        }

    }
}
