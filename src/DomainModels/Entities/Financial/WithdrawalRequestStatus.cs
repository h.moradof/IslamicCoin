﻿using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial 
{
	[Table("WithdrawalRequestStatus", Schema = "Financial")]
	public class WithdrawalRequestStatus : BaseStaticEnum<WithdrawalRequestStatusName> 
	{

		#region Navigation Properties

		// ----- RialWithdrawalRequest
		public virtual ICollection<RialWithdrawalRequest> RialWithdrawalRequest { get; set; }


		// ----- DolorWithdrawalRequest
		public virtual ICollection<DolorWithdrawalRequest> DolorWithdrawalRequest { get; set; }


		#endregion
	}
}