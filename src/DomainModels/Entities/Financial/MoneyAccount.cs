using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial 
{
	[Table("MoneyAccount", Schema = "Financial")]
	public class MoneyAccount : BaseEntity 
	{

        [ForeignKey("User")]
        public override long Id { get; set; }


        [Column(Order = 4)]
		[DisplayName("موجودی ریالی حساب کاربری")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual int TotalRialPrice { get; set; }


		[Column(Order = 8)]
		[DisplayName("موجودی ارزی حساب کاربری")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual decimal TotalDolorAmount { get; set; }


		#region Navigation Properties

		// ----- User
		public virtual User User { get; set; }

		#endregion
	}
}