namespace DomainModels.Entities.Financial 
{

	public enum WithdrawalRequestStatusName
	{
        Pending = 0,
        OnProcessing,
        Paid
	}
}