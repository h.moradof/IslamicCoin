using DomainModels.Entities.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial 
{
    // درخواست دریافت پول ریالی توسط کاربر
    [Table("RialWithdrawalRequest", Schema = "Financial")]
	public class RialWithdrawalRequest : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("مبلغ")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual int Price { get; set; }

		[Column(Order = 12)]
		[DisplayName("تاریخ پرداخت")]
		public virtual DateTime? PayDate { get; set; }


        [Column(Order = 16)]
        [DisplayName("کد شناسایی")]
        public virtual Guid RowGuid { get; set; }



        #region Navigation Properties

        // ----- RialAccount
        [Column(Order = 504), DisplayName("حساب ریالی مقصد (کاربر)")]
		public virtual long DestinationRialAccountId { get; set; }
		[ForeignKey("DestinationRialAccountId")]
		public virtual UserRialAccount UserRialAccount { get; set; }


		// ----- WithdrawalRequestStatus
		[Column(Order = 508), DisplayName("وضعیت درخواست")]
		public virtual long WithdrawalRequestStatusId { get; set; }
		[ForeignKey("WithdrawalRequestStatusId")]
		public virtual WithdrawalRequestStatus WithdrawalRequestStatus { get; set; }


        #endregion


        public RialWithdrawalRequest()
        {
            RowGuid = Guid.NewGuid();
        }
	}
}