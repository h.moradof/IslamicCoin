using DomainModels.Entities.Base;
using DomainModels.Entities.HumanResource;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial
{
	[Table("UserRialAccount", Schema = "Financial")]
	public class UserRialAccount : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("شماره شبا")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(64, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string ShabaNumber { get; set; }

		[Column(Order = 8)]
		[DisplayName("شماره کارت بانکی")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(32, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{16}$", ErrorMessage = "فرمت شماره کارت بانکی نادرست است (فقط عدد - 16 رقم)")]
        public virtual string CardNumber { get; set; }

		[Column(Order = 12)]
		[DisplayName("شماره حساب بانکی")]
		[StringLength(64, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]$", ErrorMessage = "فرمت شماره حساب بانکی نادرست است (فقط عدد)")]
        public virtual string AccountNumber { get; set; }



        #region Navigation Properties

        // ----- AccountAcceptStatus
        [Column(Order = 500), DisplayName("وضعیت")]
        public virtual long AccountAcceptStatusId { get; set; }
        [ForeignKey("AccountAcceptStatusId")]
        public virtual AccountAcceptStatus AccountAcceptStatus { get; set; }


        // ----- User
        [Column(Order = 504)]
		public virtual long UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


		// ----- RialWithdrawalRequest
		public virtual ICollection<RialWithdrawalRequest> RialWithdrawalRequests { get; set; }


		// ----- UserRialPayment
		public virtual ICollection<UserRialPayment> UserRialPayments { get; set; }


		// ----- Bank
		[Column(Order = 508)]
		public virtual long BankId { get; set; }
		[ForeignKey("BankId")]
		public virtual Bank Bank { get; set; }


		#endregion
	}
}