using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Financial
{
	[Table("Bank", Schema = "Financial")]
	public class Bank : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("نام بانک")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(128, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string Name { get; set; }


		#region Navigation Properties

		// ----- RialAccount
		public virtual ICollection<UserRialAccount> RialAccount { get; set; }


		#endregion
	}
}