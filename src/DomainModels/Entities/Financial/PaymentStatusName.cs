namespace DomainModels.Entities.Financial 
{
	
	public enum PaymentStatusName
	{
        Pending = 0,
        Accepted,
        Declined
	}
}