﻿using DomainModels.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Financial
{
    [Table("AccountAcceptStatus", Schema = "Financial")]
    public class AccountAcceptStatus : BaseStaticEnum<AccountAcceptStatusName>
    {

        // ----- UserRialAccount
        public virtual ICollection<UserRialAccount> UserRialAccounts { get; set; }



    }
}
