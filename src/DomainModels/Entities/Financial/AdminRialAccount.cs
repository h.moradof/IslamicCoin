using DomainModels.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Financial 
{
	[Table("AdminRialAccount", Schema = "Financial")]
	public class AdminRialAccount : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("شماره شبا")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(64, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string ShabaNumber { get; set; }

		[Column(Order = 8)]
		[DisplayName("شماره کارت بانکی")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(32, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string CardNumber { get; set; }

		[Column(Order = 12)]
		[DisplayName("شماره حساب بانکی")]
		[StringLength(64, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
		public virtual string AccountNumber { get; set; }


        #region Navigation Properties

        // ----- Bank
        [Column(Order = 508)]
        public virtual long BankId { get; set; }
        [ForeignKey("BankId")]
        public virtual Bank Bank { get; set; }


        // ----- UserRialPayment
        public virtual ICollection<UserRialPayment> UserRialPayment { get; set; }


		#endregion
	}
}