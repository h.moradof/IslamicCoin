using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Shop;
using DomainModels.Entities.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace DomainModels.Entities.Financial
{
	[Table("UserRialPayment", Schema = "Financial")]
	public class UserRialPayment : BaseEntity 
	{

		[Column(Order = 4)]
		[DisplayName("کد رهگیری واریز")]
		[Required(ErrorMessage= "{0} الزامی است")]
		[StringLength(64, ErrorMessage="حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [Index(IsUnique = true)]
		public virtual string TrackingNumber { get; set; }

		[Column(Order = 8)]
		[DisplayName("مبلغ (ریالی) واریز شده")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual int Price { get; set; }

		[Column(Order = 12)]
		[DisplayName("تاریخ پرداخت")]
		[Required(ErrorMessage= "{0} الزامی است")]
		public virtual DateTime PayDate { get; set; }
        
        [Column(Order = 20)]
        [DisplayName("کد شناسایی")]
        public virtual Guid RowGuid{ get; set; }



        #region Navigation Properties


        // ----- PaymentStatus
        [Column(Order = 504), DisplayName("وضعیت")]
		public virtual long PaymentStatusId { get; set; }
		[ForeignKey("PaymentStatusId")]
		public virtual PaymentStatus PaymentStatus { get; set; }


		// ----- RialAccount
		[Column(Order = 508), DisplayName("حساب ریالی بانکی مبدا (کاربر)")]
		public virtual long UserRialAccountId { get; set; }
		[ForeignKey("UserRialAccountId")]
		public virtual UserRialAccount UserRialAccount { get; set; }


		// ----- AdminRialAccount
		[Column(Order = 512), DisplayName("حساب ریالی بانکی مقصد (سایت)")]
		public virtual long AdminRialAccountId { get; set; }
		[ForeignKey("AdminRialAccountId")]
		public virtual AdminRialAccount AdminRialAccount { get; set; }

        #endregion

        public UserRialPayment()
        {
            RowGuid = Guid.NewGuid();
        }
	}
}