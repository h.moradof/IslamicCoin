﻿using System.ComponentModel;

namespace DomainModels.Entities.HumanResource
{
    public enum AcceptStatus : byte
    {
        [Description("نامشخص")]
        Unknown = 1,
        [Description("در حال بررسی")]
        Pending,
        [Description("تایید شده")]
        Accepted,
        [Description("رد شده")]
        Declined
    }
}
