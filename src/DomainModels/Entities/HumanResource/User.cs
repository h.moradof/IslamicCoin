﻿using DomainModels.Entities.Shop;
using DomainModels.Entities.Financial;
using DomainModels.Entities.Support;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModels.Entities.Access;

namespace DomainModels.Entities.HumanResource
{
    [Table("User", Schema = "HumanResource")]
    public class User : BaseUser
    {

        [Column(Order = 6)]
        [DisplayName("کد تایید شماره موبایل")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(6, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string ActivateCode { get; set; }


        [Column(Order = 8)]
        [DisplayName("شماره تلفن ثابت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(16, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{10,16}$", ErrorMessage = "فرمت شماره تلفن ثابت نادرست است (فقط عدد)")]
        public virtual string PhoneNumber { get; set; }

        [Column(Order = 24)]
        [DisplayName("آدرس ایمیل")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [EmailAddress(ErrorMessage = "فرمت آدرس ایمیل نادرست است")]
        public virtual string EmailAddress { get; set; }

        [Column(Order = 28)]
        [DisplayName("رمز دوم لاگین")]
        public virtual Guid? SecondLevelPassword { get; set; }

        [Column(Order = 30)]
        [DisplayName("کد شناسایی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public virtual Guid RowGuid { get; set; }


        // ------------------------ new columns ------------------------

        [Column(Order = 32)]
        [DisplayName("حداکثر مبلغ ریالی قابل خروج از سایت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public virtual int MaximumRialWithdrawalPrice { get; set; }

        [Column(Order = 36)]
        [DisplayName("حداکثر مبلغ ارزی قابل خروج از سایت")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public virtual decimal MaximumDolorWithdrawalAmount { get; set; }


        // ----------------------------- identity photos -----------------------------
        [Column(Order = 40)]
        [DisplayName("تصویر شناسنامه (کارت شناسایی)")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string IdentificationCardPhotoName { get; set; }

        [Column(Order = 44)]
        [DisplayName("تصویر چهره و کارت ملی")]
        public virtual string FaceAndNationalCardPhotoName { get; set; }


        // ----------------------------- accept statuses -----------------------------
        [Column(Order = 48)]
        [DisplayName("وضعیت تایید شماره موبایل")]
        public virtual AcceptStatus MobileNumberAcceptStatus { get; set; }

        [Column(Order = 52)]
        [DisplayName("وضعیت تایید شماره تلفن ثابت")]
        public virtual AcceptStatus PhoneNumberAcceptStatus { get; set; }

        [Column(Order = 55)]
        [DisplayName("وضعیت تایید کارت شناسایی کاربر")]
        public virtual AcceptStatus IdentificationCardAcceptStatus { get; set; }

        [Column(Order = 57)]
        [DisplayName("وضعیت تایید چهره و کارت ملی کاربر")]
        public virtual AcceptStatus FaceAndNationalCardAcceptStatus { get; set; }


        // ----------------------------- calculation props -----------------------------
        [NotMapped]
        public bool CanUserEditIdentificationPhoto
        {
            get
            {
                return (IdentificationCardAcceptStatus == AcceptStatus.Unknown || IdentificationCardAcceptStatus == AcceptStatus.Declined);
            }
        }

        [NotMapped]
        public bool CanUserEditFaceAndNationalCardPhoto
        {
            get
            {
                return (FaceAndNationalCardAcceptStatus == AcceptStatus.Unknown || FaceAndNationalCardAcceptStatus == AcceptStatus.Declined);
            }
        }



        #region Navigation Properties

        // ---- Role
        [Column(Order = 500)]
        [DisplayName("کد شناسایی")]
        public virtual long RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
        

        // ----- BuyRequest
        public virtual ICollection<BuyRequest> BuyRequests { get; set; }


        // ----- SaleRequest
        public virtual ICollection<SaleRequest> SaleRequests { get; set; }


        // ----- MoneyAccount
        public virtual MoneyAccount MoneyAccount { get; set; }


        // ----- RialAccount
        public virtual ICollection<UserRialAccount> RialAccounts { get; set; }


        // ----- DolorWithdrawalRequest
        public virtual ICollection<DolorWithdrawalRequest> DolorWithdrawalRequests { get; set; }


        // ----- Ticket
        public virtual ICollection<Ticket> Tickets { get; set; }


        // ----- TicketReply
        public virtual ICollection<TicketReply> TicketReplies { get; set; }

        #endregion




        public User()
        {
            RowGuid = Guid.NewGuid();
            SecondLevelPassword = Guid.NewGuid();
            AccountStatus = AccountStatus.Active;

            MobileNumberAcceptStatus = AcceptStatus.Unknown;
            PhoneNumberAcceptStatus = AcceptStatus.Unknown;
            IdentificationCardAcceptStatus = AcceptStatus.Unknown;
            FaceAndNationalCardAcceptStatus = AcceptStatus.Unknown;
        }

    }
}
